using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DataMarketAPI.Tests.Services
{
    [TestClass]
    public class CacheService_ShouldWork
    {
        private readonly CacheService _cacheService;
        private DistributedCacheEntryOptions _distributedCacheOptions;
        private readonly string _cacheKey = "Key_1";
        private readonly byte[] _bytes = new byte[] { 1, 2, 3 };

        private DataStoreCollectionModel _dsc;

        public CacheService_ShouldWork()
        {
            //SetupDataStore();

            var logger = new Mock<ILogger<CacheService>>();

            var MockCache = new Mock<IDistributedCache>();

            _distributedCacheOptions = new DistributedCacheEntryOptions();
            _distributedCacheOptions.AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(100);
            _cacheService = new CacheService(logger.Object, MockCache.Object);

            //MockCache.Setup(c => c.SetAsync(_cacheKey, _bytes, _distributedCacheOptions)).Returns(Task.FromResult(true));

            // Added tight coupling between Serialize and Deserialize, however the point is moot as if Serialize doesnt work
            // the test should fail anyway.
            //MockCache.Setup(c => c.GetAsync(_cacheKey)).Returns(Task.FromResult(_cacheService.Serialize(_dsc)));
        }

        //[Fact]
        //public void TestWrite()
        //{
        //    var result = _cacheService.Write(_dsc).Result;

        //    Xunit.Assert.True(result);
        //}

        //[Fact]
        //public void TestRead()
        //{
        //    var result = _cacheService.Read(_cacheKey).Result;

        //    Xunit.Assert.NotNull(result);
        //}

        //protected void SetupDataStore()
        //{
        //    _dsc = new DataStoreCollectionModel();
        //    _dsc.Data = new List<string>();

        //    _dsc.Data.Add("testRow1");
        //    _dsc.Data.Add("testRow2");
        //    ;
        //    _dsc.ID = _cacheKey;
        //}
    }
}