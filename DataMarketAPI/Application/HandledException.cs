namespace DataMarketAPI.Application
{
    ///<Summary>
    /// Exception Type for handeled cases.
    ///</Summary>
    public class HandledException : System.Exception
    {
        public ThrowResponseLevel throwResponseLevel { get; set; }

        public enum ThrowResponseLevel
        {
            Unathorized = -1,
            BadRequest = 0,
            Ok = 1
        }

        ///<Summary>
        /// Parameter-less constructor
        ///</Summary>
        public HandledException() { }

        ///<Summary>
        /// Parameter-less constructor
        ///</Summary>
        public HandledException(string message, ThrowResponseLevel responseLevel) : base(message)
        {
            throwResponseLevel = responseLevel;
        }

        ///<Summary>
        /// Message only constructor
        ///</Summary>
        public HandledException(string message) : base(message) { }

        ///<Summary>
        /// Message and Exception constructor
        ///</Summary>
        public HandledException(string message, System.Exception inner) : base(message, inner) { }
    }
}