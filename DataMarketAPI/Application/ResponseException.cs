namespace DataMarketAPI.Application
{
    ///<Summary>
    /// Exception Type for controllers.
    ///</Summary>
    public class ResponseException : System.Exception
    {
        ///<Summary>
        /// Parameter-less constructor
        ///</Summary>
        public ResponseException() { }

        ///<Summary>
        /// Message only constructor
        ///</Summary>
        public ResponseException(string message) : base(message) { }

        ///<Summary>
        /// Message and Exception constructor
        ///</Summary>
        public ResponseException(string message, System.Exception inner) : base(message, inner) { }
    }
}