using DataMarketAPI.Application;
using DataMarketAPI.Database;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using DataMarketAPI.ViewModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;
        private readonly RequestBetaService _requestBetaSerivce;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly TokenService _tokenService;
        private readonly DatabaseContext _context;

        public AccountController(
            AccountService accountService,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            RequestBetaService requestBetaService,
            TokenService tokenService,
            DatabaseContext context
            )
        {
            _signInManager = signInManager;
            _accountService = accountService;
            _userManager = userManager;
            _requestBetaSerivce = requestBetaService;
            _tokenService = tokenService;
            _context = context;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetUser()
        {
            User user = await _userManager.GetUserAsync(HttpContext.User);

            return Ok(user);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUserClaims()
        {
            return Ok(User.Claims.Select(c =>
            new
            {
                Type = c.Type,
                Value = c.Value
            }));
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetUserProfile()
        {
            User user = await _userManager.GetUserAsync(HttpContext.User);
            user.Company = await _accountService.PopulateCompany(user);
            user.OmitSentsitiveData();
            return Ok(user);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateProfile([FromBody] User userVm)
        {
            try
            {
                await _accountService.UpdateProfile(userVm);
            }
            catch (ResponseException exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginVm)
        {
            if (ModelState.IsValid)
            {
                // var user = await _userManager.FindByNameAsync(loginVm.Email);
                // if (user != null)
                // {
                //     if (!await _userManager.IsEmailConfirmedAsync(user))
                //     {
                //         return BadRequest("Please confirm your address.");
                //     }
                // }

                var result = await _signInManager.PasswordSignInAsync(loginVm.Email, loginVm.Password, loginVm.RememberMe, lockoutOnFailure: true);

                if (!result.Succeeded)
                {
                    return StatusCode(422);
                }

                if (result.IsLockedOut)
                {
                    return StatusCode(429);
                }

                User user = await _userManager.FindByNameAsync(loginVm.Email);
                await _accountService.UpdateLastLogin(user);

                if (user.CompanyId != null)
                {
                    user.Company = await _accountService.PopulateCompany(user);
                }

                return Ok(_tokenService.GenerateToken(HttpContext, user));
            }

            return BadRequest();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerVm)
        {
            try
            {
                await _accountService.Register(registerVm);
            }
            catch (ResponseException exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok();
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        [HttpGet("{email}")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassowrd(string email)
        {
            if (email.Length > 0)
            {
                await _accountService.ForgotPassowrd(email);

                return Ok();
            }

            return BadRequest("Please provide all the required fields.");
        }

        /// <summary>
        /// Reset Password action.
        /// POST: /Account/ResetPassword
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] NewPasswordViewModel vm)
        {
            if (ModelState.IsValid)
            {
                bool result = await _accountService.ResetPassword(vm);
                if (result)
                {
                    return Ok();
                }
                return BadRequest("There was an error processing your request please try again.");
            }

            return BadRequest("Please provide all the required fields.");
        }

        /// <summary>
        /// Email confirmation
        /// GET: /Account/ConfirmEmail/{verificationKey}
        /// </summary>
        /// <param name="verificationKey"></param>
        /// <returns></returns>
        [HttpGet("{verificationKey}")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromBody] string verificationKey)
        {
            if (verificationKey.Length == 0)
            {
                return BadRequest();
            }

            await _accountService.ConfirmEmail(verificationKey);
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RequestBeta([FromBody] RequestBetaModel requestBeta)
        {
            if (ModelState.IsValid)
            {
                int result = await _requestBetaSerivce.Create(requestBeta);
                if (result > 0)
                {
                    return Ok();
                }
            }

            return BadRequest("Please provide all the required fields.");
        }
    }
}