﻿using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CardsController : Controller
    {
        private readonly CardService _cardService;

        public CardsController(CardService cardService)
        {
            _cardService = cardService;
        }

        [HttpGet]
        public IActionResult DatasourceCount()
        {
            return Ok(_cardService.GetDatasourceCount(HttpContext.GetCurrentUser()));
        }

        [HttpGet]
        public IActionResult MetricCount()
        {
            return Ok(_cardService.GetMetricCount(HttpContext.GetCurrentUser()));
        }

        [HttpGet]
        public IActionResult RequestCount()
        {
            return Ok(_cardService.GetRequestCount(HttpContext.GetCurrentUser()));
        }

        [HttpGet]
        public IActionResult ConsumerCount()
        {
            return Ok(_cardService.GetConsumerCount(HttpContext.GetCurrentUser()));
        }
    }
}