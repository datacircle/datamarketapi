using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ConnectorController : Controller
    {
        private readonly DatasourceService _datasourceService;

        public ConnectorController(DatasourceService datasourceService)
        {
            _datasourceService = datasourceService;
        }

        [HttpPost]
        public async Task<IActionResult> CheckAnonymConnection([FromBody] DatasourceModel datasource)
        {
            if (ModelState.IsValid && Enum.IsDefined(typeof(Models.Type), datasource.Type))
            {
                return Ok(await _datasourceService.CheckAnonymConnection(datasource) ? 1 : 0);
            }

            return BadRequest();
        }
    }
}