﻿using DataMarketAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CronjobController : Controller
    {
        private readonly MetricTransferService _metricTransferService;
        private readonly ILogger _logger;

        public CronjobController(
            MetricTransferService metricTransferService,
            ILogger<DataController> logger)
        {
            _metricTransferService = metricTransferService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> ProcessAllMetricTransfers()
        {
            //HttpClient client = new HttpClient();
            //IEnumerable<MetricTransferModel> _metricTransferCollection = await _metricTransferService.GetAllThatNeedProcessing();

            //foreach (MetricTransferModel _mt in _metricTransferCollection)
            //{
            //    BackgroundJob.Enqueue(() => _metricTransferService.Transfer(_mt, true));
            //}

            //Parallel.ForEach(_metricTransferCollection, (metricTransfer) =>
            //{
            //    try
            //    {
            //        var a = _metricTransferService.Transfer(metricTransfer, true).Result;
            //    }
            //    catch (Exception e)
            //    {
            //        _logger.LogError($"MetricTransfer Processing Failure [{metricTransfer.Id}]" + e.Message);
            //    }
            //});

            return Ok();
        }
    }
}