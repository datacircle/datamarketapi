using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DataController : Controller
    {
        private readonly MetricService _metricService;
        private readonly DatasourceService _datasourceService;
        private readonly DataService _dataService;
        private readonly RequestService _requestService;
        private readonly ILogger _logger;

        public DataController(
            MetricService metricService,
            DatasourceService datasourceService,
            DataService dataEngineService,
            RequestService requestService,
            ILogger<DataController> logger)
        {
            _metricService = metricService;
            _datasourceService = datasourceService;
            _dataService = dataEngineService;
            _requestService = requestService;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            int? CompanyFk = HttpContext.GetCompanyId();

            MetricModel _m = new MetricModel();
            _m.CompanyId = HttpContext.GetCompanyId();
            _m.ID = id;

            _m = await _metricService.GetWithDatasource(_m);
            _m.Datasource = _datasourceService.UnProtect(_m.Datasource);

            if (_m == null)
            {
                return NotFound("No data found");
            }

            try
            {
                return Ok(await _dataService.Gather(_m));
            }
            catch (HandledException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError("Error dataservice gather " + e.Message);
                return BadRequest("Error Processing");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Trigger(Guid id)
        {
            int? CompanyFk = HttpContext.GetCompanyId();

            MetricModel _m = new MetricModel();
            _m.CompanyId = HttpContext.GetCompanyId();
            _m.Owner = HttpContext.GetUserId();
            _m.ID = id;

            _m = await _metricService.GetWithDatasource(_m);

            _m.Datasource = _datasourceService.UnProtect(_m.Datasource);

            if (_m == null)
            {
                return NotFound("No data found");
            }

            try
            {
                if ((await _dataService.Gather(_m)).Structure.Count > 0)
                {
                    return Ok();
                }
                else
                {
                    return new StatusCodeResult(204);
                }
            }
            catch (HandledException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError("Error dataservice gather " + e.Message);
                return BadRequest("Error Processing");
            }
        }
    }
}