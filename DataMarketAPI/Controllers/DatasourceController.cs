using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DatasourceController : Controller
    {
        private readonly ILogger _logger;
        private readonly DatasourceService _datasourceService;
        private readonly MetricService _metricService;

        public DatasourceController(ILogger<DatasourceController> logger, DatasourceService datasourceService, MetricService metricService)
        {
            _logger = logger;
            _datasourceService = datasourceService;
            _metricService = metricService;
        }

        [HttpGet("{id?}")]
        public async Task<IActionResult> Get(Guid id)
        {
            int? CompanyFk = HttpContext.GetCompanyId();

            if (id != Guid.Empty)
            {
                DatasourceModel datasourceContainer = new DatasourceModel { ID = id, CompanyId = CompanyFk, Owner = HttpContext.GetUserId() };
                DatasourceModel datasource = await _datasourceService.Get(datasourceContainer);

                if (
                    datasource == null ||
                    datasource.CompanyId != HttpContext.GetCompanyId() ||
                    datasource.Owner != HttpContext.GetUserId()
                )
                {
                    return NotFound();
                }

                if ((int)datasource.Type < 100)
                {
                    return NotFound();
                }

                return Ok(datasource);
            }
            else
            {
                DatasourceModel _datasource = new DatasourceModel
                {
                    CompanyId = HttpContext.GetCompanyId(),
                    Owner = HttpContext.GetUserId()
                };

                IEnumerable<DatasourceModel> datasourceCollection = await _datasourceService.GetAll(_datasource);

                if (datasourceCollection == null)
                {
                    return BadRequest("No Data found.");
                }

                return Ok(datasourceCollection);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] DatasourceModel datasource)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            datasource.CompanyId = HttpContext.GetCompanyId();
            datasource.Owner = HttpContext.GetUserId();

            datasource = await _datasourceService.Create(datasource);

            return Ok(datasource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] DatasourceModel datasource)
        {
            if (id != datasource.ID)
            {
                return NotFound("Id's do not match.");
            }

            datasource.CompanyId = HttpContext.GetCompanyId();
            datasource.Owner = HttpContext.GetUserId();

            await _datasourceService.Update(datasource);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] DatasourceModel datasource)
        {
            datasource.CompanyId = HttpContext.GetCompanyId();
            datasource.Owner = HttpContext.GetUserId();

            if (await _datasourceService.IsAttachedToMetric(datasource))
            {
                return BadRequest("Cannot delete a datasource that has Metrics.");
            }

            await _datasourceService.Delete(datasource);

            return Ok();
        }
    }
}