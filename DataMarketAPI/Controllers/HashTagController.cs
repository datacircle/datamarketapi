﻿using DataMarketAPI.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class HashTagController : Controller
    {
        private readonly IHashTagRepository _hashTagRepository;

        public HashTagController(IHashTagRepository hashTagRepository)
        {
            _hashTagRepository = hashTagRepository;
        }

        [HttpGet("{keyword}")]
        public async System.Threading.Tasks.Task<IActionResult> SearchAsync(string keyword)
        {
            if (keyword.Length < 2)
            {
                return Ok();
            }

            return Ok(await _hashTagRepository.Search(keyword));
        }
    }
}