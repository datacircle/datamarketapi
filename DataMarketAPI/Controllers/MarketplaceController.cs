﻿using DataMarketAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class MarketplaceController : Controller
    {
        private readonly IMetricRepository _metricRepository;

        public MarketplaceController(IMetricRepository metricRepository)
        {
            _metricRepository = metricRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return View(_metricRepository.GetAllForMarketplace());
        }
    }
}