﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using DataMarketAPI.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MetricAccessController : Controller
    {
        private readonly MetricAccessService _metricAccessService;

        public MetricAccessController(MetricAccessService metricAccessService)
        {
            _metricAccessService = metricAccessService;
        }

        [HttpGet("{metricId}")]
        public async Task<IActionResult> GetAll(Guid metricId)
        {
            MetricAccessMapModel _m = new MetricAccessMapModel
            {
                ActionableUserId = HttpContext.GetUserId(),
                MetricId = metricId
            };

            return Ok(await _metricAccessService.GetAll(_m));
        }

        [HttpDelete("{metricAccessId?}")]
        public async Task<IActionResult> Delete(int metricAccessId)
        {
            MetricAccessMapModel _m = new MetricAccessMapModel
            {
                ActionableUserId = HttpContext.GetUserId(),
                ID = metricAccessId
            };

            _m = await _metricAccessService.Get(_m);

            if (_m == null)
            {
                return BadRequest("No such share was found.");
            }

            try
            {
                _metricAccessService.Delete(_m);
                return Ok();
            }
            catch (HandledException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] MetricAccessMapViewModel model)
        {
            if (!ModelState.IsValid || (string.IsNullOrEmpty(model.Email) && string.IsNullOrEmpty(model.TargetPublicId)))
            {
                return BadRequest("Please provide all the required fields.");
            }

            User targetUser = await _metricAccessService.FindUser(model.Email, model.TargetPublicId);

            if (targetUser == null)
            {
                return BadRequest("No user could be found [" + model.Email + "]");
            }

            if (targetUser.Id == HttpContext.GetUserId())
            {
                return BadRequest("You can't add yourself to the share list.");
            }

            MetricAccessMapModel _m = new MetricAccessMapModel
            {
                ActionableUserId = HttpContext.GetUserId(),
                TargetUserId = targetUser.Id,
                MetricId = model.MetricId
            };

            try
            {
                MetricAccessMapModel _map = await _metricAccessService.Create(_m);
                return Ok(_map);
            }
            catch (HandledException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}