using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class MetricCommentController : Controller
    {
        private readonly MetricCommentService _metricCommentService;

        public MetricCommentController(MetricCommentService metricCommentService)
        {
            _metricCommentService = metricCommentService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            IEnumerable<MetricCommentModel> request = await _metricCommentService.GetByRequest(new MetricCommentModel { MetricId = id });

            if (request == null)
            {
                return NotFound();
            }

            return Ok(request);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] CommentModel comment)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            MetricCommentModel mc = new MetricCommentModel
            {
                MetricId = comment.TargetId,
                Value = comment.Value,
                Owner = HttpContext.GetUserId()
            };

            await _metricCommentService.Create(mc);

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] MetricCommentModel metricComment)
        {
            metricComment.Owner = HttpContext.GetUserId();

            await _metricCommentService.Delete(metricComment);

            return Ok();
        }
    }
}