using DataMarketAPI.Models;
using DataMarketAPI.Services;
using DataMarketAPI.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class MetricController : Controller
    {
        private readonly MetricService _metricService;
        private readonly DatasourceService _datasourceService;
        private readonly OrderService _orderService;

        public MetricController(MetricService metricService, DatasourceService datasourceService, OrderService orderService)
        {
            _metricService = metricService;
            _datasourceService = datasourceService;
            _orderService = orderService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            MetricModel metricContainer = new MetricModel { ID = id, CompanyId = HttpContext.GetCompanyId(), Owner = HttpContext.GetUserId() };
            MetricModel metric = await _metricService.Get(metricContainer);

            if (metric == null)
            {
                return NotFound();
            }

            // return data for 3rd Party view
            if (metric.Owner != HttpContext.GetUserId() && (metric.CompanyId == null || metric.CompanyId != HttpContext.GetCompanyId()))
            {
                DatasourceModel ds = await _datasourceService.GetUncencorsed(new DatasourceModel { ID = metric.DatasourceId });
                MetricDatasourceViewModel vm = new MetricDatasourceViewModel();
                vm.DatasourceType = ds.Type;
                vm.Hydrate(metric);
                vm.CreatedAt = metric.CreatedAt;
                vm.UpdatedAt = metric.UpdatedAt;
                vm.LastPool = metric.LastPool;

                return Ok(vm);
            }

            return Ok(metric);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get(bool filterForRequest, bool filterForCompany)
        {
            // all for company
            if (filterForCompany)
            {
                return await GetAllWithinCompany();
            }

            // all for request
            if (filterForRequest)
            {
                return await GetAllForRequest();
            }

            // all
            return BadRequest();
        }

        private async Task<IActionResult> GetAllWithinCompany()
        {
            MetricModel _metric = new MetricModel()
            {
                CompanyId = HttpContext.GetCompanyId(),
                Owner = HttpContext.GetUserId()
            };

            IEnumerable<MetricModel> metricCollection = await _metricService.GetAllForCompany(_metric);

            if (metricCollection == null)
            {
                return BadRequest("No Data found.");
            }

            return Ok(metricCollection);
        }

        private async Task<IActionResult> GetAllForRequest()
        {
            MetricModel _metric = new MetricModel()
            {
                CompanyId = HttpContext.GetCompanyId(),
                Owner = HttpContext.GetUserId()
            };

            IEnumerable<MetricModel> metricCollection = await _metricService.GetAllForRequestCreation(_metric);

            if (metricCollection == null)
            {
                return BadRequest("No Data found.");
            }

            return Ok(metricCollection);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] MetricModel metric)
        {
            if (metric.Structure == null)
            {
                metric.Structure = "";
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (metric.Query == String.Empty && metric.FileName == String.Empty)
            {
                return BadRequest(ModelState);
            }

            Guid DatasourceId = metric.DatasourceId;
            int? CompanyFk = HttpContext.GetCompanyId();

            if (DatasourceId == Guid.Empty)
            {
                return BadRequest("Invalid Datasource Id.");
            }

            DatasourceModel datasourceContainer = new DatasourceModel { ID = DatasourceId, CompanyId = CompanyFk, Owner = HttpContext.GetUserId() };
            DatasourceModel _datasource = await _datasourceService.Get(datasourceContainer);

            if (_datasource == null)
            {
                return BadRequest("No such Datasource found.");
            }

            metric.CompanyId = CompanyFk;
            metric.Owner = HttpContext.GetUserId();
            metric.Datasource = _datasource;
            metric = await _metricService.Create(metric);

            return Ok(metric);
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update(Guid id, [FromBody] MetricModel metric)
        {
            if (metric.Structure == null)
            {
                metric.Structure = "";
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != metric.ID)
            {
                return BadRequest();
            }

            if (id == Guid.Empty || metric.ID == Guid.Empty)
            {
                return BadRequest();
            }

            Guid DatasourceId = metric.DatasourceId;

            if (DatasourceId == Guid.Empty)
            {
                return BadRequest("Invalid Datasource Id.");
            }

            int? CompanyFk = HttpContext.GetCompanyId();

            DatasourceModel datasourceContainer = new DatasourceModel { ID = DatasourceId, CompanyId = CompanyFk, Owner = HttpContext.GetUserId() };
            DatasourceModel _datasource = await _datasourceService.Get(datasourceContainer);

            if (_datasource == null)
            {
                return BadRequest("No such Datasource found.");
            }

            metric.Datasource = _datasource;
            metric.CompanyId = CompanyFk;
            metric.Owner = datasourceContainer.Owner;
            MetricModel newMetric = await _metricService.UpdateMetricBusiness(metric);

            return Ok(newMetric);
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            MetricModel metric = new MetricModel() { ID = id };

            metric.CompanyId = HttpContext.GetCompanyId();
            metric.Owner = HttpContext.GetUserId();

            await _metricService.Delete(metric);

            return Ok();
        }
    }
}