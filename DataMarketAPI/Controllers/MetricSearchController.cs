﻿using DataMarketAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class MetricSearchController : Controller
    {
        private readonly MetricService _metricService;
        private readonly ILogger _logger;

        public MetricSearchController(ILogger<MetricSearchController> logger, MetricService metricService)
        {
            _logger = logger;
            _metricService = metricService;
        }

        [HttpGet("")]
        public async Task<IActionResult> FullMetricDetails([FromQuery] string searchTerm, [FromQuery] string hashtagTerm)
        {
            try
            {
                return Ok(await _metricService.Search(searchTerm, hashtagTerm));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Search Term Error [{searchTerm}]" + Environment.NewLine + ex.Message);
                return BadRequest("Bad search term.");
            }
        }
    }
}