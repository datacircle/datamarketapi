﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MetricTransferController : Controller
    {
        private readonly IMetricRepository _metricRepository;
        private readonly MetricTransferService _metricTransferService;
        private readonly IDatasourceRepository _datasourceRepository;
        private readonly OrderService _orderService;

        public MetricTransferController(IMetricRepository metricRepository, MetricTransferService metricTransferService, IDatasourceRepository datasourceRepository, OrderService orderService)
        {
            _metricRepository = metricRepository;
            _metricTransferService = metricTransferService;
            _datasourceRepository = datasourceRepository;
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            MetricTransferModel _mtContainer = new MetricTransferModel
            {
                ActionableUser = HttpContext.GetUserId(),
                TargetCompanyID = HttpContext.GetCompanyId()
            };

            return Ok(await _metricTransferService.GetAll(_mtContainer));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] MetricTransferModel metricTransfer)
        {
            metricTransfer.TargetCompanyID = HttpContext.GetCompanyId() ?? null;
            metricTransfer.ActionableUser = HttpContext.GetUserId();

            if (!await _metricRepository.UserHasViewAccess(metricTransfer.MetricID, metricTransfer.ActionableUser, metricTransfer.TargetCompanyID))
            {
                return Unauthorized();
            }

            if (!ModelState.IsValid && (metricTransfer.DownloadAsFile == false && metricTransfer.TargetDatasourceID == Guid.Empty))
            {
                return this.BadRequest("Please provide all the required fields.");
            }

            if (await _metricRepository.HasCostAsync(metricTransfer.MetricID) && !await _orderService.OrderIsPaid(metricTransfer.MetricID, metricTransfer.ActionableUser))
            {
                return Unauthorized();
            }

            if (metricTransfer.DownloadAsFile)
            {
                DatasourceModel _datasourceEntity = await _datasourceRepository.GetSystem(new DatasourceModel() { Type = Models.Type.FileDownload });
                metricTransfer.TargetDatasourceID = _datasourceEntity.ID;
            }

            try
            {
                metricTransfer = await _metricTransferService.Create(metricTransfer);

                DataStoreCollectionModel _dsc = await _metricTransferService.Transfer(metricTransfer);

                if (_dsc != null)
                {
                    return Ok(_dsc);
                }

                return Ok();
            }
            catch (HandledException he)
            {
                return BadRequest(he.Message);
            }
            catch (Exception e)
            {
                return BadRequest("Service Error, please contact Support if the issue persists.");
            }
        }
    }
}