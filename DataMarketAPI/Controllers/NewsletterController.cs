using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    /// <summary>
    ///
    /// </summary>
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class NewsletterController : Controller
    {
        private readonly NewsletterService _newsletterService;

        /// <summary>
        ///
        /// </summary>
        /// <param name="newsletterService"></param>
        public NewsletterController(NewsletterService newsletterService)
        {
            _newsletterService = newsletterService;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="newsletterModel"></param>
        /// <returns></returns>
        [HttpGet("{email}")]
        [AllowAnonymous]
        public async Task<IActionResult> Subscribe([FromBody] NewsletterModel newsletterModel)
        {
            if (newsletterModel != null && newsletterModel.Email.Length > 0)
            {
                await _newsletterService.Create(newsletterModel);
            }

            return Ok();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{token}")]
        [AllowAnonymous]
        public async Task<IActionResult> DoubleOptIn(string token)
        {
            if (ModelState.IsValid)
            {
                int result = await _newsletterService.DoubleOptIn(token);
                if (result >= 1)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Something went wrong. Please try again.");
                }
            }

            return BadRequest("Please provide all required details.");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("{token}")]
        [AllowAnonymous]
        public async Task<IActionResult> UnSubscribe(string token)
        {
            Console.WriteLine(token);
            if (token != null && token.Length > 0)
            {
                if ((await _newsletterService.Unsubscribe(token)) > 0)
                {
                    return Ok();
                }
            }
            return BadRequest("Please provide all required details.");
        }
    }
}