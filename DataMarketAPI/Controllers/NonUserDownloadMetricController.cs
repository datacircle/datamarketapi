﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class NonUserDownloadMetricController : Controller
    {
        private readonly NonUserDownloadMetricService _nonUserDownloadMetricService;

        public NonUserDownloadMetricController(NonUserDownloadMetricService nonUserDownloadMetricService)
        {
            _nonUserDownloadMetricService = nonUserDownloadMetricService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] FreeDownloadModel model)
        {
            try
            {
                await _nonUserDownloadMetricService.Create(model);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Error creating request for download");
            }
        }

        [HttpPost]
        public async Task<IActionResult> JustAdd([FromBody] FreeDownloadModel model)
        {
            try
            {
                await _nonUserDownloadMetricService.JustAdd(model);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Error creating request for download");
            }
        }

        [HttpGet("{freeDownloadId}")]
        public async Task<IActionResult> Download(Guid freeDownloadId)
        {
            try
            {
                return Ok(await _nonUserDownloadMetricService.Download(freeDownloadId));
            }
            catch (HandledException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest("Error downloading file");
            }
        }
    }
}