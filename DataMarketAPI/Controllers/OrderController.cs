﻿using DataMarketAPI.Services;
using DataMarketAPI.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderController : Controller
    {
        private readonly OrderService _orderService;

        public OrderController(OrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> BuyAsync([FromBody] OrderViewModel vm)
        {
            vm.UserId = HttpContext.GetUserId();
            return Ok(await _orderService.Order(vm));
        }

        [HttpGet("{metricId}")]
        public async Task<IActionResult> GetOrderDetails(Guid metricId)
        {
            return Ok(await _orderService.GetOrderDetails(metricId, HttpContext.GetUserId()));
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
        {
            return Ok(await _orderService.GetOrders(HttpContext.GetUserId()));
        }
    }
}