using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class QueryLogController : Controller
    {
        private readonly QueryLogService _queryLogService;

        public QueryLogController(QueryLogService queryLogService)
        {
            _queryLogService = queryLogService;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> All()
        {
            User userContainer = new User();
            userContainer.CompanyId = HttpContext.GetCompanyId();
            userContainer.Id = HttpContext.GetUserId();

            return Ok(await _queryLogService.GetByCompanyWithUserFallback(userContainer));
        }
    }
}