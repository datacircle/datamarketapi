using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RatingController : Controller
    {
        private readonly RatingService _ratingService;

        public RatingController(RatingService ratingService)
        {
            _ratingService = ratingService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] RatingModel rating)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            rating.Owner = HttpContext.GetUserId();

            try
            {
                double avg = await _ratingService.Add(rating, HttpContext.GetCompanyId());
                return Ok(avg);
            }
            catch (HandledException e)
            {
                return HandleException(e);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] RatingModel rating)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            rating.Owner = HttpContext.GetUserId();

            try
            {
                double avg = await _ratingService.Update(rating, HttpContext.GetCompanyId());
                return Ok(avg);
            }
            catch (HandledException e)
            {
                return HandleException(e);
            }
        }

        private IActionResult HandleException(HandledException e)
        {
            switch (e.throwResponseLevel)
            {
                case HandledException.ThrowResponseLevel.Unathorized:
                    return Unauthorized();

                case HandledException.ThrowResponseLevel.BadRequest:
                    return BadRequest(e.Message);

                case HandledException.ThrowResponseLevel.Ok:
                    return Ok();

                default:
                    return BadRequest();
            }
        }
    }
}