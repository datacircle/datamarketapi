using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class RequestCommentController : Controller
    {
        private readonly RequestCommentService _requestCommentService;

        public RequestCommentController(RequestCommentService requestCommentService)
        {
            _requestCommentService = requestCommentService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            IEnumerable<RequestCommentModel> request = await _requestCommentService.GetByRequest(new RequestCommentModel { RequestId = id });

            if (request == null)
            {
                return NotFound();
            }

            return Ok(request);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] CommentModel comment)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            RequestCommentModel mc = new RequestCommentModel
            {
                RequestId = comment.TargetId,
                Value = comment.Value,
                Owner = HttpContext.GetUserId()
            };

            await _requestCommentService.Create(mc);

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] CommentModel comment)
        {
            RequestCommentModel mc = new RequestCommentModel
            {
                RequestId = comment.TargetId,
                Value = comment.Value,
                Owner = HttpContext.GetUserId()
            };

            await _requestCommentService.Delete(mc);

            return Ok();
        }
    }
}