using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    public class RequestController : Controller
    {
        private readonly RequestService _requestService;

        public RequestController(RequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(bool filterForAll = false)
        {
            if (filterForAll)
            {
                IEnumerable<RequestModel> requestCollection = await _requestService.GetAllUncensored();

                if (requestCollection == null)
                {
                    return BadRequest("No Data found.");
                }

                return Ok(requestCollection);
            }
            else
            {
                RequestModel requestContainer = new RequestModel { CompanyId = HttpContext.GetCompanyId(), Owner = HttpContext.GetUserId() };
                IEnumerable<RequestModel> requestCollection = await _requestService.GetAll(requestContainer);

                if (requestCollection == null)
                {
                    return BadRequest("No Data found.");
                }

                return Ok(requestCollection);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id, bool filterForAll = false)
        {
            if (filterForAll)
            {
                RequestModel request = await _requestService.GetUncensored(new RequestModel { ID = id });

                if (request == null)
                {
                    return NotFound();
                }

                return Ok(request);
            }
            else
            {
                RequestModel requestContainer = new RequestModel { CompanyId = HttpContext.GetCompanyId(), Owner = HttpContext.GetUserId() };

                requestContainer.ID = id;
                RequestModel request = await _requestService.Get(requestContainer);

                if (
                    request == null ||
                    request.CompanyId != HttpContext.GetCompanyId()
                   )
                {
                    return NotFound();
                }

                return Ok(request);
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] RequestModel request)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Invalid Data");
            }

            request.CompanyId = HttpContext.GetCompanyId();
            request.Owner = HttpContext.GetUserId();

            request = await _requestService.Create(request);

            return Ok(request);
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(Guid id, [FromBody] RequestModel request)
        {
            if (id != request.ID)
            {
                return NotFound("Id's do not match.");
            }

            request.CompanyId = HttpContext.GetCompanyId();
            request.Owner = HttpContext.GetUserId();

            await _requestService.Update(request);

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] RequestModel request)
        {
            request.CompanyId = HttpContext.GetCompanyId();
            request.Owner = HttpContext.GetUserId();

            await _requestService.Delete(request);

            return Ok();
        }
    }
}