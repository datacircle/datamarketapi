using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RequestVoteController : Controller
    {
        private readonly RequestVoteService _requestvoteService;

        public RequestVoteController(RequestVoteService requestvoteService)
        {
            _requestvoteService = requestvoteService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Vote(Guid id, bool upvote = false, bool downvote = false, bool unvote = false)
        {
            if (upvote)
            {
                await _requestvoteService.Upvote(new RequestVoteModel { Vote = RequestVoteModel.VoteType.Upvote, RequestId = id, Owner = HttpContext.GetUserId() });
            }
            else if (downvote)
            {
                await _requestvoteService.Downvote(new RequestVoteModel { Vote = RequestVoteModel.VoteType.Downvote, RequestId = id, Owner = HttpContext.GetUserId() });
            }
            else if (unvote)
            {
                await _requestvoteService.Unvote(new RequestVoteModel { RequestId = id, Owner = HttpContext.GetUserId() });
            }

            return Ok();
        }
    }
}