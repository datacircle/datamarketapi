using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketAPI.Controllers
{
    public class StatusController : Controller
    {
        [HttpGet]
        [Route("api/[controller]/[action]")]
        [AllowAnonymous]
        public IActionResult heartbeat()
        {
            return Ok();
        }
    }
}