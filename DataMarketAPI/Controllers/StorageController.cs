﻿using DataMarketAPI.Extensions;
using DataMarketAPI.Filters;
using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StorageController : Controller
    {
        private readonly UploadService _uploadService;
        private readonly MetricService _metricService;

        private static readonly FormOptions _defaultFormOptions = new FormOptions();

        public StorageController(UploadService uploadService, MetricService metricService)
        {
            _uploadService = uploadService;
            _metricService = metricService;
        }

        [HttpPost]
        public async Task<IActionResult> Use([FromBody] FileShareModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            model.UserId = HttpContext.GetUserId();

            MetricModel _metric = new MetricModel { Owner = model.UserId, ID = model.MetricId, CompanyId = HttpContext.GetCompanyId() };
            _metric = await _metricService.Get(_metric);
            _metric.FileName = model.FileName;
            await _metricService.Update(_metric);

            _uploadService.UploadFile(model);

            return Ok();
        }

        [HttpPost("{id}")]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> Upload()
        {
            string displayUrl = HttpContext.Request.GetDisplayUrl();
            var arrayUrl = displayUrl.Split('/');
            string metricId = arrayUrl.ToList().Last();

            if (!this.IsMultipartContentType(Request.ContentType))
            {
                return BadRequest($"Expected a multipart request, but got {Request.ContentType}");
            }

            string targetFilePath = null;

            var boundary = this.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();

            while (section != null)
            {
                ContentDispositionHeaderValue contentDisposition;
                var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                if (this.HasFileContentDisposition(contentDisposition))
                {
                    targetFilePath = Path.GetTempFileName();
                    using (FileStream targetStream = System.IO.File.Create(targetFilePath))
                    {
                        await section.Body.CopyToAsync(targetStream);
                        await targetStream.FlushAsync();
                    }

                    //await _cloudStorageService.CreateFile(HttpContext.GetUserId().ToString(), metricId, contentDisposition.FileName, section.Body);
                }

                section = await reader.ReadNextSectionAsync();
            }

            return Ok("File Uploaded");
        }
    }
}