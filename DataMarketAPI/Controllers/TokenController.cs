using DataMarketAPI.Models;
using DataMarketAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DataMarketAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TokenController : Controller
    {
        private readonly UserManager<User> _userManager;

        private readonly TokenService _tokenService;

        public TokenController(UserManager<User> userManager, TokenService tokenService)
        {
            _userManager = userManager;
            _tokenService = tokenService;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetActionToken()
        {
            User user = await _userManager.GetUserAsync(HttpContext.User);

            if (user == null)
            {
                return BadRequest();
            }

            return Ok(_tokenService.GenerateToken(HttpContext, user));
        }
    }
}