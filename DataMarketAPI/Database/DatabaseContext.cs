using DataMarketAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DataMarketAPI.Database
{
    public class DatabaseContext : IdentityDbContext<User, Role, int>
    {
        public DatabaseContext(DbContextOptions options)
            : base(options) { }

        public DbSet<CompanyModel> Company { get; set; }
        public DbSet<DatasourceModel> Datasource { get; set; }
        public DbSet<MetricModel> Metric { get; set; }
        public DbSet<NewsletterModel> Newsletter { get; set; }
        public DbSet<QueryLogModel> QueryLog { get; set; }
        public DbSet<InsertLogModel> InsertLog { get; set; }
        public DbSet<RequestBetaModel> RequestBeta { get; set; }
        public DbSet<RequestModel> Request { get; set; }
        public DbSet<RatingModel> Rating { get; set; }
        public DbSet<MetricTransferModel> MetricTransfer { get; set; }
        public DbSet<RequestCommentModel> RequestComment { get; set; }
        public DbSet<MetricCommentModel> MetricComment { get; set; }
        public DbSet<RequestVoteModel> RequestVote { get; set; }
        public DbSet<MetricAccessMapModel> MetricAccessMap { get; set; }
        public DbSet<FreeDownloadModel> FreeDownload { get; set; }
        public DbSet<ProductHutSubModel> ProductHubSub { get; set; }
        public DbSet<CategoyModel> Category { get; set; }
        public DbSet<HashTagModel> HashTag { get; set; }
        public DbSet<HashTagToMetricModel> HashTagMetric { get; set; }
        public DbSet<OrderModel> Order { get; set; }
        public DbSet<InvoiceModel> Invoice { get; set; }
        public DbSet<CountryModel> Country { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Role>(entity => entity.ToTable("Roles"));
            builder.Entity<User>(entity => entity.ToTable("Users"));
            builder.Entity<User>().Ignore(c => c.UserName);

            builder.Entity<IdentityUserClaim<int>>(entity => entity.ToTable("UserClaims"));
            builder.Entity<IdentityUserLogin<int>>(entity => entity.ToTable("UserLogins"));
            builder.Entity<IdentityUserToken<int>>(entity => entity.ToTable("UserTokens"));
            builder.Entity<IdentityRoleClaim<int>>(entity => entity.ToTable("RoleClaims"));

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            builder.Entity<RatingModel>().HasIndex("Owner", "MetricID");

            base.OnModelCreating(builder);
        }
    }
}