﻿using DataMarketAPI.Models;
using DataMarketAPI.Services;
using DataMarketAPI.ViewModel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Database
{
    public static class DevelopmentSeed
    {
        public static async Task SeedAsync(IServiceProvider services)
        {
            using (DatabaseContext context = services.GetRequiredService<DatabaseContext>())
            {
                await SystemUser(context, services);
                await DummyUsers(context, services);

                // !! ---------- If adding a system Datasouce be carefull to edit the DatasoureRepository GET ALL Function --------- !!
                await DatasourceInit(context, services);
                await CategoriesAsync(context);
            }
        }

        private static async Task CategoriesAsync(DatabaseContext context)
        {
            List<string> categories = new List<string>
            {
                "Other",
                "Agriculture / Manufacturing",
                "Technology",
                "Sports",
                "Soial Media",
                "Retails / Shops",
                "Marketing / Advertisment / Sales",
                "Startups",
                "Tourism & Hospitality & Travel",
                "Transportation",
                "Automotive",
                "Restaurant / Deliery / Catering",
                "Economy / Trade / Finanials",
                "Entertainment",
                "Health / Beuaty",
                "Science / Education",
                "Goverment / Politics",
                "Civil",
                "Guides"
            };

            List<CategoyModel> _c = new List<CategoyModel>();

            foreach (string category in categories)
            {
                _c.Add(new CategoyModel { Name = category, UsageCount = 0 });
            }

            if (context.Category.Count() == 0)
            {
                await context.AddRangeAsync(_c);
                await context.SaveChangesAsync();
            }
        }

        public static async Task SystemUser(DatabaseContext context, IServiceProvider services)
        {
            AccountService accountService = services.GetRequiredService<AccountService>();

            Guid password = Guid.NewGuid();
            Guid companyName = Guid.NewGuid();

            if (context.Users.FirstOrDefault(u => u.FirstName == "System" && u.Email == "support@datacircle.io") == null)
            {
                await accountService.Register(new RegisterViewModel()
                {
                    FirstName = "System",
                    LastName = "System",
                    Email = "support@datacircle.io",
                    Password = password.ToString() + "aA1!",
                    Password2 = password.ToString() + "aA1!",
                    Company = "SystemCompany_" + companyName.ToString()
                }, false);

                context.SaveChanges();
            }
        }

        public static async Task DummyUsers(DatabaseContext context, IServiceProvider services)
        {
            AccountService accountService = services.GetRequiredService<AccountService>();

            if (context.Users.Count() == 1)
            {
                List<RegisterViewModel> dummyUsers = new List<RegisterViewModel> {
                    new RegisterViewModel {
                        FirstName = "Chris",
                        LastName = "Zioutas",
                        Email = "chriszioutas@datacircle.io",
                        Password = "Dummy!1",
                        Password2 = "Dummy!1",
                        Company = "DataCircle"
                    },
                    new RegisterViewModel {
                        FirstName = "Jon",
                        LastName = "Doe",
                        Email = "z.chris.92@gmail.com",
                        Password = "Dummy!1",
                        Password2 = "Dummy!1",
                        Company = ""
                    }
                };

                foreach (RegisterViewModel vm in dummyUsers)
                {
                    await accountService.Register(vm, false);
                }
            }
        }

        // !! ---------- If adding a system Datasouce be carefull to edit the DatasoureRepository GET ALL Function --------- !!
        public static async Task DatasourceInit(DatabaseContext context, IServiceProvider services)
        {
            DatasourceService datasourceService = services.GetRequiredService<DatasourceService>();
            CompanyModel systemCompany = context.Company.Where(c => c.Name.Contains("SystemCompany_")).FirstOrDefault();
            User systemUser = context.Users.FirstOrDefault(u => u.FirstName == "System" && u.LastName == "System" && u.Email == "support@datacircle.io");

            if (context.Datasource.FirstOrDefault(ds => ds.Title == "DownloadAsFile") == null)
            {
                await context.Datasource.AddAsync(new DatasourceModel
                {
                    Title = "DownloadAsFile",
                    Description = "Datasource to download data as Files",
                    Open = true,
                    Type = Models.Type.FileDownload,
                    Host = string.Empty,
                    Username = string.Empty,
                    Password = string.Empty,
                    Database = string.Empty,
                    Port = 0,
                    CompanyId = systemCompany.Id,
                    Owner = systemUser.Id
                });
            }

            if (context.Datasource.FirstOrDefault(ds => ds.Title == "Upload File") == null)
            {
                await context.Datasource.AddAsync(new DatasourceModel
                {
                    Title = "Upload File",
                    Description = "Datasource to upload data as Files",
                    Open = true,
                    Type = Models.Type.FileUploadGeneric,
                    Host = string.Empty,
                    Username = string.Empty,
                    Password = string.Empty,
                    Database = string.Empty,
                    Port = 0,
                    CompanyId = systemCompany.Id,
                    Owner = systemUser.Id
                });
            }

            if (context.Datasource.FirstOrDefault(ds => ds.Title == "Link To Dataset") == null)
            {
                await context.Datasource.AddAsync(new DatasourceModel
                {
                    Title = "Link To Dataset",
                    Description = "Datasource to be used as Link",
                    Open = true,
                    Type = Models.Type.Link,
                    Host = string.Empty,
                    Username = string.Empty,
                    Password = string.Empty,
                    Database = string.Empty,
                    Port = 0,
                    CompanyId = systemCompany.Id,
                    Owner = systemUser.Id
                });
            }

            if (context.Datasource.FirstOrDefault(ds => ds.Title == "Airtable") == null)
            {
                await context.Datasource.AddAsync(new DatasourceModel
                {
                    Title = "Airtable Embed",
                    Description = "Airtable Embed",
                    Open = true,
                    Type = Models.Type.Airtable,
                    Host = string.Empty,
                    Username = string.Empty,
                    Password = string.Empty,
                    Database = string.Empty,
                    Port = 0,
                    CompanyId = systemCompany.Id,
                    Owner = systemUser.Id
                });
            }

            await context.SaveChangesAsync();
        }
    }
}