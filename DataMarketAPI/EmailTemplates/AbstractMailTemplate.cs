namespace DataMarketAPI.EmailTemplates
{
    /// <summary>
    ///
    /// </summary>
    public abstract class AbstractMailTemplate : IMailTemplate
    {
        private string _contentHtml;
        private string _contentText;

        /// <summary>
        ///
        /// </summary>
        public string Recipient { get; set; } = null;

        /// <summary>
        ///
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual string ContentHtml
        {
            get
            {
                if (_contentHtml == null)
                {
                    PopulateContent();
                }
                return _contentHtml;
            }
            set
            {
                _contentHtml = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public virtual string ContentText
        {
            get
            {
                if (_contentText == null)
                {
                    PopulateContent();
                }

                return _contentText;
            }
            set { _contentText = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public string ContentScaffold { get; set; } = null;

        /// <summary>
        ///
        /// </summary>
        public abstract void PopulateContent();
    }
}