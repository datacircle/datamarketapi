﻿using DataMarketAPI.ViewModel;
using System;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    /// <summary>
    ///
    /// </summary>
    public class AccountRegistrationTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/AccountRegistrationInline.html";
        private string _pathText = @"/EmailTemplates/TEXT/AccountRegistration.txt";
        private string AccountHolderName;
        private string _preview = "Account Registration 🚀";

        /// <summary>
        ///
        /// </summary>
        /// <param name="registerVM"></param>
        public AccountRegistrationTemplate(RegisterViewModel registerVM)
        {
            AccountHolderName = registerVM.LastName + ", " + registerVM.FirstName;
            Recipient = registerVM.Email;
            base.Subject = "DataCircle, Welcome to the family!";
        }

        /// <summary>
        ///
        /// </summary>
        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            Console.WriteLine(Directory.GetCurrentDirectory() + _pathHtml);
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@preview@@", _preview);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@preview@@", _preview);
            ContentText = content;
        }
    }
}