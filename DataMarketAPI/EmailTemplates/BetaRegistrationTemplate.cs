﻿using DataMarketAPI.Models;
using System;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    public class BetaRegistrationTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/BetaRegistrationInline.html";
        private string _pathText = @"/EmailTemplates/TEXT/BetaRegistration.txt";
        private string AccountHolderName;
        private string _preview = "Account Registration";

        public BetaRegistrationTemplate(RequestBetaModel vm)
        {
            AccountHolderName = vm.LastName + ", " + vm.FirstName;
            Recipient = vm.Email;
            base.Subject = "DataCircle, Welcome to the family! 👪";
        }

        /// <summary>
        ///
        /// </summary>
        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            Console.WriteLine(Directory.GetCurrentDirectory() + _pathHtml);
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@preview@@", _preview);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@preview@@", _preview);
            ContentText = content;
        }
    }
}