using DataMarketAPI.Models;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    /// <summary>
    ///
    /// </summary>
    public class DoubleOptInTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/DoubleOptInInline.html";
        private string _pathText = @"/EmailTemplates/TEXT/DoubleOptIn.txt";
        private string AccountHolderName;
        private string _preview = "Newsletter Confirmation.";

        private string _token;

        /// <summary>
        ///
        /// </summary>
        /// <param name="newsletter"></param>
        public DoubleOptInTemplate(NewsletterModel newsletter)
        {
            AccountHolderName = "Sir/Madam";
            Recipient = newsletter.Email;
            _token = System.Net.WebUtility.UrlEncode(newsletter.DoubleOptInToken);
            base.Subject = "DataCircle, Please confirm your newsletter subscription.";
        }

        /// <summary>
        ///
        /// </summary>
        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@token@@", _token);
            content = content.Replace("@@preview@@", _preview);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@token@@", _token);
            content = content.Replace("@@preview@@", _preview);
            ContentText = content;
        }
    }
}