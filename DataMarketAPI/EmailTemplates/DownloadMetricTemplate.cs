﻿using DataMarketAPI.Models;
using System;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    public class DownloadMetricTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/DownloadMetric.html";
        private string _pathText = @"/EmailTemplates/TEXT/DownloadMetric.txt";
        private string Title;
        private Guid Id;

        public DownloadMetricTemplate(FreeDownloadModel model)
        {
            Title = model.Metric.Title;
            Id = model.ID;
            Recipient = model.Email;
            base.Subject = $"Data Set Download Link 💾 {Title}";
        }

        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            Console.WriteLine(Directory.GetCurrentDirectory() + _pathHtml);
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@Title@@", Title);
            content = content.Replace("@@Id@@", Id.ToString());
            content = content.Replace("*|MC_PREVIEW_TEXT|*", Title);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@Title@@", Title);
            content = content.Replace("@@Id@@", Id.ToString());
            ContentText = content;
        }
    }
}