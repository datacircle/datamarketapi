using DataMarketAPI.ViewModel;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    /// <summary>
    ///
    /// </summary>
    public class ForgotPasswordTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/ForgotPasswordInline.html";
        private string _pathText = @"/EmailTemplates/TEXT/ForgotPassword.txt";
        private string _token;
        private string AccountHolderName;
        private string _preview = "Password Reset";

        /// <summary>
        ///
        /// </summary>
        /// <param name="forgotPasswordVM"></param>
        public ForgotPasswordTemplate(ForgotPasswordViewModel forgotPasswordVM)
        {
            AccountHolderName = forgotPasswordVM.LastName + ", " + forgotPasswordVM.FirstName;
            _token = forgotPasswordVM.Token;
            Recipient = forgotPasswordVM.Email;
            base.Subject = "DataCircle, Forgot Password!";
        }

        /// <summary>
        ///
        /// </summary>
        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@token@@", _token);
            content = content.Replace("@@preview@@", _preview);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@AccountHolderName@@", AccountHolderName);
            content = content.Replace("@@token@@", _token);
            content = content.Replace("@@preview@@", _preview);
            ContentText = content;
        }
    }
}