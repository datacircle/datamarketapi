namespace DataMarketAPI.EmailTemplates
{
    /// <summary>
    ///
    /// </summary>
    public interface IMailTemplate
    {
        /// <summary>
        ///
        /// </summary>
        string Recipient { get; set; }

        /// <summary>
        ///
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        ///
        /// </summary>
        string ContentHtml { get; set; }

        /// <summary>
        ///
        /// </summary>
        string ContentText { get; set; }
    }
}