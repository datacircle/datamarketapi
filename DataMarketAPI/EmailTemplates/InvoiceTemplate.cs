﻿using DataMarketAPI.ViewModels;
using System.IO;

namespace DataMarketAPI.EmailTemplates
{
    public class InvoiceTemplate : AbstractMailTemplate
    {
        private string _pathHtml = @"/EmailTemplates/HTML/Invoice.html";
        private string _pathText = @"/EmailTemplates/TEXT/Invoice.txt";
        private string _data;

        public InvoiceTemplate(OrderViewModel vm)
        {
            _data = vm.MetricId + " _ " + vm.UserId.ToString();
            Recipient = "chriszioutas@datacircle.io";
            base.Subject = "🌟 🚀 🎉 Invoice Arrived!";
        }
        
        public override void PopulateContent()
        {
            PopulateHtml();
            PopulateText();
        }

        private void PopulateHtml()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathHtml);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@data@@", _data);
            ContentHtml = content;
        }

        private void PopulateText()
        {
            string[] contentArray = System.IO.File.ReadAllLines(Directory.GetCurrentDirectory() + _pathText);
            string content = string.Join(" ", contentArray);

            content = content.Replace("@@data@@", _data);
            ContentText = content;
        }
    }
}