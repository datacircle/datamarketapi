using DataMarketAPI.Application;
using DataMarketAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace DataMarketAPI
{
    public static class HttpContextExtensions
    {
        public static bool IsUserLoggedIn(this HttpContext httpContext)
        {
            return (httpContext.User != null) &&
                httpContext.User.Identity.IsAuthenticated &&
                httpContext.Session.GetString("IdentityCookieId") != null;
        }

        public static User GetCurrentUser(this HttpContext httpContext)
        {
            return new Models.User
            {
                Id = GetUserId(httpContext),
                CompanyId = GetCompanyId(httpContext)
            };
        }

        public static int? GetCompanyId(this HttpContext httpContext)
        {
            string _key = CustomClaimTypes.CompanyId;
            string _value = GetClaimValue(httpContext, _key);

            if (_value == null || _value == String.Empty)
            {
                return null;
            }

            if (!Int32.TryParse(_value, out int x))
            {
                throw new HandledException("Could not parse " + _key);
            }

            return x;
        }

        public static string GetCompanyName(this HttpContext httpContext)
        {
            return GetClaimValue(httpContext, CustomClaimTypes.CompanyName);
        }

        public static string GetFirstName(this HttpContext httpContext)
        {
            return GetClaimValue(httpContext, CustomClaimTypes.FirstName);
        }

        public static string GetLastName(this HttpContext httpContext)
        {
            return GetClaimValue(httpContext, CustomClaimTypes.LastName);
        }

        public static string GetEmail(this HttpContext httpContext)
        {
            return GetClaimValue(httpContext, CustomClaimTypes.Email);
        }

        public static int GetUserId(this HttpContext httpContext)
        {
            string _key = CustomClaimTypes.UserId;

            if (!Int32.TryParse(GetClaimValue(httpContext, _key), out int x))
            {
                return int.MinValue + 1;
            }

            return x;
        }

        public static string GetClaimValue(HttpContext context, string ClaimType)
        {
            context.Request.Headers.TryGetValue("Authorization", out var accessToken);

            if (string.IsNullOrEmpty(accessToken))
            {
                return null;
            }

            accessToken = accessToken.ToString().Replace("Bearer", "").Trim();

            if (string.IsNullOrEmpty(accessToken))
            {
                return null;
            }


            JwtSecurityToken jwt = new JwtSecurityToken(accessToken);

            return jwt.Claims.FirstOrDefault(c => c.Type == ClaimType).Value;

            //return context.User.Claims.FirstOrDefault(c => c.Type == ClaimType).Value;
        }
    }
}