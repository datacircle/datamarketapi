using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace DataMarketAPI.Library.Connection
{
    public abstract class Connection
    {
        private Dictionary<string, string> TypeMap;

        public abstract string GetColumnType(string type);

        public abstract string GetColumnName(string name);

        public abstract Task<bool> CheckConnection(DatasourceModel datasource);

        public abstract Task<bool> Connect(DatasourceModel datasource);

        public abstract ConnectionState GetState();

        public abstract void Close();

        public abstract Task<long> InsertPrepare(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc);

        public abstract Task<bool> InsertRaw(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc);

        public abstract Task<DbDataReader> ExecuteCmd(DatasourceModel datasource, string query);

        public abstract string GenerateTable(string tableName, Dictionary<string, string> structure);

        public abstract string GenerateInsert(DataStoreCollectionModel dsc, string tableName);

        protected abstract string GenerateConnectionString(DatasourceModel datasource);

        public abstract string GetDataValue(string rawValue, string type);

        public abstract DbType GetDbType(string columnType);
    }
}