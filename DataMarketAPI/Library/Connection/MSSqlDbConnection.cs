using DataMarketAPI.Application;
using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Library.Connection
{
    public class MSSqlDbConnection : Connection
    {
        private Dictionary<string, string> TypeMap = new Dictionary<string, string> {
            { "String", "[varchar](250)" },
            { "UInt32", "[int]" },
            { "DateTime", "[varchar](250)" },
            { "Byte", "[bit]" },
            { "Boolean", "[bit]" },
            { "Int16", "[smallint]" },
            { "Int32", "[int]" },
            { "Int64", "[bigint]" },
            { "Single", "[float]" },
            { "Double", "[real]" },
            { "Decimal", "[decimal]" }
        };

        private Dictionary<string, DbType> DbTypeMap = new Dictionary<string, DbType> {
            { "String", DbType.String },
            { "UInt32", DbType.UInt32 },
            { "DateTime", DbType.String },
            { "Byte", DbType.Byte },
            { "Boolean", DbType.Boolean },
            { "Int16", DbType.Int16 },
            { "Int32", DbType.Int32 },
            { "Int64", DbType.Int64 },
            { "Single", DbType.Single },
            { "Double", DbType.Double },
            { "Decimal", DbType.Decimal }
        };

        // DbType.

        private List<string> columnNameBlackList = new List<string>() {
            "int", "datetime2", "datetime", "datetimeoffset", "smallint" , "int" , "tinyint" , "bigint" , "float" , "real" , "varchar" , "bit"
        };

        public SqlConnection _con;
        private DatasourceModel _datasource;

        public override async Task<bool> CheckConnection(DatasourceModel datasource)
        {
            try
            {
                bool result = await Connect(datasource);
                if (result)
                {
                    Close();
                }
                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override void Close()
        {
            try
            {
                if (_con != null && _con.State != ConnectionState.Closed)
                {
                    _con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("------------------ FU UUUU -----------");
            }
        }

        public override async Task<bool> Connect(DatasourceModel datasource)
        {
            try
            {
                _datasource = datasource;
                _con = new SqlConnection(GenerateConnectionString(datasource));
                await _con.OpenAsync();
                return true;
            }
            catch (Exception e)
            {
                throw new HandledException($"Could not connect to Datasource: {datasource.ID}");
            }
        }

        public override async Task<DbDataReader> ExecuteCmd(DatasourceModel datasource, string query)
        {
            try
            {
                await Connect(datasource);
                DbCommand cmd = _con.CreateCommand();
                cmd.CommandText = query;
                cmd.Prepare();
                return await cmd.ExecuteReaderAsync();
            }
            catch (Exception e)
            {
                Close();
                throw e;
            }
        }

        public override async Task<long> InsertPrepare(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc)
        {
            DbTransaction trans = null;
            List<string> columnNames = dsc.Structure.Keys.ToList();
            List<string> columnTypes = dsc.Structure.Values.ToList();
            int ColumnTypeLength = columnTypes.Count - 1;
            int ColumnTypeCount = columnTypes.Count;
            int totalDataCount = dsc.Data.Count;

            var watch = System.Diagnostics.Stopwatch.StartNew();

            try
            {
                await Connect(datasource);
                using (DbCommand cmd = _con.CreateCommand())
                {
                    cmd.CommandText = insertStmt;
                    trans = _con.BeginTransaction();
                    cmd.Transaction = trans;
                    DbParameter param;

                    for (int y = 0; y < dsc.Data.Count; y++)
                    {
                        var row = dsc.Data[y];
                        for (int i = 0; i < row.Count; i++)
                        {
                            if (y == 0)
                            {
                                param = cmd.CreateParameter();
                                param.ParameterName = $"@{i}aa";
                                param.DbType = GetDbType(columnTypes[i]);                                
                                param.Value = GetDataValue(row[i], columnTypes[i]);
                                param.Size = 250;
                                cmd.Parameters.Add(param);
                            }
                            else
                            {
                                cmd.Parameters[$"@{i}aa"].Value = GetDataValue(row[i], columnTypes[i]);
                            }
                        }

                        cmd.Prepare();
                        await cmd.ExecuteNonQueryAsync();

                        if (y % 100 == 0)
                        {
                            cmd.Transaction.Commit();
                            cmd.Transaction = _con.BeginTransaction();
                        }
                    }

                    cmd.Transaction.Commit();
                }

                watch.Stop();

                return watch.ElapsedMilliseconds;
            }
            catch (Exception e)
            {
                Close();
                throw e;
            }
        }

        public override string GetDataValue(string rawValue, string type)
        {
            rawValue = rawValue.ToLower();

            if (type.ToLower() == "boolean")
            {
                if (rawValue != "1" || rawValue == "false")
                {
                    return "0";
                }
                else
                {
                    return "1";
                }
            }

            return rawValue;
        }

        
        public override Task<bool> InsertRaw(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc)
        {
            throw new NotImplementedException();
        }

        public override string GenerateInsert(DataStoreCollectionModel dsc, string tableName)
        {
            
            // INSERT INTO [dbo].[table_name]
            // ([col1]... )
            // VALUES
            //( val1, val2 .. )
            
            string insertStmt = $"INSERT INTO [dbo].[{tableName}] ( [" + string.Join("],[", dsc.Structure.Select(x => GetColumnName(x.Key))) + "] ) VALUES (";

            for (int i = 0; i < dsc.Structure.Keys.Count; i++)
            {
                insertStmt += $"@{i}aa,";
            }

            insertStmt = insertStmt.TrimEnd(',');
            insertStmt += ");";

            return insertStmt;
        }

        public override string GenerateTable(string tableName, Dictionary<string, string> structure)
        {
            // IF OBJECT_ID(N'dbo.Cars', N'U') IS NULL BEGIN CREATE TABLE dbo.Cars(Name varchar(64) not null); END;

            string createStmt = $"IF OBJECT_ID(N'dbo.{tableName}', N'U') IS NULL BEGIN CREATE TABLE dbo.{tableName}(";

            foreach (KeyValuePair<string, string> kv in structure)
            {
                createStmt += GetColumnName(kv.Key) + " " + GetColumnType(kv.Value) + " null,";
            }

            createStmt = createStmt.TrimEnd(',');
            createStmt += "); END;";

            return createStmt;            
        }

        public override ConnectionState GetState()
        {
            if (_con != null)
            {
                return _con.State;
            }

            return ConnectionState.Closed;
        }

        protected override string GenerateConnectionString(DatasourceModel datasource = null)
        {
            DatasourceModel _ds = datasource ?? _datasource;

            string host = _ds.Host;
            string port = _ds.Port.ToString();
            string user = _ds.Username;
            string password = _ds.Password;
            string database = _ds.Database;

            // Server=myServerAddress;Database=myDataBase;User Id=myUsername;
            // Password = myPassword;

            string _connectionString = "Server=" + host + ";";
            _connectionString += "Database=" + database + ";";
            _connectionString += "User Id=" + user + ";";
            _connectionString += "Password=" + password + ";";            
            return _connectionString;
        }

        public override string GetColumnName(string name)
        {
            if (columnNameBlackList.Contains(name.ToUpper()))
            {
                return $"DC_{name}";
            }

            return name;
        }

        public override string GetColumnType(string type)
        {
            if (TypeMap.TryGetValue(type, out string val))
            {
                return val;
            }

            return "[varchar](250)";

            throw new HandledException($"Could not map {type} to any Table Structure Type.");
        }

        public override DbType GetDbType(string columnType)
        {
            if (DbTypeMap.TryGetValue(columnType, out DbType val))
            {
                return val;
            }

            return DbType.String;

            throw new HandledException($"Could not map {columnType} to any Table Structure Type.");
        }
    }
}