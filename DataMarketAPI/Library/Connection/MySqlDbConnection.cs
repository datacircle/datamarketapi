using DataMarketAPI.Application;
using DataMarketAPI.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Library.Connection
{
    public class MySqlDbConnection : Connection
    {
        private Dictionary<string, string> TypeMap = new Dictionary<string, string> {
            { "String", "VARCHAR(250)" },
            { "UInt32", "INT" },
            { "DateTime", "TIMESTAMP" },
            { "Boolean", "TINYINT" },
            { "Int16", "SMALLINT" },
            { "Int32", "INT" },
            { "Int64", "BIGINT" },
            { "Single", "FLOAT" },
            { "Double", "DOUBLE" },
            { "Decimal", "DECIMAL" }
        };

        private List<string> columnNameBlackList = new List<string>() {
            "INT", "TIMESTAMP", "TINYINT" , "SMALLINT" , "MEDIUMINT" , "BIGINT" , "FLOAT" , "DOUBLE" , "REAL" , "DECIMAL"
        };

        public MySqlConnection _con;
        private DatasourceModel _datasource;

        public override async Task<bool> CheckConnection(DatasourceModel datasource)
        {
            try
            {
                return await Connect(datasource);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override void Close()
        {
            try
            {
                if (_con != null && _con.State != ConnectionState.Closed)
                {
                    _con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("------------------ FU UUUU -----------");
            }
        }

        public override async Task<bool> Connect(DatasourceModel datasource)
        {
            try
            {
                _datasource = datasource;
                _con = new MySqlConnection(GenerateConnectionString(datasource));
                await _con.OpenAsync();
                return true;
            }
            catch (Exception e)
            {
                throw new HandledException($"Could not connect to Datasource: {datasource.ID}");
            }
        }

        public override async Task<DbDataReader> ExecuteCmd(DatasourceModel datasource, string query)
        {
            try
            {
                await Connect(datasource);
                DbCommand cmd = _con.CreateCommand();
                cmd.CommandText = query;
                cmd.Prepare();
                return await cmd.ExecuteReaderAsync();
            }
            catch (Exception e)
            {
                Close();
                throw e;
            }
        }

        public override async Task<long> InsertPrepare(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc)
        {
            DbTransaction trans = null;
            List<string> columnNames = dsc.Structure.Keys.ToList();
            List<string> columnTypes = dsc.Structure.Values.ToList();
            int ColumnTypeLength = columnTypes.Count - 1;
            int ColumnTypeCount = columnTypes.Count;
            int totalDataCount = dsc.Data.Count;

            var watch = System.Diagnostics.Stopwatch.StartNew();

            try
            {
                await Connect(datasource);
                using (DbCommand cmd = _con.CreateCommand())
                {
                    cmd.CommandText = insertStmt;
                    trans = _con.BeginTransaction();
                    cmd.Transaction = trans;
                    DbParameter param;

                    for (int y = 0; y < dsc.Data.Count; y++)
                    {
                        var row = dsc.Data[y];
                        for (int i = 0; i < row.Count; i++)
                        {
                            if (y == 0)
                            {
                                param = cmd.CreateParameter();
                                param.ParameterName = $"@{i}aa";
                                param.Value = GetDataValue(row[i], columnTypes[i]);
                                cmd.Parameters.Add(param);
                            }
                            else
                            {
                                cmd.Parameters[$"@{i}aa"].Value = GetDataValue(row[i], columnTypes[i]);
                            }
                        }

                        cmd.Prepare();
                        await cmd.ExecuteNonQueryAsync();

                        if (y % 100 == 0)
                        {
                            cmd.Transaction.Commit();
                            cmd.Transaction = _con.BeginTransaction();
                        }
                    }

                    cmd.Transaction.Commit();
                }

                watch.Stop();

                return watch.ElapsedMilliseconds;
            }
            catch (Exception e)
            {
                Close();
                throw e;
            }
        }

        public override string GetDataValue(string rawValue, string type)
        {
            rawValue = rawValue.ToLower();

            if (type.ToLower() == "boolean")
            {
                if (rawValue != "1" || rawValue == "false")
                {
                    return "0";
                }
                else
                {
                    return "1";
                }
            }

            return rawValue;
        }

        public override string GenerateInsert(DataStoreCollectionModel dsc, string tableName)
        {
            // INSERT INTO table_name ( field1, field2,...fieldN )
            // VALUES
            // ( value1, value2,...valueN );

            string insertStmt = $"INSERT INTO `{tableName}` ( `" + string.Join("`,`", dsc.Structure.Select(x => GetColumnName(x.Key))) + "` ) VALUES (";

            for (int i = 0; i < dsc.Structure.Keys.Count; i++)
            {
                insertStmt += $"@{i}aa,";
            }

            insertStmt = insertStmt.TrimEnd(',');
            insertStmt += ");";

            return insertStmt;
        }

        public override string GenerateTable(string tableName, Dictionary<string, string> structure)
        {
            // CREATE TABLE table_name (
            //     column1 datatype,
            //     column2 datatype,
            //     column3 datatype,
            // ....
            // );

            string createStmt = $"CREATE TABLE IF NOT EXISTS {tableName} (";

            foreach (KeyValuePair<string, string> kv in structure)
            {
                createStmt += "`" + GetColumnName(kv.Key) + "` " + GetColumnType(kv.Value) + ",";
            }

            createStmt = createStmt.TrimEnd(',');
            createStmt += ");";

            return createStmt;
        }

        public override ConnectionState GetState()
        {
            if (_con != null)
            {
                return _con.State;
            }

            return ConnectionState.Closed;
        }

        protected override string GenerateConnectionString(DatasourceModel datasource = null)
        {
            DatasourceModel _ds = datasource ?? _datasource;

            string host = _ds.Host;
            string port = _ds.Port.ToString();
            string user = _ds.Username;
            string password = _ds.Password;
            string database = _ds.Database;

            string _connectionString = "host=" + host + ";";
            _connectionString += "port=" + port + ";";
            _connectionString += "user id=" + user + ";";
            _connectionString += "password=" + password + ";";
            _connectionString += "database=" + database + ";";
            _connectionString += "Allow User Variables=true;";
            return _connectionString;
        }

        public override string GetColumnName(string name)
        {
            if (columnNameBlackList.Contains(name.ToUpper()))
            {
                return $"DC_{name}";
            }

            return name;
        }

        public override string GetColumnType(string type)
        {
            if (TypeMap.TryGetValue(type, out string val))
            {
                return val;
            }

            throw new HandledException($"Could not map {type} to any Table Structure Type.");
        }

        public override Task<bool> InsertRaw(DatasourceModel datasource, string insertStmt, DataStoreCollectionModel dsc)
        {
            throw new NotImplementedException();
        }

        public override DbType GetDbType(string columnType)
        {
            throw new NotImplementedException();
        }
    }
}