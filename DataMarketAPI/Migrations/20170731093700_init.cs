﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", nullable: true),
                    NormalizedName = table.Column<string>(type: "varchar(100)", nullable: true),
                    PublicId = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Newsletter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DoubleOptInToken = table.Column<string>(nullable: true),
                    DoubleOptedIn = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", nullable: true),
                    Subscribed = table.Column<bool>(nullable: false),
                    UnsubscriptionToken = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Newsletter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RequestBeta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyUser = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestBeta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                });

            migrationBuilder.CreateTable(
                name: "CompanyToCompanyAccessMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    OriginCompanyID = table.Column<int>(nullable: false),
                    TargetCompanyId = table.Column<int>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyToCompanyAccessMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyToCompanyAccessMap_Company_OriginCompanyID",
                        column: x => x.OriginCompanyID,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyToCompanyAccessMap_Company_TargetCompanyId",
                        column: x => x.TargetCompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    ConsumerApiKey = table.Column<string>(nullable: true),
                    ConsumerSecretApiKey = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(type: "varchar(100)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsCompanyOwner = table.Column<bool>(nullable: false),
                    IsVerified = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(type: "varchar(100)", nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    MiddleName = table.Column<string>(type: "varchar(100)", nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    PublicId = table.Column<string>(nullable: true),
                    RoleId = table.Column<int>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    Token = table.Column<string>(type: "varchar(250)", nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    VerificationKey = table.Column<string>(type: "varchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Datasource",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    ConnectionStatus = table.Column<bool>(nullable: false),
                    ConnectionString = table.Column<string>(type: "varchar(250)", nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Database = table.Column<string>(type: "varchar(250)", nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", nullable: true),
                    Host = table.Column<string>(type: "varchar(250)", nullable: false),
                    Licence = table.Column<string>(nullable: true),
                    Open = table.Column<bool>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    Password = table.Column<string>(type: "text", nullable: false),
                    Port = table.Column<int>(nullable: false),
                    Title = table.Column<string>(type: "varchar(100)", nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(type: "varchar(250)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Datasource", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Datasource_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Datasource_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Metric",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    Cost = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DatasourceId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Interval = table.Column<int>(nullable: false),
                    LastPool = table.Column<DateTime>(nullable: false),
                    Licence = table.Column<string>(nullable: true),
                    Open = table.Column<bool>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    Processing = table.Column<bool>(nullable: false),
                    Query = table.Column<string>(type: "text", nullable: false),
                    Rating = table.Column<double>(nullable: false),
                    RequiresAuthorization = table.Column<bool>(nullable: false),
                    Structure = table.Column<string>(nullable: true),
                    Title = table.Column<string>(type: "varchar(100)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Metric", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Metric_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Metric_Datasource_DatasourceId",
                        column: x => x.DatasourceId,
                        principalTable: "Datasource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Metric_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetricCompanyAccessMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    MetricId = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricCompanyAccessMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricCompanyAccessMap_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricCompanyAccessMap_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetricTransfer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActionableUser = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DownloadAsFile = table.Column<bool>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    MetricID = table.Column<Guid>(nullable: false),
                    Owner = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    TargetCompanyID = table.Column<int>(nullable: true),
                    TargetDatasourceID = table.Column<Guid>(nullable: false),
                    TargetTableName = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricTransfer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricTransfer_Metric_MetricID",
                        column: x => x.MetricID,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricTransfer_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricTransfer_Company_TargetCompanyID",
                        column: x => x.TargetCompanyID,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricTransfer_Datasource_TargetDatasourceID",
                        column: x => x.TargetDatasourceID,
                        principalTable: "Datasource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QueryLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cached = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DataSize = table.Column<int>(nullable: false),
                    DatasourceID = table.Column<Guid>(nullable: false),
                    ErrorMessage = table.Column<string>(nullable: true),
                    IsSuccesful = table.Column<bool>(nullable: false),
                    MetricID = table.Column<Guid>(nullable: false),
                    QueryTime = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QueryLog_Datasource_DatasourceID",
                        column: x => x.DatasourceID,
                        principalTable: "Datasource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QueryLog_Metric_MetricID",
                        column: x => x.MetricID,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rating",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    MetricID = table.Column<Guid>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rating", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rating_Metric_MetricID",
                        column: x => x.MetricID,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rating_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Request",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AutoUpdate = table.Column<bool>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    CreateTable = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", nullable: true),
                    Fields = table.Column<string>(nullable: true),
                    Interval = table.Column<int>(nullable: false),
                    LastPool = table.Column<DateTime>(nullable: false),
                    Modifications = table.Column<string>(nullable: true),
                    Owner = table.Column<int>(nullable: false),
                    RefreshData = table.Column<bool>(nullable: false),
                    SourceId = table.Column<Guid>(nullable: false),
                    TargetId = table.Column<Guid>(nullable: false),
                    TargetTableName = table.Column<string>(type: "varchar(100)", nullable: true),
                    Title = table.Column<string>(type: "varchar(100)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Request", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Request_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Request_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Request_Metric_SourceId",
                        column: x => x.SourceId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Request_Datasource_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Datasource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InsertLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DataSize = table.Column<int>(nullable: false),
                    ErrorMessage = table.Column<string>(nullable: true),
                    InsertTime = table.Column<long>(nullable: false),
                    IsSuccesful = table.Column<bool>(nullable: false),
                    MetricTransferID = table.Column<int>(nullable: false),
                    Rows = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsertLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsertLog_MetricTransfer_MetricTransferID",
                        column: x => x.MetricTransferID,
                        principalTable: "MetricTransfer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyToCompanyAccessMap_OriginCompanyID",
                table: "CompanyToCompanyAccessMap",
                column: "OriginCompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyToCompanyAccessMap_TargetCompanyId",
                table: "CompanyToCompanyAccessMap",
                column: "TargetCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Datasource_CompanyId",
                table: "Datasource",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Datasource_Owner",
                table: "Datasource",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_InsertLog_MetricTransferID",
                table: "InsertLog",
                column: "MetricTransferID");

            migrationBuilder.CreateIndex(
                name: "IX_MetricCompanyAccessMap_CompanyId",
                table: "MetricCompanyAccessMap",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricCompanyAccessMap_MetricId",
                table: "MetricCompanyAccessMap",
                column: "MetricId");

            migrationBuilder.CreateIndex(
                name: "IX_Metric_CompanyId",
                table: "Metric",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Metric_DatasourceId",
                table: "Metric",
                column: "DatasourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Metric_Owner",
                table: "Metric",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_MetricTransfer_MetricID",
                table: "MetricTransfer",
                column: "MetricID");

            migrationBuilder.CreateIndex(
                name: "IX_MetricTransfer_Owner",
                table: "MetricTransfer",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_MetricTransfer_TargetCompanyID",
                table: "MetricTransfer",
                column: "TargetCompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_MetricTransfer_TargetDatasourceID",
                table: "MetricTransfer",
                column: "TargetDatasourceID");

            migrationBuilder.CreateIndex(
                name: "IX_QueryLog_DatasourceID",
                table: "QueryLog",
                column: "DatasourceID");

            migrationBuilder.CreateIndex(
                name: "IX_QueryLog_MetricID",
                table: "QueryLog",
                column: "MetricID");

            migrationBuilder.CreateIndex(
                name: "IX_Rating_MetricID",
                table: "Rating",
                column: "MetricID");

            migrationBuilder.CreateIndex(
                name: "IX_Rating_Owner_MetricID",
                table: "Rating",
                columns: new[] { "Owner", "MetricID" });

            migrationBuilder.CreateIndex(
                name: "IX_Request_CompanyId",
                table: "Request",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_Owner",
                table: "Request",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_Request_SourceId",
                table: "Request",
                column: "SourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_TargetId",
                table: "Request",
                column: "TargetId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_RoleId",
                table: "AspNetUsers",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyToCompanyAccessMap");

            migrationBuilder.DropTable(
                name: "InsertLog");

            migrationBuilder.DropTable(
                name: "MetricCompanyAccessMap");

            migrationBuilder.DropTable(
                name: "Newsletter");

            migrationBuilder.DropTable(
                name: "QueryLog");

            migrationBuilder.DropTable(
                name: "Rating");

            migrationBuilder.DropTable(
                name: "RequestBeta");

            migrationBuilder.DropTable(
                name: "Request");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "MetricTransfer");

            migrationBuilder.DropTable(
                name: "Metric");

            migrationBuilder.DropTable(
                name: "Datasource");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "AspNetRoles");
        }
    }
}