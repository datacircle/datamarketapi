﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class datasetrequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Request_Metric_SourceId",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_Request_Datasource_TargetId",
                table: "Request");

            migrationBuilder.DropIndex(
                name: "IX_Request_SourceId",
                table: "Request");

            migrationBuilder.DropIndex(
                name: "IX_Request_TargetId",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "AutoUpdate",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "CreateTable",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "Fields",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "LastPool",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "Modifications",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "RefreshData",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "TargetId",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "TargetTableName",
                table: "Request");

            migrationBuilder.RenameColumn(
                name: "Interval",
                table: "Request",
                newName: "UpvoteCount");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Request",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommentCount",
                table: "Request",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "RequestComment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    RequestId = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Value = table.Column<string>(type: "varchar(500)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestComment_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestComment_Request_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Request",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RequestVote",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    RequestId = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Vote = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestVote", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestVote_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestVote_Request_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Request",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequestComment_Owner",
                table: "RequestComment",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_RequestComment_RequestId",
                table: "RequestComment",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestVote_Owner",
                table: "RequestVote",
                column: "Owner");

            migrationBuilder.CreateIndex(
                name: "IX_RequestVote_RequestId",
                table: "RequestVote",
                column: "RequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequestComment");

            migrationBuilder.DropTable(
                name: "RequestVote");

            migrationBuilder.DropColumn(
                name: "CommentCount",
                table: "Request");

            migrationBuilder.RenameColumn(
                name: "UpvoteCount",
                table: "Request",
                newName: "Interval");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Request",
                type: "varchar(100)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AutoUpdate",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CreateTable",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Fields",
                table: "Request",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastPool",
                table: "Request",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Modifications",
                table: "Request",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RefreshData",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "SourceId",
                table: "Request",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TargetId",
                table: "Request",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "TargetTableName",
                table: "Request",
                type: "varchar(100)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Request_SourceId",
                table: "Request",
                column: "SourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_TargetId",
                table: "Request",
                column: "TargetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Request_Metric_SourceId",
                table: "Request",
                column: "SourceId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Request_Datasource_TargetId",
                table: "Request",
                column: "TargetId",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}