﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMarketAPI.Migrations
{
    public partial class uploadformetric : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cost",
                table: "Metric");

            migrationBuilder.AlterColumn<string>(
                name: "Query",
                table: "Metric",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Metric",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "Metric",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ShareLinkEnabled",
                table: "Metric",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Metric");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "Metric");

            migrationBuilder.DropColumn(
                name: "ShareLinkEnabled",
                table: "Metric");

            migrationBuilder.AlterColumn<string>(
                name: "Query",
                table: "Metric",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Cost",
                table: "Metric",
                nullable: false,
                defaultValue: 0);
        }
    }
}