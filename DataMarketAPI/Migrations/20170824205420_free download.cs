﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class freedownload : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyToCompanyAccessMap");

            migrationBuilder.DropTable(
                name: "MetricCompanyAccessMap");

            migrationBuilder.CreateTable(
                name: "FreeDownload",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DownloadTimes = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    MetricId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FreeDownload", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FreeDownload_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetricAccessMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActionableUser = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    MetricId = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricAccessMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricAccessMap_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricAccessMap_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricAccessMap_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FreeDownload_MetricId",
                table: "FreeDownload",
                column: "MetricId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricAccessMap_CompanyId",
                table: "MetricAccessMap",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricAccessMap_MetricId",
                table: "MetricAccessMap",
                column: "MetricId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricAccessMap_UserId",
                table: "MetricAccessMap",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FreeDownload");

            migrationBuilder.DropTable(
                name: "MetricAccessMap");

            migrationBuilder.CreateTable(
                name: "CompanyToCompanyAccessMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    OriginCompanyID = table.Column<int>(nullable: false),
                    TargetCompanyId = table.Column<int>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyToCompanyAccessMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyToCompanyAccessMap_Company_OriginCompanyID",
                        column: x => x.OriginCompanyID,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyToCompanyAccessMap_Company_TargetCompanyId",
                        column: x => x.TargetCompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetricCompanyAccessMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    MetricId = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricCompanyAccessMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricCompanyAccessMap_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricCompanyAccessMap_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyToCompanyAccessMap_OriginCompanyID",
                table: "CompanyToCompanyAccessMap",
                column: "OriginCompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyToCompanyAccessMap_TargetCompanyId",
                table: "CompanyToCompanyAccessMap",
                column: "TargetCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricCompanyAccessMap_CompanyId",
                table: "MetricCompanyAccessMap",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricCompanyAccessMap_MetricId",
                table: "MetricCompanyAccessMap",
                column: "MetricId");
        }
    }
}