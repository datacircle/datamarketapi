﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using DataMarketAPI.Database;
using DataMarketAPI.Models;

namespace DataMarketAPI.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20170831185606_share data")]
    partial class sharedata
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataMarketAPI.Models.CompanyModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Name")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("NormalizedName")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("PublicId");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Company");
                });

            modelBuilder.Entity("DataMarketAPI.Models.DatasourceModel", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<int?>("CompanyId");

                    b.Property<bool>("ConnectionStatus");

                    b.Property<string>("ConnectionString")
                        .HasColumnType("varchar(250)");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Database")
                        .IsRequired()
                        .HasColumnType("varchar(250)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Host")
                        .IsRequired()
                        .HasColumnType("varchar(250)");

                    b.Property<string>("Licence");

                    b.Property<bool>("Open");

                    b.Property<int>("Owner");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Port");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<int>("Type");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("varchar(250)");

                    b.HasKey("ID");

                    b.HasIndex("CompanyId");

                    b.HasIndex("Owner");

                    b.ToTable("Datasource");
                });

            modelBuilder.Entity("DataMarketAPI.Models.FreeDownloadModel", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<int>("DownloadTimes");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<Guid>("MetricId");

                    b.HasKey("ID");

                    b.HasIndex("MetricId");

                    b.ToTable("FreeDownload");
                });

            modelBuilder.Entity("DataMarketAPI.Models.InsertLogModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("DataSize");

                    b.Property<string>("ErrorMessage");

                    b.Property<long>("InsertTime");

                    b.Property<bool>("IsSuccesful");

                    b.Property<int>("MetricTransferID");

                    b.Property<int>("Rows");

                    b.HasKey("ID");

                    b.HasIndex("MetricTransferID");

                    b.ToTable("InsertLog");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricAccessMapModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<int>("ActionableUserId");

                    b.Property<int?>("CompanyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<Guid>("MetricId");

                    b.Property<int?>("TargetUserId");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("ID");

                    b.HasIndex("ActionableUserId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("MetricId");

                    b.HasIndex("TargetUserId");

                    b.ToTable("MetricAccessMap");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricModel", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<bool>("Approved");

                    b.Property<int?>("CompanyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<Guid>("DatasourceId");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<string>("FileName");

                    b.Property<string>("FilePath");

                    b.Property<int>("Interval");

                    b.Property<DateTime>("LastPool");

                    b.Property<string>("Licence");

                    b.Property<bool>("Open");

                    b.Property<int>("Owner");

                    b.Property<bool>("Processing");

                    b.Property<string>("Query")
                        .HasColumnType("text");

                    b.Property<double>("Rating");

                    b.Property<bool>("RequiresAuthorization");

                    b.Property<bool>("ShareLinkEnabled");

                    b.Property<string>("Structure");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("ID");

                    b.HasIndex("CompanyId");

                    b.HasIndex("DatasourceId");

                    b.HasIndex("Owner");

                    b.ToTable("Metric");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricTransferModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ActionableUser");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("DownloadAsFile");

                    b.Property<string>("Message");

                    b.Property<Guid>("MetricID");

                    b.Property<int?>("Owner");

                    b.Property<int>("Status");

                    b.Property<int?>("TargetCompanyID");

                    b.Property<Guid>("TargetDatasourceID");

                    b.Property<string>("TargetTableName");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("MetricID");

                    b.HasIndex("Owner");

                    b.HasIndex("TargetCompanyID");

                    b.HasIndex("TargetDatasourceID");

                    b.ToTable("MetricTransfer");
                });

            modelBuilder.Entity("DataMarketAPI.Models.NewsletterModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("DoubleOptInToken");

                    b.Property<bool>("DoubleOptedIn");

                    b.Property<string>("Email")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("Subscribed");

                    b.Property<string>("UnsubscriptionToken");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("ID");

                    b.ToTable("Newsletter");
                });

            modelBuilder.Entity("DataMarketAPI.Models.QueryLogModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<bool>("Cached");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("DataSize");

                    b.Property<Guid>("DatasourceID");

                    b.Property<string>("ErrorMessage");

                    b.Property<bool>("IsSuccesful");

                    b.Property<Guid>("MetricID");

                    b.Property<long>("QueryTime");

                    b.HasKey("ID");

                    b.HasIndex("DatasourceID");

                    b.HasIndex("MetricID");

                    b.ToTable("QueryLog");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RatingModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<Guid>("MetricID");

                    b.Property<int>("Owner");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<double>("Value");

                    b.HasKey("ID");

                    b.HasIndex("MetricID");

                    b.HasIndex("Owner", "MetricID");

                    b.ToTable("Rating");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestBetaModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<bool>("CompanyUser");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("ID");

                    b.ToTable("RequestBeta");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestCommentModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("Owner");

                    b.Property<Guid>("RequestId");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasColumnType("varchar(500)");

                    b.HasKey("ID");

                    b.HasIndex("Owner");

                    b.HasIndex("RequestId");

                    b.ToTable("RequestComment");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestModel", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<bool>("Anonymous");

                    b.Property<int>("CommentCount");

                    b.Property<int?>("CompanyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<int>("Owner");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<int>("UpvoteCount");

                    b.HasKey("ID");

                    b.HasIndex("CompanyId");

                    b.HasIndex("Owner");

                    b.ToTable("Request");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestVoteModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("Owner");

                    b.Property<Guid>("RequestId");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<int>("Vote");

                    b.HasKey("ID");

                    b.HasIndex("Owner");

                    b.HasIndex("RequestId");

                    b.ToTable("RequestVote");
                });

            modelBuilder.Entity("DataMarketAPI.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("DataMarketAPI.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<int?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("ConsumerApiKey");

                    b.Property<string>("ConsumerSecretApiKey");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsCompanyOwner");

                    b.Property<bool>("IsVerified");

                    b.Property<string>("LastName")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MiddleName")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("PublicId");

                    b.Property<int?>("RoleId");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("Token")
                        .HasColumnType("varchar(250)");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.Property<string>("VerificationKey")
                        .HasColumnType("varchar(250)");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DataMarketAPI.Models.DatasourceModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.CompanyModel", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");
                });

            modelBuilder.Entity("DataMarketAPI.Models.FreeDownloadModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.MetricModel", "Metric")
                        .WithMany()
                        .HasForeignKey("MetricId");
                });

            modelBuilder.Entity("DataMarketAPI.Models.InsertLogModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.MetricTransferModel", "MetricTransfer")
                        .WithMany()
                        .HasForeignKey("MetricTransferID");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricAccessMapModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.User", "ActionableUser")
                        .WithMany()
                        .HasForeignKey("ActionableUserId");

                    b.HasOne("DataMarketAPI.Models.CompanyModel", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("DataMarketAPI.Models.MetricModel", "Metric")
                        .WithMany()
                        .HasForeignKey("MetricId");

                    b.HasOne("DataMarketAPI.Models.User", "TargetUser")
                        .WithMany()
                        .HasForeignKey("TargetUserId");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.CompanyModel", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("DataMarketAPI.Models.DatasourceModel", "Datasource")
                        .WithMany()
                        .HasForeignKey("DatasourceId");

                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");
                });

            modelBuilder.Entity("DataMarketAPI.Models.MetricTransferModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.MetricModel", "Metric")
                        .WithMany()
                        .HasForeignKey("MetricID");

                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");

                    b.HasOne("DataMarketAPI.Models.CompanyModel", "TargetCompany")
                        .WithMany()
                        .HasForeignKey("TargetCompanyID");

                    b.HasOne("DataMarketAPI.Models.DatasourceModel", "TargetDatasource")
                        .WithMany()
                        .HasForeignKey("TargetDatasourceID");
                });

            modelBuilder.Entity("DataMarketAPI.Models.QueryLogModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.DatasourceModel", "Datasource")
                        .WithMany()
                        .HasForeignKey("DatasourceID");

                    b.HasOne("DataMarketAPI.Models.MetricModel", "Metric")
                        .WithMany()
                        .HasForeignKey("MetricID");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RatingModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.MetricModel", "Metric")
                        .WithMany()
                        .HasForeignKey("MetricID");

                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestCommentModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");

                    b.HasOne("DataMarketAPI.Models.RequestModel", "Request")
                        .WithMany()
                        .HasForeignKey("RequestId");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.CompanyModel", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");
                });

            modelBuilder.Entity("DataMarketAPI.Models.RequestVoteModel", b =>
                {
                    b.HasOne("DataMarketAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("Owner");

                    b.HasOne("DataMarketAPI.Models.RequestModel", "Request")
                        .WithMany()
                        .HasForeignKey("RequestId");
                });

            modelBuilder.Entity("DataMarketAPI.Models.User", b =>
                {
                    b.HasOne("DataMarketAPI.Models.CompanyModel", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("DataMarketAPI.Models.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("DataMarketAPI.Models.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("DataMarketAPI.Models.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("DataMarketAPI.Models.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<int>", b =>
                {
                    b.HasOne("DataMarketAPI.Models.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId");

                    b.HasOne("DataMarketAPI.Models.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId");
                });
        }
    }
}
