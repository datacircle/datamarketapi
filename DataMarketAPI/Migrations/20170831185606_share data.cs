﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMarketAPI.Migrations
{
    public partial class sharedata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_UserId",
                table: "MetricAccessMap");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "MetricAccessMap",
                newName: "TargetUserId");

            migrationBuilder.RenameColumn(
                name: "ActionableUser",
                table: "MetricAccessMap",
                newName: "ActionableUserId");

            migrationBuilder.RenameIndex(
                name: "IX_MetricAccessMap_UserId",
                table: "MetricAccessMap",
                newName: "IX_MetricAccessMap_TargetUserId");

            migrationBuilder.AddColumn<bool>(
                name: "Anonymous",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_MetricAccessMap_ActionableUserId",
                table: "MetricAccessMap",
                column: "ActionableUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap",
                column: "ActionableUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap",
                column: "TargetUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropIndex(
                name: "IX_MetricAccessMap_ActionableUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropColumn(
                name: "Anonymous",
                table: "Request");

            migrationBuilder.RenameColumn(
                name: "TargetUserId",
                table: "MetricAccessMap",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ActionableUserId",
                table: "MetricAccessMap",
                newName: "ActionableUser");

            migrationBuilder.RenameIndex(
                name: "IX_MetricAccessMap_TargetUserId",
                table: "MetricAccessMap",
                newName: "IX_MetricAccessMap_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_UserId",
                table: "MetricAccessMap",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}