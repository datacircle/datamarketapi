﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMarketAPI.Migrations
{
    public partial class plaintext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionPlainText",
                table: "Request",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionPlainText",
                table: "Metric",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescriptionPlainText",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "DescriptionPlainText",
                table: "Metric");
        }
    }
}