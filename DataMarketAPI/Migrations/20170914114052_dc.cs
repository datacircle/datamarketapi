﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class dc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CommentCount",
                table: "Metric",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MetricComment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    MetricId = table.Column<Guid>(nullable: false),
                    Owner = table.Column<int>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Value = table.Column<string>(type: "varchar(500)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricComment_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MetricComment_AspNetUsers_Owner",
                        column: x => x.Owner,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MetricComment_MetricId",
                table: "MetricComment",
                column: "MetricId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricComment_Owner",
                table: "MetricComment",
                column: "Owner");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MetricComment");

            migrationBuilder.DropColumn(
                name: "CommentCount",
                table: "Metric");
        }
    }
}