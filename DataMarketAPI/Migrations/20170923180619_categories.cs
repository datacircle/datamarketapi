﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMarketAPI.Migrations
{
    public partial class categories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                table: "AspNetRoleClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                table: "AspNetUserClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                table: "AspNetUserLogins");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                table: "AspNetUserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_AspNetRoles_RoleId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Datasource_Company_CompanyId",
                table: "Datasource");

            migrationBuilder.DropForeignKey(
                name: "FK_Datasource_AspNetUsers_Owner",
                table: "Datasource");

            migrationBuilder.DropForeignKey(
                name: "FK_FreeDownload_Metric_MetricId",
                table: "FreeDownload");

            migrationBuilder.DropForeignKey(
                name: "FK_InsertLog_MetricTransfer_MetricTransferID",
                table: "InsertLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Company_CompanyId",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Datasource_DatasourceId",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_AspNetUsers_Owner",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_Company_CompanyId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_Metric_MetricId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricComment_Metric_MetricId",
                table: "MetricComment");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricComment_AspNetUsers_Owner",
                table: "MetricComment");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Metric_MetricID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_AspNetUsers_Owner",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Company_TargetCompanyID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Datasource_TargetDatasourceID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_QueryLog_Datasource_DatasourceID",
                table: "QueryLog");

            migrationBuilder.DropForeignKey(
                name: "FK_QueryLog_Metric_MetricID",
                table: "QueryLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Rating_Metric_MetricID",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_Rating_AspNetUsers_Owner",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_Request_Company_CompanyId",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_Request_AspNetUsers_Owner",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestComment_AspNetUsers_Owner",
                table: "RequestComment");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestComment_Request_RequestId",
                table: "RequestComment");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestVote_AspNetUsers_Owner",
                table: "RequestVote");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestVote_Request_RequestId",
                table: "RequestVote");

            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UsageCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HashTag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UsageCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HashTag", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_AspNetRoles_RoleId",
                table: "AspNetUsers",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                table: "AspNetUserTokens",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datasource_Company_CompanyId",
                table: "Datasource",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datasource_AspNetUsers_Owner",
                table: "Datasource",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FreeDownload_Metric_MetricId",
                table: "FreeDownload",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsertLog_MetricTransfer_MetricTransferID",
                table: "InsertLog",
                column: "MetricTransferID",
                principalTable: "MetricTransfer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Company_CompanyId",
                table: "Metric",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Datasource_DatasourceId",
                table: "Metric",
                column: "DatasourceId",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_AspNetUsers_Owner",
                table: "Metric",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap",
                column: "ActionableUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_Company_CompanyId",
                table: "MetricAccessMap",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_Metric_MetricId",
                table: "MetricAccessMap",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap",
                column: "TargetUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricComment_Metric_MetricId",
                table: "MetricComment",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricComment_AspNetUsers_Owner",
                table: "MetricComment",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Metric_MetricID",
                table: "MetricTransfer",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_AspNetUsers_Owner",
                table: "MetricTransfer",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Company_TargetCompanyID",
                table: "MetricTransfer",
                column: "TargetCompanyID",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Datasource_TargetDatasourceID",
                table: "MetricTransfer",
                column: "TargetDatasourceID",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QueryLog_Datasource_DatasourceID",
                table: "QueryLog",
                column: "DatasourceID",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QueryLog_Metric_MetricID",
                table: "QueryLog",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_Metric_MetricID",
                table: "Rating",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_AspNetUsers_Owner",
                table: "Rating",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Request_Company_CompanyId",
                table: "Request",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Request_AspNetUsers_Owner",
                table: "Request",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestComment_AspNetUsers_Owner",
                table: "RequestComment",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestComment_Request_RequestId",
                table: "RequestComment",
                column: "RequestId",
                principalTable: "Request",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestVote_AspNetUsers_Owner",
                table: "RequestVote",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestVote_Request_RequestId",
                table: "RequestVote",
                column: "RequestId",
                principalTable: "Request",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                table: "AspNetRoleClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                table: "AspNetUserClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                table: "AspNetUserLogins");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                table: "AspNetUserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_AspNetRoles_RoleId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                table: "AspNetUserTokens");

            migrationBuilder.DropForeignKey(
                name: "FK_Datasource_Company_CompanyId",
                table: "Datasource");

            migrationBuilder.DropForeignKey(
                name: "FK_Datasource_AspNetUsers_Owner",
                table: "Datasource");

            migrationBuilder.DropForeignKey(
                name: "FK_FreeDownload_Metric_MetricId",
                table: "FreeDownload");

            migrationBuilder.DropForeignKey(
                name: "FK_InsertLog_MetricTransfer_MetricTransferID",
                table: "InsertLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Company_CompanyId",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Datasource_DatasourceId",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_AspNetUsers_Owner",
                table: "Metric");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_Company_CompanyId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_Metric_MetricId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricComment_Metric_MetricId",
                table: "MetricComment");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricComment_AspNetUsers_Owner",
                table: "MetricComment");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Metric_MetricID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_AspNetUsers_Owner",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Company_TargetCompanyID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_MetricTransfer_Datasource_TargetDatasourceID",
                table: "MetricTransfer");

            migrationBuilder.DropForeignKey(
                name: "FK_QueryLog_Datasource_DatasourceID",
                table: "QueryLog");

            migrationBuilder.DropForeignKey(
                name: "FK_QueryLog_Metric_MetricID",
                table: "QueryLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Rating_Metric_MetricID",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_Rating_AspNetUsers_Owner",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_Request_Company_CompanyId",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_Request_AspNetUsers_Owner",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestComment_AspNetUsers_Owner",
                table: "RequestComment");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestComment_Request_RequestId",
                table: "RequestComment");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestVote_AspNetUsers_Owner",
                table: "RequestVote");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestVote_Request_RequestId",
                table: "RequestVote");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "HashTag");

            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_AspNetRoles_RoleId",
                table: "AspNetUsers",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datasource_Company_CompanyId",
                table: "Datasource",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datasource_AspNetUsers_Owner",
                table: "Datasource",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FreeDownload_Metric_MetricId",
                table: "FreeDownload",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsertLog_MetricTransfer_MetricTransferID",
                table: "InsertLog",
                column: "MetricTransferID",
                principalTable: "MetricTransfer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Company_CompanyId",
                table: "Metric",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Datasource_DatasourceId",
                table: "Metric",
                column: "DatasourceId",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_AspNetUsers_Owner",
                table: "Metric",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_ActionableUserId",
                table: "MetricAccessMap",
                column: "ActionableUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_Company_CompanyId",
                table: "MetricAccessMap",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_Metric_MetricId",
                table: "MetricAccessMap",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricAccessMap_AspNetUsers_TargetUserId",
                table: "MetricAccessMap",
                column: "TargetUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricComment_Metric_MetricId",
                table: "MetricComment",
                column: "MetricId",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricComment_AspNetUsers_Owner",
                table: "MetricComment",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Metric_MetricID",
                table: "MetricTransfer",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_AspNetUsers_Owner",
                table: "MetricTransfer",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Company_TargetCompanyID",
                table: "MetricTransfer",
                column: "TargetCompanyID",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MetricTransfer_Datasource_TargetDatasourceID",
                table: "MetricTransfer",
                column: "TargetDatasourceID",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QueryLog_Datasource_DatasourceID",
                table: "QueryLog",
                column: "DatasourceID",
                principalTable: "Datasource",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QueryLog_Metric_MetricID",
                table: "QueryLog",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_Metric_MetricID",
                table: "Rating",
                column: "MetricID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_AspNetUsers_Owner",
                table: "Rating",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Request_Company_CompanyId",
                table: "Request",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Request_AspNetUsers_Owner",
                table: "Request",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestComment_AspNetUsers_Owner",
                table: "RequestComment",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestComment_Request_RequestId",
                table: "RequestComment",
                column: "RequestId",
                principalTable: "Request",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestVote_AspNetUsers_Owner",
                table: "RequestVote",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestVote_Request_RequestId",
                table: "RequestVote",
                column: "RequestId",
                principalTable: "Request",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}