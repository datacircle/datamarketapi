﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class categories2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoryID",
                table: "Metric",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "MetricModelID",
                table: "HashTag",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Metric_CategoryID",
                table: "Metric",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_HashTag_MetricModelID",
                table: "HashTag",
                column: "MetricModelID");

            migrationBuilder.AddForeignKey(
                name: "FK_HashTag_Metric_MetricModelID",
                table: "HashTag",
                column: "MetricModelID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Category_CategoryID",
                table: "Metric",
                column: "CategoryID",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HashTag_Metric_MetricModelID",
                table: "HashTag");

            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Category_CategoryID",
                table: "Metric");

            migrationBuilder.DropIndex(
                name: "IX_Metric_CategoryID",
                table: "Metric");

            migrationBuilder.DropIndex(
                name: "IX_HashTag_MetricModelID",
                table: "HashTag");

            migrationBuilder.DropColumn(
                name: "CategoryID",
                table: "Metric");

            migrationBuilder.DropColumn(
                name: "MetricModelID",
                table: "HashTag");
        }
    }
}