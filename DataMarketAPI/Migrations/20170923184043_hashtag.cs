﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DataMarketAPI.Migrations
{
    public partial class hashtag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HashTag_Metric_MetricModelID",
                table: "HashTag");

            migrationBuilder.DropIndex(
                name: "IX_HashTag_MetricModelID",
                table: "HashTag");

            migrationBuilder.DropColumn(
                name: "MetricModelID",
                table: "HashTag");

            migrationBuilder.AddColumn<string>(
                name: "HashTags",
                table: "Metric",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HashTags",
                table: "Metric");

            migrationBuilder.AddColumn<Guid>(
                name: "MetricModelID",
                table: "HashTag",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HashTag_MetricModelID",
                table: "HashTag",
                column: "MetricModelID");

            migrationBuilder.AddForeignKey(
                name: "FK_HashTag_Metric_MetricModelID",
                table: "HashTag",
                column: "MetricModelID",
                principalTable: "Metric",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}