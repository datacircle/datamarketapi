﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMarketAPI.Migrations
{
    public partial class hasasdasdhtag3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Metric_Category_CategoryID",
                table: "Metric");

            migrationBuilder.DropIndex(
                name: "IX_Metric_CategoryID",
                table: "Metric");

            migrationBuilder.RenameColumn(
                name: "CategoryID",
                table: "Metric",
                newName: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Metric",
                newName: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Metric_CategoryID",
                table: "Metric",
                column: "CategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Metric_Category_CategoryID",
                table: "Metric",
                column: "CategoryID",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}