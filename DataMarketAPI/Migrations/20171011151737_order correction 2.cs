﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DataMarketAPI.Migrations
{
    public partial class ordercorrection2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Owner",
                table: "Order",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Order_Owner",
                table: "Order",
                column: "Owner");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_AspNetUsers_Owner",
                table: "Order",
                column: "Owner",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_AspNetUsers_Owner",
                table: "Order");

            migrationBuilder.DropIndex(
                name: "IX_Order_Owner",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "Owner",
                table: "Order");
        }
    }
}
