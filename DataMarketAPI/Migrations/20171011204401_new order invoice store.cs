﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DataMarketAPI.Migrations
{
    public partial class neworderinvoicestore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyAddress",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Invoice");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Invoice",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Invoice",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Invoice");

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress",
                table: "Invoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Invoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Invoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Invoice",
                nullable: true);
        }
    }
}
