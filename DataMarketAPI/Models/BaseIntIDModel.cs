using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace DataMarketAPI.Models
{
    public class BaseIntIDModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public void Hydrate(BaseIntIDModel updatedModel)
        {
            foreach (PropertyInfo _property in updatedModel.GetType().GetProperties())
            {
                var propertyName = _property.Name;

                var newPropertyValue = _property.GetValue(updatedModel);

                if (propertyName == "UpdatedAt")
                {
                    newPropertyValue = DateTime.Now;
                }

                if (propertyName == "CreatedAt")
                {
                    continue;
                }

                if (propertyName == "Owner")
                {
                    continue;
                }

                this.GetType().GetProperty(propertyName).SetValue(this, newPropertyValue);
            }
        }
    }
}