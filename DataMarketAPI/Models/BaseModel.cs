using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace DataMarketAPI.Models
{
    public class BaseModel
    {
        [Key, Column("Id")]
        public Guid ID { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public bool Deleted { get; set; } = false;

        public void Hydrate(BaseModel updatedModel)
        {
            foreach (PropertyInfo _property in updatedModel.GetType().GetProperties())
            {
                var propertyName = _property.Name;

                var newPropertyValue = _property.GetValue(updatedModel);

                // if (propertyName.ToLower().IndexOf("id") != -1) {
                //     continue;
                // }

                if (propertyName == "UpdatedAt")
                {
                    newPropertyValue = DateTime.Now;
                }

                if (propertyName == "CreatedAt")
                {
                    continue;
                }

                if (propertyName == "Owner")
                {
                    continue;
                }

                this.GetType().GetProperty(propertyName).SetValue(this, newPropertyValue);
            }
        }
    }
}