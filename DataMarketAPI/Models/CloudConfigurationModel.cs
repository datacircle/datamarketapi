﻿namespace DataMarketAPI.Models
{
    public class StorageConfigurationModel
    {
        public string StorageConnectionString { get; set; }
        public string Share { get; set; }
        public string Container { get; set; }
        public string BasePath { get; set; }
        public string BaseDirectory { get; set; }
    }
}