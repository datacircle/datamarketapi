﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketAPI.Models
{
    public class CommentModel
    {
        [Required]
        public string Value { get; set; }

        public Guid TargetId { get; set; }
    }
}