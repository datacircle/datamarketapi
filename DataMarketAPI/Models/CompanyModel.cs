using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class CompanyModel : BaseIntIDModel
    {
        public string PublicId { get; set; } = Guid.NewGuid().ToString().Replace("-", "");

        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string NormalizedName { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string Description { get; set; }
    }
}