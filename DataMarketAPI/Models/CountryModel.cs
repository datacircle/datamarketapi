namespace DataMarketAPI.Models
{
    public class CountryModel : BaseIntIDModel
    {        
        public string ISO2 { get; set; }

        public string Name { get; set; }

        public string TaxRate { get; set; }
    }
}

