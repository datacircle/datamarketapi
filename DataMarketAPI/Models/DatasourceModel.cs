using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public enum Type
    {
        FileDownload = 0,

        FileUploadGeneric = 1,

        Link = 2,

        Airtable = 3,

        Mysql = 100,

        Mssql = 101
    }

    public class DatasourceModel : BaseModel
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string Description { get; set; }

        public bool Open { get; set; } = false;

        public string Licence { get; set; }

        [Required]
        public Type Type { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string ConnectionString { get; set; }

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string Host { get; set; }

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string Username { get; set; }

        [Required]
        [Column(TypeName = "text")]
        public string Password { get; set; }

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string Database { get; set; }

        [Required]
        public int Port { get; set; }

        public bool ConnectionStatus { get; set; }

        public int? CompanyId { get; set; }
        public CompanyModel Company { get; set; }

        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }
    }
}