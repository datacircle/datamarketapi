﻿using System;

namespace DataMarketAPI.Models
{
    public class FileShareModel
    {
        public Guid MetricId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int UserId { get; set; }

        public string GetFullyQualifiedName()
        {
            return UserId.ToString() + "|@|" + MetricId.ToString() + "|@|" + FileName;
        }
    }
}