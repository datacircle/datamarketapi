﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class FreeDownloadModel
    {
        [Key, Column("Id")]
        public Guid ID { get; set; }

        [Required]
        public Guid MetricId { get; set; }

        public MetricModel Metric { get; set; }

        [Required]
        public string Email { get; set; }

        public int DownloadTimes { get; set; } = 0;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}