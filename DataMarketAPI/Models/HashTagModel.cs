﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class HashTagModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public int UsageCount { get; set; }
    }
}