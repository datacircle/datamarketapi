﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class HashTagToMetricModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        public int HashTagId { get; set; }

        public HashTagModel HashTag { get; set; }

        public Guid MetricId { get; set; }

        public MetricModel Metric { get; set; }
    }
}