﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class InsertLogModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        public DateTime CreatedAt { get; set; }

        public int MetricTransferID { get; set; }

        public MetricTransferModel MetricTransfer { get; set; }

        public int Rows { get; set; }

        public long InsertTime { get; set; } = 0;

        public int DataSize { get; set; } = 0;
        public bool IsSuccesful { get; set; }
        public string ErrorMessage { get; set; }
    }
}