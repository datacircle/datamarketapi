namespace DataMarketAPI.Models
{
    public class InvoiceModel : BaseIntIDModel
    {
        public string PublicId { get; set; }
        
        public string FullName { get; set; }

        public string Address { get; set; }
        
        public int CountryId { get; set; }

        public CountryModel Country { get; set; }

        public bool Paid { get; set; }
    }
}