using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class MetricModel : BaseModel
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Column(TypeName = "text")]
        public string DescriptionPlainText { get; set; }

        public double Rating { get; set; }

        public bool Approved { get; set; } = false;

        public int Cost { get; set;} = 0;

        public bool RequiresAuthorization { get; set; } = false;

        public int CommentCount { get; set; } = 0;

        public int? CategoryId { get; set; }

        public int DownloadCount { get; set; } = 0;

        public String HashTags { get; set; } = "";

        #region Privacy

        public string Licence { get; set; }
        public bool Open { get; set; } = false;
        public bool ShareLinkEnabled { get; set; } = false;
        public bool Anonymous { get; set; } = false;

        #endregion Privacy

        #region Process

        public int Interval { get; set; } = 1800;
        public DateTime LastPool { get; set; } = DateTime.Now;
        public bool Processing { get; set; } = false;

        #endregion Process

        public string Structure { get; set; }

        #region DB

        [Column(TypeName = "text")]
        public string Query { get; set; }

        #endregion DB

        #region Link

        public string Url { get; set; }

        #endregion Link

        #region File

        public string FileName { get; set; }
        public string FilePath { get; set; }

        #endregion File

        [Required]
        public Guid DatasourceId { get; set; }

        public DatasourceModel Datasource { get; set; }
        public int? CompanyId { get; set; }
        public CompanyModel Company { get; set; }

        [Required]
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }

        public void OmitSensitiveData()
        {
            this.Datasource = null;
            this.Query = null;
        }
    }
}