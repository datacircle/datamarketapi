using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class MetricAccessMapModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        [Required]
        public Guid MetricId { get; set; }

        public MetricModel Metric { get; set; }

        public int ActionableUserId { get; set; }

        public User ActionableUser { get; set; }

        public int? TargetUserId { get; set; }

        public User TargetUser { get; set; }

        public int? CompanyId { get; set; }

        public CompanyModel Company { get; set; }
    }
}