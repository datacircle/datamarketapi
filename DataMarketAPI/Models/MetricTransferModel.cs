﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class MetricTransferModel : BaseIntIDModel
    {
        public enum StatusCode
        {
            Error = 0,

            Created = 10,

            Queued = 11,

            Started = 20,

            Completed = 30
        }

        [Required]
        public Guid MetricID { get; set; }

        public MetricModel Metric { get; set; }

        [Required]
        public Guid TargetDatasourceID { get; set; }

        public DatasourceModel TargetDatasource { get; set; }

        public string TargetTableName { get; set; } = String.Empty;
        public bool DownloadAsFile { get; set; } = false;

        public int? TargetCompanyID { get; set; }

        [ForeignKey("TargetCompanyID")]
        public CompanyModel TargetCompany { get; set; }

        public StatusCode Status { get; set; } = MetricTransferModel.StatusCode.Created;

        public string Message { get; set; }

        [Required]
        public int ActionableUser { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }
    }
}