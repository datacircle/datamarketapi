using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class NewsletterModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string Email { get; set; }

        public string UnsubscriptionToken { get; set; }

        public bool Subscribed { get; set; }

        public string DoubleOptInToken { get; set; }

        public bool DoubleOptedIn { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}