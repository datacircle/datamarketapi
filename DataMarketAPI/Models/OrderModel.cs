using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public enum OrderStatus
    {
        New = 0,

        Paid = 1,

        Returned = -1,
    }

    public class OrderModel : BaseIntIDModel
    {
        public string PublicId { get; set; } = Guid.NewGuid().ToString().Replace("-", "");

        [Required]
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }

        public int Cost { get; set; }

        [Required]
        public Guid? MetricId { get; set; }
        
        public bool Test { get; set; } = false;

        public OrderStatus Status { get; set; } = OrderStatus.New;
        
        public int? InvoiceId { get; set; }

        public InvoiceModel Invoice { get; set; }
    }
}