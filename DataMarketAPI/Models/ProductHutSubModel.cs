using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class ProductHutSubModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        public string Email { get; set; }
    }
}