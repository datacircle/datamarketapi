using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class QueryLogModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public Guid DatasourceID { get; set; }

        [Required]
        public DatasourceModel Datasource { get; set; }

        [Required]
        public Guid MetricID { get; set; }

        [Required]
        public MetricModel Metric { get; set; }

        [Required]
        public bool Cached { get; set; } = false;

        public long QueryTime { get; set; } = 0;

        public int DataSize { get; set; } = 0;
        public bool IsSuccesful { get; set; }
        public string ErrorMessage { get; set; }

        public QueryLogModel()
        {
        }

        public QueryLogModel(MetricModel metric)
        {
            this.Metric = metric;
            this.MetricID = this.Metric.ID;
            this.DatasourceID = this.Metric.DatasourceId;
            this.CreatedAt = DateTime.Now;
        }
    }
}