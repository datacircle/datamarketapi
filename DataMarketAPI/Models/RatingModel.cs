using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class RatingModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Required]
        public double Value { get; set; }

        [Required]
        public Guid MetricID { get; set; }

        public MetricModel Metric { get; set; }

        [Required]
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}