using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class RequestCommentModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        [Required]
        [Column(TypeName = "varchar(500)")]
        public string Value { get; set; }

        public Guid RequestId { get; set; }
        public RequestModel Request { get; set; }
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}