using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class RequestModel : BaseModel
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Column(TypeName = "text")]
        public string DescriptionPlainText { get; set; }

        public int UpvoteCount { get; set; } = 0;

        public int CommentCount { get; set; } = 0;

        public bool Anonymous { get; set; } = false;

        public int? CompanyId { get; set; }
        public CompanyModel Company { get; set; }
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }
    }
}