using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class RequestVoteModel
    {
        [Key, Column("Id")]
        public int ID { get; set; }

        public enum VoteType
        {
            Downvote = -1,
            Upvote = 1
        }

        [Required]
        public VoteType Vote { get; set; }

        [Required]
        public Guid RequestId { get; set; }

        public RequestModel Request { get; set; }

        [Required]
        public int Owner { get; set; }

        [ForeignKey("Owner")]
        public User User { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}