using Microsoft.IdentityModel.Tokens;
using System;

namespace DataMarketAPI.Models
{
    /// <summary>
    ///
    /// </summary>
    public class TokenProviderOptionsModel
    {
        /// <summary>
        ///
        /// </summary>
        public string Path { get; set; } = "/api/token/";

        /// <summary>
        ///
        /// </summary>
        public string Issuer { get; set; } = "ExampleIssuer";

        /// <summary>
        ///
        /// </summary>
        public string Audience { get; set; } = "ExampleAudience";

        /// <summary>
        ///
        /// </summary>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromHours(5);

        /// <summary>
        ///
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }
    }
}