using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketAPI.Models
{
    public class User : IdentityUser<int>
    {
        public string PublicId { get; set; } = Guid.NewGuid().ToString().Replace("-", "");

        [Column(TypeName = "varchar(100)")]
        public string FirstName { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string MiddleName { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string LastName { get; set; }

        public string Channel { get; set; }

        public int? CompanyId { get; set; }
        public CompanyModel Company { get; set; }
        public int? RoleId { get; set; }
        public Role Role { get; set; }
        public bool IsCompanyOwner { get; set; }
        public bool IsActive { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string VerificationKey { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Token { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string PayPalLink { get; set; }

        public string ConsumerApiKey { get; set; }
        public string ConsumerSecretApiKey { get; set; }
        public bool IsVerified { get; set; }

        public DateTime LastLogin { get; set; } = DateTime.Now;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public void OmitSentsitiveData()
        {
            this.VerificationKey = String.Empty;
            this.ConsumerApiKey = String.Empty;
            this.ConsumerSecretApiKey = String.Empty;
            this.PasswordHash = String.Empty;
            this.SecurityStamp = String.Empty;
            this.ConcurrencyStamp = String.Empty;
        }
    }
}