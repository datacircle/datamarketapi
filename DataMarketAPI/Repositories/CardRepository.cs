﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DataMarketAPI.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly DatabaseContext _context;

        public CardRepository(DatabaseContext context)
        {
            _context = context;
        }

        public int GetConsumerCount(User user)
        {
            var result = (from mt in _context.MetricTransfer
                          join m in _context.Metric on mt.MetricID equals m.ID
                          where (m.CompanyId == user.CompanyId && user.CompanyId.HasValue) || mt.ActionableUser == user.Id
                          where mt.Status == MetricTransferModel.StatusCode.Completed
                          select mt).Count();

            return result;
        }

        public int GetDatasourceCount(User user)
        {
            return _context.Datasource.Where(d => (d.CompanyId == user.CompanyId && user.CompanyId.HasValue) || d.Owner == user.Id).AsNoTracking().Count();
        }

        public int GetMetricCount(User user)
        {
            return _context.Metric.Where(d => (d.CompanyId == user.CompanyId && user.CompanyId.HasValue) || d.Owner == user.Id).AsNoTracking().Count();
        }

        public int GetRequestCount(User user)
        {
            return _context.Request.Where(d => (d.CompanyId == user.CompanyId && user.CompanyId.HasValue) || d.Owner == user.Id).AsNoTracking().Count();
        }
    }
}