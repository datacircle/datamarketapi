﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly DatabaseContext _context;

        public CategoryRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<IEnumerable<CategoyModel>> ICategoryRepository.GetAll()
        {
            return await _context.Category.ToListAsync();
        }

        async Task<CategoyModel> ICategoryRepository.Get(string name)
        {
            return await _context.Category.FirstOrDefaultAsync(c => c.Name == name);
        }
    }
}