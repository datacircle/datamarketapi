using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly DatabaseContext _context;

        public CompanyRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<CompanyModel> ICompanyRepository.Create(CompanyModel company)
        {
            DateTime now = DateTime.Now;

            company.NormalizedName = company.Name.ToLower();

            company.CreatedAt = now;
            company.UpdatedAt = now;

            await _context.Company.AddAsync(company);
            return company;
        }

        async Task<CompanyModel> ICompanyRepository.Get(CompanyModel company)
        {
            return await _context.Company
                        .AsNoTracking()
                        .Where(c => c.Id == company.Id)
                        .FirstOrDefaultAsync();
        }

        async Task<CompanyModel> ICompanyRepository.GetByName(CompanyModel company)
        {
            return await _context.Company.FirstOrDefaultAsync(c => c.Name == company.Name.ToLower());
        }

        async Task<CompanyModel> ICompanyRepository.GetByNormalizedName(CompanyModel company)
        {
            return await _context.Company.FirstOrDefaultAsync(c => c.NormalizedName == company.NormalizedName.ToLower());
        }

        async Task<CompanyModel> ICompanyRepository.Update(CompanyModel company)
        {
            CompanyModel _companyEntity = await _context.Company
                                                    .Where(c => c.Id == company.Id)
                                                    .FirstOrDefaultAsync();

            if (_companyEntity == null)
            {
                return null;
            }

            _companyEntity.Hydrate(company);

            _context.Company.Update(_companyEntity);

            return _companyEntity;
        }

        async Task<bool> ICompanyRepository.Delete(CompanyModel company)
        {
            CompanyModel _company = await _context.Company
                .Where(c => c.Id == company.Id)
                .FirstOrDefaultAsync();

            if (_company != null)
            {
                _context.Remove(_company);
                return true;
            }

            return false;
        }

        async Task<int> ICompanyRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}