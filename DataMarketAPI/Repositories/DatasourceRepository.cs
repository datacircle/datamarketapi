using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class DatasourceRepository : IDatasourceRepository
    {
        private readonly DatabaseContext _context;

        public DatasourceRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<DatasourceModel> IDatasourceRepository.Create(DatasourceModel model)
        {
            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            return (await _context.Datasource.AddAsync(model)).Entity;
        }

        async Task<DatasourceModel> IDatasourceRepository.Get(DatasourceModel model)
        {
            return await _context.Datasource
                            .Where(ds =>
                                (ds.CompanyId == model.CompanyId && model.CompanyId.HasValue) ||
                                ds.Owner == model.Owner ||
                                ds.CompanyId == 1 && ds.Type != Models.Type.FileDownload
                            ).AsNoTracking()
                        .FirstOrDefaultAsync(ds => ds.ID == model.ID);
        }

        async Task<DatasourceModel> IDatasourceRepository.GetUncencorsed(DatasourceModel model)
        {
            return await _context.Datasource.AsNoTracking()
                        .FirstOrDefaultAsync(ds => ds.ID == model.ID);
        }

        async Task<IEnumerable<DatasourceModel>> IDatasourceRepository.GetAll(DatasourceModel model)
        {
            return await _context.Datasource
                        .AsNoTracking()
                        .Where(ds =>
                                (ds.CompanyId == model.CompanyId && model.CompanyId.HasValue) ||
                                ds.Owner == model.Owner ||
                                ds.CompanyId == 1 && ds.Type != Models.Type.FileDownload
                            )
                        .ToListAsync();
        }

        async Task<DatasourceModel> IDatasourceRepository.Update(DatasourceModel datasource)
        {
            DatasourceModel _datasourceEntity = await _context.Datasource
                                                    .Where(ds => (ds.CompanyId == datasource.CompanyId && datasource.CompanyId.HasValue) || ds.Owner == datasource.Owner)
                                                    .Where(ds => ds.ID == datasource.ID)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();

            if (_datasourceEntity == null)
            {
                return null;
            }

            _datasourceEntity.Hydrate(datasource);
            _context.Datasource.Update(_datasourceEntity);

            return _datasourceEntity;
        }

        bool IDatasourceRepository.Delete(DatasourceModel model)
        {
            DatasourceModel _datasource = _context.Datasource
                                            .Where(ds => (ds.CompanyId == model.CompanyId && model.CompanyId.HasValue) || ds.Owner == model.Owner)
                                            .AsNoTracking()
                                            .FirstOrDefault(ds => ds.ID == model.ID);

            if (_datasource != null)
            {
                _context.Remove(_datasource);
            }

            return true;
        }

        async Task<int> IDatasourceRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<bool> IDatasourceRepository.IsAttachedToMetric(DatasourceModel datasource)
        {
            MetricModel _metric = await _context.Metric.Where(m => m.DatasourceId == datasource.ID).AsNoTracking().FirstOrDefaultAsync();

            if (_metric != null)
            {
                return true;
            }

            return false;
        }

        public async Task<DatasourceModel> GetSystem(DatasourceModel model)
        {
            return await _context.Datasource.Where(ds => ds.Type == model.Type).AsNoTracking().FirstOrDefaultAsync();
        }
    }
}