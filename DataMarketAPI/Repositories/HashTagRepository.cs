﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class HashTagRepository : IHashTagRepository
    {
        private readonly DatabaseContext _context;

        public HashTagRepository(DatabaseContext context)
        {
            _context = context;
        }

        Task<HashTagModel> IHashTagRepository.Create(string Name)
        {
            throw new System.NotImplementedException();
        }

        async Task IHashTagRepository.MetricBinding(Guid iD, string hashTags)
        {
            List<string> htCollection = hashTags.Split(",").ToList();
            List<HashTagToMetricModel> _htm = new List<HashTagToMetricModel>();

            foreach (string _ht in htCollection)
            {
                HashTagModel _htEntity = await _context.HashTag.FirstOrDefaultAsync(ht => ht.Name == _ht);
                if (_htEntity != null)
                {
                    _htm.Add(new HashTagToMetricModel { MetricId = iD, HashTagId = _htEntity.ID });
                }
            }

            _context.HashTagMetric.RemoveRange(await _context.HashTagMetric.Where(htm => htm.MetricId == iD).ToListAsync());
            await _context.HashTagMetric.AddRangeAsync(_htm);

            await _context.SaveChangesAsync();
        }

        Task<int> IHashTagRepository.Save()
        {
            throw new System.NotImplementedException();
        }

        async Task<IEnumerable<HashTagModel>> IHashTagRepository.Search(string keyword)
        {
            return await _context.HashTag.Where(ht => ht.Name.Contains(keyword)).ToListAsync();
        }

        async Task<int> IHashTagRepository.UpdateCount(int hashTagId)
        {
            HashTagModel _ht = await _context.HashTag.FirstOrDefaultAsync(ht => ht.ID == hashTagId);

            //if (_ht != null)
            //{
            //    _ht.UsageCount = _context.HashTagToMetric.Where(e => e.HashTagId == hashTagId).Count();
            //    return _ht.UsageCount;
            //}

            return 0;
        }

        async Task IHashTagRepository.Upsert(string hashTags)
        {
            List<string> htCollection = hashTags.Split(",").ToList();

            foreach (string _ht in htCollection)
            {
                HashTagModel htEntity = await _context.HashTag.FirstOrDefaultAsync(ht => ht.Name.Contains(_ht));
                if (htEntity == null)
                {
                    _context.HashTag.Add(new HashTagModel { Name = _ht, UsageCount = 1 });
                }
                else
                {
                }
            }

            await _context.SaveChangesAsync();
        }
    }
}