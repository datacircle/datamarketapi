﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class InsertLogRepository : IInsertLogRepository
    {
        private readonly DatabaseContext _context;

        public InsertLogRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<InsertLogModel> IInsertLogRepository.Read(int Id)
        {
            return await _context.InsertLog.FirstOrDefaultAsync(il => il.ID == Id);
        }

        async Task<bool> IInsertLogRepository.Write(InsertLogModel model)
        {
            model.CreatedAt = DateTime.Now;
            await _context.InsertLog.AddAsync(model);
            return Convert.ToBoolean(await _context.SaveChangesAsync());
        }
    }
}