﻿using DataMarketAPI.Models;

namespace DataMarketAPI.Repositories
{
    public interface ICardRepository
    {
        int GetConsumerCount(User user);

        int GetDatasourceCount(User user);

        int GetMetricCount(User user);

        int GetRequestCount(User user);
    }
}