﻿using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<CategoyModel>> GetAll();

        Task<CategoyModel> Get(string name);
    }
}