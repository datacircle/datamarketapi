using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface ICompanyRepository
    {
        Task<CompanyModel> Create(CompanyModel company);

        Task<CompanyModel> Get(CompanyModel company);

        Task<CompanyModel> GetByName(CompanyModel company);

        Task<CompanyModel> GetByNormalizedName(CompanyModel company);

        Task<CompanyModel> Update(CompanyModel company);

        Task<bool> Delete(CompanyModel company);

        Task<int> Save();
    }
}