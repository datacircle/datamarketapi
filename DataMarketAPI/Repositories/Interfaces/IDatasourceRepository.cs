using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IDatasourceRepository
    {
        Task<DatasourceModel> Create(DatasourceModel model);

        Task<DatasourceModel> Get(DatasourceModel model);

        Task<DatasourceModel> GetUncencorsed(DatasourceModel model);

        Task<DatasourceModel> GetSystem(DatasourceModel model);

        Task<IEnumerable<DatasourceModel>> GetAll(DatasourceModel model);

        Task<DatasourceModel> Update(DatasourceModel model);

        bool Delete(DatasourceModel model);

        Task<int> Save();

        Task<bool> IsAttachedToMetric(DatasourceModel model);
    }
}