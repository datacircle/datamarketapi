﻿using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IHashTagRepository
    {
        Task<IEnumerable<HashTagModel>> Search(string keyword);

        Task<HashTagModel> Create(string Name);

        Task<int> UpdateCount(int hashTagId);

        Task<int> Save();

        Task Upsert(string hashTags);

        Task MetricBinding(Guid iD, string hashTags);
    }
}