﻿using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IInsertLogRepository
    {
        Task<InsertLogModel> Read(int Id);

        Task<bool> Write(InsertLogModel model);
    }
}