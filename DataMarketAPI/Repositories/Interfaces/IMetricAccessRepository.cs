﻿using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IMetricAccessRepository
    {
        Task<MetricAccessMapModel> GetAsync(MetricAccessMapModel model);

        Task<MetricAccessMapModel> GetAsync(Guid id);

        Task<MetricAccessMapModel> Create(MetricAccessMapModel model);

        Task<IEnumerable<MetricAccessMapModel>> GetAllAsync(MetricAccessMapModel model);

        Task<IEnumerable<MetricAccessMapModel>> GetAllAsync();

        Task<MetricAccessMapModel> FindByTargetUserAndMetric(int userId, Guid MetricId);

        bool Delete(MetricAccessMapModel model);

        Task<int> Save();
    }
}