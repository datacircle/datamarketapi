using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IMetricCommentRepository
    {
        Task<MetricCommentModel> Create(MetricCommentModel model);

        Task<IEnumerable<MetricCommentModel>> GetByRequest(MetricCommentModel model);

        Task<bool> Delete(MetricCommentModel model);

        Task<int> Save();
    }
}