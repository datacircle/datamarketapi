using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IMetricRepository
    {
        Task<bool> UserHasViewAccess(Guid metricId, int userId, int? companyId);

        Task<bool> UserHasEditAccess(Guid metricId, int userId, int? companyId);

        Task<MetricModel> Create(MetricModel metric);

        Task<MetricModel> Get(MetricModel metric);

        Task<MetricModel> Get(Guid id);

        Task<double> Rating(MetricModel metric);

        Task<IEnumerable<MetricModel>> GetAllForCompany(MetricModel metric);

        Task<IEnumerable<MetricModel>> GetAllForSearch(MetricModel metric);

        Task<IEnumerable<MetricModel>> GetMetricThatNeedUpdate();

        Task<IEnumerable<MetricModel>> GetAllForMarketplace();

        Task<IEnumerable<MetricModel>> GetAllForRequestCreation(MetricModel metric);

        Task<IEnumerable<MetricModel>> Search(string searchTerm, string[] hashtags, int size = 50, int page = 1);

        Task<IEnumerable<MetricModel>> SearchByCategory(string categoryName, int size = 50, int page = 1);

        Task<IEnumerable<MetricModel>> SearchTop50();

        Task<IEnumerable<MetricModel>> SearchTrending50();

        Task<IEnumerable<MetricModel>> SearchNew50();

        Task<IEnumerable<MetricModel>> SearchPaid();

        Task<MetricModel> Update(MetricModel metric);

        Task<MetricModel> UpdateProcessingStatus(MetricModel metric);

        Task<MetricModel> EagerLoad(MetricModel metric);
        Task<bool> HasCostAsync(Guid metricId);
        Task<bool> Delete(MetricModel metric);

        Task<int> Save();

        Task<MetricModel> GetWithDatasource(MetricModel metric);

        Task<bool> UpdateCommentCount(Guid id);
    }
}