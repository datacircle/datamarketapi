using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IMetricTransferRepository
    {
        Task<MetricTransferModel> Create(MetricTransferModel model);

        Task<MetricTransferModel> Get(MetricTransferModel model);

        Task<IEnumerable<MetricTransferModel>> GetAll(MetricTransferModel model);

        Task<IEnumerable<MetricTransferModel>> GetAllThatNeedProcessing();

        Task<MetricTransferModel> Update(MetricTransferModel model);

        Task<MetricTransferModel> Hydrate(MetricTransferModel model);

        bool Delete(MetricTransferModel model);

        Task<int> Save();
    }
}