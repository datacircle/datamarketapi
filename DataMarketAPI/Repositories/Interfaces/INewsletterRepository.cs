using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface INewsletterRepository
    {
        Task<NewsletterModel> Create(NewsletterModel newsletter);

        Task<NewsletterModel> Get(string email);

        Task<bool> DoubleOptIn(string token);

        Task<bool> Unsubscribe(string token);

        Task<bool> Delete(NewsletterModel newsletter);

        Task<int> Save();
    }
}