using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IQueryLogRepository
    {
        Task<QueryLogModel> Read(int Id);

        Task<IEnumerable<QueryLogModel>> ReadByDatasource(DatasourceModel datasourceModel);

        Task<IEnumerable<QueryLogModel>> ReadByMetric(MetricModel metricModel);

        Task<IEnumerable<QueryLogModel>> GetByCompanyWithUserFallback(User user);

        Task<bool> Write(QueryLogModel model);
    }
}