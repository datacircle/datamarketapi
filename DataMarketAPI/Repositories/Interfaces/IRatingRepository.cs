﻿using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRatingRepository
    {
        Task<RatingModel> GetByUserMetric(RatingModel model);

        Task<RatingModel> Create(RatingModel model);

        Task<RatingModel> Get(RatingModel model);

        Task<IEnumerable<RatingModel>> AllForMetric(RatingModel model);

        Task<bool> UpdateRatingOfMetric(RatingModel model, double avg);

        Task<bool> Update(RatingModel model);

        bool Delete(RatingModel model);

        Task<int> Save();
    }
}