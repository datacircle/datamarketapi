﻿using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRepository<TEntity> where TEntity : BaseModel, new()
    {
        IList<TEntity> GetAll();

        IList<TEntity> GetAllWithInclude(params Expression<Func<TEntity, object>>[] includes);

        Task<TEntity> GetAsync(string id);

        Task<TEntity> GetAsync(TEntity entity);

        Task<TEntity> GetAsyncWithInclude(string id, Expression<Func<TEntity, object>>[] includes);

        Task<TEntity> GetAsyncWithInclude(TEntity entity, Expression<Func<TEntity, object>>[] includes);

        Task InsertAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteAsync(string id);

        Task DeleteAsync(TEntity entity);

        IList<TEntity> Search(Expression<Func<TEntity, bool>> where);

        IList<TEntity> SearchWithInclude(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includes);

        Task EagerLoad(TEntity entity, params Expression<Func<TEntity, object>>[] includes);
    }
}