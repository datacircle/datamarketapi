using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRequestBetaRepository
    {
        Task<RequestBetaModel> Create(RequestBetaModel requestBeta);

        Task<int> Save();
    }
}