using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IUserRepository
    {

        Task UpdateLastLogin(User user);

        Task UpdateUserDetails(User user);
        Task<User> FindByVerificationToken(string verificationToken);

        Task<User> FindByEmail(string email);

        Task<CompanyModel> GetCompany(User user);

        Task<User> FindForShare(string Email, string PublicId);

        Task<int> Save();
    }
}