using DataMarketAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRequestCommentRepository
    {
        Task<RequestCommentModel> Create(RequestCommentModel model);

        Task<IEnumerable<RequestCommentModel>> GetByRequest(RequestCommentModel model);

        Task<bool> Delete(RequestCommentModel model);

        Task<int> Save();
    }
}