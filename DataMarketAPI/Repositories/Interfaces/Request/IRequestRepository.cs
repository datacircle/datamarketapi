using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRequestRepository
    {
        Task<RequestModel> Create(RequestModel request);

        Task<RequestModel> Get(RequestModel request);

        Task<bool> UpdateCommentCount(Guid id);

        Task<bool> UpdateVoteCount(Guid id);

        Task<RequestModel> GetUncensored(RequestModel request);

        Task<IEnumerable<RequestModel>> GetAll(RequestModel request);

        Task<IEnumerable<RequestModel>> GetAllUncensored();

        Task<RequestModel> Update(RequestModel request);

        bool Delete(RequestModel request);

        Task<int> Save();
    }
}