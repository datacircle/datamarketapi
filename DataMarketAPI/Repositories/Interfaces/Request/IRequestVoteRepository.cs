using DataMarketAPI.Models;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public interface IRequestVoteRepository
    {
        Task<bool> Upvote(RequestVoteModel model);

        Task<bool> Downvote(RequestVoteModel model);

        Task<bool> Unvote(RequestVoteModel model);

        Task<int> Save();
    }
}