﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class MetricAccessRepository : IMetricAccessRepository
    {
        private readonly DatabaseContext _context;

        public MetricAccessRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<MetricAccessMapModel> IMetricAccessRepository.Create(MetricAccessMapModel model)
        {
            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            await _context.MetricAccessMap.AddAsync(model);
            await _context.SaveChangesAsync();

            await _context.Entry(model).Reference(m => m.TargetUser).LoadAsync();

            return model;
        }

        bool IMetricAccessRepository.Delete(MetricAccessMapModel model)
        {
            _context.MetricAccessMap.Remove(model);
            _context.SaveChanges();

            return true;
        }

        async Task<MetricAccessMapModel> IMetricAccessRepository.FindByTargetUserAndMetric(int userId, Guid metricId)
        {
            return await _context.MetricAccessMap.Where(m => m.TargetUserId == userId && m.MetricId == metricId).AsNoTracking().FirstOrDefaultAsync();
        }

        async Task<IEnumerable<MetricAccessMapModel>> IMetricAccessRepository.GetAllAsync(MetricAccessMapModel model)
        {
            return await _context.MetricAccessMap
                .Where(m => m.ActionableUserId == model.ActionableUserId && m.MetricId == model.MetricId)
                .Include(m => m.TargetUser)
                .ToListAsync();
        }

        Task<IEnumerable<MetricAccessMapModel>> IMetricAccessRepository.GetAllAsync()
        {
            throw new NotImplementedException();
        }

        async Task<MetricAccessMapModel> IMetricAccessRepository.GetAsync(MetricAccessMapModel model)
        {
            return await _context.MetricAccessMap.Where(m => m.ActionableUserId == model.ActionableUserId).FirstOrDefaultAsync(m => m.ID == model.ID);
        }

        Task<MetricAccessMapModel> IMetricAccessRepository.GetAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<int> IMetricAccessRepository.Save()
        {
            throw new NotImplementedException();
        }
    }
}