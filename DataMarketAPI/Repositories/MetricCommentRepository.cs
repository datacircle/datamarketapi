using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class MetricCommentRepository : IMetricCommentRepository
    {
        private readonly DatabaseContext _context;

        public MetricCommentRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<MetricCommentModel> IMetricCommentRepository.Create(MetricCommentModel model)
        {
            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            return (await _context.MetricComment.AddAsync(model)).Entity;
        }

        async Task<bool> IMetricCommentRepository.Delete(MetricCommentModel model)
        {
            MetricCommentModel metricComment = await _context.MetricComment
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync(rc => rc.Owner == model.Owner);

            _context.Remove(metricComment);

            return true;
        }

        async Task<IEnumerable<MetricCommentModel>> IMetricCommentRepository.GetByRequest(MetricCommentModel model)
        {
            return await _context.MetricComment.Where(rc => rc.MetricId == model.MetricId).Include(rc => rc.User).ToListAsync();
        }

        async Task<int> IMetricCommentRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}