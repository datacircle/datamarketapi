using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class MetricRepository : IMetricRepository
    {
        private readonly DatabaseContext _context;

        public MetricRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<MetricModel> IMetricRepository.Create(MetricModel metric)
        {
            metric.Datasource = null;
            metric.CreatedAt = metric.UpdatedAt = DateTime.Now;
            return (await _context.Metric.AddAsync(metric)).Entity;
        }

        // Simple GET has Security
        // In order to get the details the metric must be OPEN or the User is part of the company
        async Task<MetricModel> IMetricRepository.Get(MetricModel metric)
        {
            MetricModel _metric = _context.Metric.Local
                    .FirstOrDefault(m => m.ID == metric.ID);

            if (_metric == null)
            {
                _metric = await _context.Metric
                                .AsNoTracking()
                                .FirstOrDefaultAsync(m => m.ID == metric.ID);
            }

            if (_metric != null)
            {
                _metric.User = await _context.Users.FirstOrDefaultAsync(u => u.Id == _metric.Owner);
            }

            return _metric;
        }

        async Task<MetricModel> IMetricRepository.Get(Guid id)
        {
            return await _context.Metric.FirstOrDefaultAsync(m => m.ID == id);
        }

        // GET with Datasource
        // As it gets Datasource Details it must be accessible only within the company
        async Task<MetricModel> IMetricRepository.GetWithDatasource(MetricModel metric)
        {
            return await _context.Metric
                        .Include(m => m.Datasource)
                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(m => m.ID == metric.ID);
        }

        // Simple GET ALL has security
        // This is used to populate the dropdowns for EDITING
        async Task<IEnumerable<MetricModel>> IMetricRepository.GetAllForCompany(MetricModel metric)
        {
            return await _context.Metric
                        .Include(m => m.Company)
                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner)
                        .AsNoTracking()
                        .ToListAsync();
        }

        //TODO finish this
        // During search look only for the ones available under that company or that are open
        async Task<IEnumerable<MetricModel>> IMetricRepository.GetAllForSearch(MetricModel metric)
        {
            return await _context.Metric
                        .Include(m => m.Company)
                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner || m.Open == true)
                        .AsNoTracking()
                        .ToListAsync();
        }

        // Load for Marketplace front-page should show only the ones that are OPEN and ordered by Rating
        async Task<IEnumerable<MetricModel>> IMetricRepository.GetAllForMarketplace()
        {
            return await _context.Metric
                         .Where(m => m.Open == true)
                         .OrderBy(m => m.Rating)
                         .AsNoTracking()
                         .ToListAsync();
        }

        async Task<IEnumerable<MetricModel>> IMetricRepository.GetAllForRequestCreation(MetricModel metric)
        {
            return await _context.Metric
                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner || m.Open == true)
                        .AsNoTracking()
                        .ToListAsync();
        }

        async Task<IEnumerable<MetricModel>> IMetricRepository.GetMetricThatNeedUpdate()
        {
            return await _context.Metric
                        .Include(m => m.Datasource)
                        .Where(r => r.LastPool.AddSeconds(r.Interval) <= DateTime.Now)
                        .Where(r => r.Processing == false)
                        .AsNoTracking()
                        .ToListAsync();
        }

        async Task<MetricModel> IMetricRepository.Update(MetricModel metric)
        {
            metric.UpdatedAt = DateTime.Now;
            _context.Metric.Update(metric);

            return metric;
        }

        async Task<MetricModel> IMetricRepository.UpdateProcessingStatus(MetricModel metric)
        {
            _context.Entry(metric).Property(m => m.Processing).IsModified = true;
            await _context.SaveChangesAsync();

            return metric;
        }

        async Task<bool> IMetricRepository.Delete(MetricModel metric)
        {
            MetricModel _metricEntity = _context.Metric
                                        .Where(m => m.ID == metric.ID)
                                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner)
                                        .AsNoTracking()
                                        .FirstOrDefault();

            if (_metricEntity == null)
            {
                return false;
            }

            List<QueryLogModel> _queryEntityCollection = await _context.QueryLog
                .Where(q => q.MetricID == _metricEntity.ID).ToListAsync();

            _context.RemoveRange(_queryEntityCollection);
            _context.Remove(_metricEntity);

            return true;
        }

        async Task<int> IMetricRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<double> IMetricRepository.Rating(MetricModel metric)
        {
            return await _context.Metric
                        .Where(m => m.ID == metric.ID)
                        .Where(m => (m.CompanyId == metric.CompanyId && metric.CompanyId.HasValue) || m.Owner == metric.Owner)
                        .AsNoTracking()
                        .Select(m => m.Rating)
                        .FirstOrDefaultAsync();
        }

        public async Task<bool> UserHasViewAccess(Guid metricId, int userId, int? companyId)
        {
            MetricAccessMapModel _m = await _context.MetricAccessMap
                .Where(m => m.MetricId == metricId && m.TargetUserId == userId)
                .FirstOrDefaultAsync();

            if (_m != null)
            {
                return true;
            }

            MetricModel _metricEntity = await _context.Metric
                                                // is within company or is open
                                                .Where(m => (m.CompanyId == companyId && companyId.HasValue) || m.Owner == userId || m.Open == true)
                                                .Where(m => m.ID == metricId)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();

            return _metricEntity != null;
        }

        public async Task<bool> UserHasEditAccess(Guid metricId, int userId, int? companyId)
        {
            MetricModel _metricEntity = await _context.Metric
                                                // is within company
                                                .Where(m => (m.CompanyId == companyId && companyId.HasValue) || m.Owner == userId)
                                                .Where(m => m.ID == metricId)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();

            return _metricEntity != null;
        }

        async Task<IEnumerable<MetricModel>> IMetricRepository.Search(string searchTerm, string[] hashtags, int size = 50, int page = 1)
        {
            var query = _context.Metric
                                .Include(m => m.User)
                                .Include(m => m.Company)
                                .Where(m => m.Open == true)
                                .Where(m => m.Approved == true)
                                .Where(
                                    m => m.Title.ToLower().Contains(searchTerm) ||
                                    m.Title == searchTerm ||
                                    m.Description.Contains(searchTerm)
                                );

            List<MetricModel> _metricCollection = await query.AsNoTracking().ToListAsync();

            if (hashtags.Length > 0)
            {
                _metricCollection = _metricCollection
                    .Where(m => m.HashTags != null)
                    .Where(m => hashtags.Any(s => m.HashTags.Contains(s))).ToList();
            }

            return _metricCollection;
        }

        async Task<MetricModel> IMetricRepository.EagerLoad(MetricModel metric)
        {
            _context.Attach(metric);
            await _context.Entry(metric).Reference(m => m.Datasource).LoadAsync();

            return metric;
        }

        public async Task<IEnumerable<MetricModel>> SearchByCategory(string categoryName, int size = 50, int page = 1)
        {
            CategoyModel _categoryEntity = await _context.Category.FirstOrDefaultAsync(c => c.Name == categoryName);

            if (_categoryEntity == null)
            {
                return new List<MetricModel>();
            }

            List<MetricModel> _metricCollection = await _context.Metric
                                                               .Where(m => m.Open == true)
                                                               .Where(m => m.Approved == true)
                                                               .Where(m => m.CategoryId == _categoryEntity.ID)
                                                               .Include(m => m.User)
                                                               .Include(m => m.Company)
                                                               .OrderByDescending(m => m.Rating)
                                                               .Take(size)
                                                               .AsNoTracking()
                                                               .ToListAsync();

            return _metricCollection;
        }

        public async Task<IEnumerable<MetricModel>> SearchTop50()
        {
            List<MetricModel> _metricCollection = await _context.Metric
                                                               .Include(m => m.User)
                                                               .Include(m => m.Company)
                                                               .Where(m => m.Open == true)
                                                               .Where(m => m.Approved == true)
                                                               .OrderByDescending(m => m.Rating)
                                                               .Take(50)
                                                               .AsNoTracking()
                                                               .ToListAsync();

            return _metricCollection;
        }

        public async Task<IEnumerable<MetricModel>> SearchTrending50()
        {
            List<MetricModel> _metricCollection = await _context.Metric
                                                               .Include(m => m.User)
                                                               .Include(m => m.Company)
                                                               .Where(m => m.Open == true)
                                                               .Where(m => m.Approved == true)
                                                               .OrderByDescending(m => m.CreatedAt)
                                                               .Take(50)
                                                               .OrderBy(m => m.Rating)
                                                               .AsNoTracking()
                                                               .ToListAsync();

            return _metricCollection;
        }

        public async Task<IEnumerable<MetricModel>> SearchNew50()
        {
            List<MetricModel> _metricCollection = await _context.Metric
                                                               .Include(m => m.User)
                                                               .Include(m => m.Company)
                                                               .Where(m => m.Open == true)
                                                               .Where(m => m.Approved == true)
                                                               .OrderByDescending(m => m.CreatedAt)
                                                               .Take(50)
                                                               .AsNoTracking()
                                                               .ToListAsync();

            return _metricCollection;
        }

        public async Task<IEnumerable<MetricModel>> SearchPaid()
        {
            List<MetricModel> _metricCollection = await _context.Metric
                                                               .Include(m => m.User)
                                                               .Include(m => m.Company)
                                                               .Where(m => m.Open == true)
                                                               .Where(m => m.Approved == true)
                                                               .Where(m => m.Cost > 1)
                                                               .OrderByDescending(m => m.CreatedAt)
                                                               .Take(50)
                                                               .AsNoTracking()
                                                               .ToListAsync();

            return _metricCollection;
        }

        async Task<bool> IMetricRepository.UpdateCommentCount(Guid id)
        {
            MetricModel _e = await _context.Metric
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(r => r.ID == id);

            _e.CommentCount = await _context.MetricComment.Where(mc => mc.MetricId == id).CountAsync();

            _context.Metric.Update(_e);

            return true;
        }

        public async Task<bool> HasCostAsync(Guid metricId)
        {
            MetricModel metric = await _context.Metric.Where(m => m.ID == metricId).FirstOrDefaultAsync();

            if (metric == null)
            {
                return false;
            }

            return metric.Cost > 0;
        }
    }
}