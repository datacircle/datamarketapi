using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class MetricTransferRepository : IMetricTransferRepository
    {
        private readonly DatabaseContext _context;

        public MetricTransferRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<MetricTransferModel> IMetricTransferRepository.Create(MetricTransferModel model)
        {
            var test = _context.ChangeTracker.Entries();

            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            return (await _context.MetricTransfer.AddAsync(model)).Entity;
        }

        async Task<MetricTransferModel> IMetricTransferRepository.Get(MetricTransferModel model)
        {
            throw new NotImplementedException();
        }

        async Task<IEnumerable<MetricTransferModel>> IMetricTransferRepository.GetAll(MetricTransferModel model)
        {
            return await _context.MetricTransfer
                        .Where(mt => (mt.TargetCompanyID == model.TargetCompanyID && mt.TargetCompanyID != null) || mt.ActionableUser == model.ActionableUser)
                        .OrderBy(mt => mt.CreatedAt)
                        .AsNoTracking()
                        .ToListAsync();
        }

        async Task<MetricTransferModel> IMetricTransferRepository.Update(MetricTransferModel model)
        {
            var query = _context.MetricTransfer.Where(mt => mt.Id == model.Id);

            if (model.TargetCompanyID != null)
            {
                query.Where(mt => mt.TargetCompanyID == model.TargetCompanyID);
            }
            else
            {
                query.Where(mt => mt.ActionableUser == model.ActionableUser);
            }

            MetricTransferModel _metricTransferEntity = await query.FirstOrDefaultAsync();

            if (_metricTransferEntity == null)
            {
                return null;
            }

            _metricTransferEntity.Status = model.Status;
            _metricTransferEntity.Message = model.Message;

            return _metricTransferEntity;
        }

        bool IMetricTransferRepository.Delete(MetricTransferModel model)
        {
            throw new NotImplementedException();
        }

        async Task<int> IMetricTransferRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        public async Task<MetricTransferModel> Hydrate(MetricTransferModel model)
        {
            return await _context.MetricTransfer
                .Include(mt => mt.TargetDatasource)
                .Include(mt => mt.Metric).ThenInclude(m => m.Datasource)
                .Where(mt => mt.Id == model.Id)
                .Where(mt => mt.ActionableUser == model.ActionableUser)
                .AsNoTracking()
                .FirstAsync();
        }

        public async Task<IEnumerable<MetricTransferModel>> GetAllThatNeedProcessing()
        {
            return await _context.MetricTransfer
                .Where(mt => mt.Status == MetricTransferModel.StatusCode.Queued)
                .Where(mt => mt.DownloadAsFile == false)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}