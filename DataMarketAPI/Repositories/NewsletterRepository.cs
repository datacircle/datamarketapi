using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class NewsletterRepository : INewsletterRepository
    {
        private readonly DatabaseContext _context;

        public NewsletterRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<NewsletterModel> INewsletterRepository.Create(NewsletterModel newsletter)
        {
            newsletter.CreatedAt = newsletter.UpdatedAt = DateTime.Now;
            return (await _context.Newsletter.AddAsync(newsletter)).Entity;
        }

        async Task<bool> INewsletterRepository.Unsubscribe(string token)
        {
            NewsletterModel entity = await _context.Newsletter
                                                .Where(n => n.UnsubscriptionToken == token)
                                                .FirstOrDefaultAsync();
            if (entity != null)
            {
                entity.Subscribed = false;
                return true;
            }

            return false;
        }

        async Task<bool> INewsletterRepository.DoubleOptIn(string token)
        {
            NewsletterModel _newsletterEntity = await _context.Newsletter
            .Where(n => n.DoubleOptInToken == token)
            .FirstOrDefaultAsync();

            if (_newsletterEntity != null)
            {
                _newsletterEntity.DoubleOptedIn = true;
                return true;
            }

            return false;
        }

        async Task<NewsletterModel> INewsletterRepository.Get(string email)
        {
            return await _context.Newsletter.SingleOrDefaultAsync(n => n.Email == email);
        }

        async Task<bool> INewsletterRepository.Delete(NewsletterModel newsletter)
        {
            NewsletterModel _newsletter = await _context.Newsletter
                .Where(n => n.Email == newsletter.Email)
                .FirstOrDefaultAsync();

            if (_newsletter != null)
            {
                _context.Remove(_newsletter);
                return true;
            }

            return false;
        }

        async Task<int> INewsletterRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}