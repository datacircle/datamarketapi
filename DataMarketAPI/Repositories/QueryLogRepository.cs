using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class QueryLogRepository : IQueryLogRepository
    {
        private readonly DatabaseContext _context;

        public QueryLogRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<QueryLogModel> IQueryLogRepository.Read(int Id)
        {
            return await _context.QueryLog.FirstOrDefaultAsync(ql => ql.ID == Id);
        }

        async Task<IEnumerable<QueryLogModel>> IQueryLogRepository.GetByCompanyWithUserFallback(User user)
        {
            var query = _context.QueryLog
                .Include(q => q.Datasource)
                .Include(q => q.Metric)
                .OrderByDescending(ql => ql.ID);

            if (user.CompanyId != null)
            {
                query.Where(ql => ql.Metric.CompanyId == user.CompanyId);
            }
            else
            {
                query.Where(ql => ql.Metric.Owner == user.Id);
            }

            return await query.ToListAsync();
        }

        async Task<IEnumerable<QueryLogModel>> IQueryLogRepository.ReadByDatasource(DatasourceModel datasourceModel)
        {
            return await _context.QueryLog.Where(ql => ql.DatasourceID == datasourceModel.ID).ToListAsync();
        }

        async Task<IEnumerable<QueryLogModel>> IQueryLogRepository.ReadByMetric(MetricModel metricModel)
        {
            return await _context.QueryLog.Where(ql => ql.MetricID == metricModel.ID).ToListAsync();
        }

        async Task<bool> IQueryLogRepository.Write(QueryLogModel model)
        {
            await _context.QueryLog.AddAsync(model);
            return Convert.ToBoolean(await _context.SaveChangesAsync());
        }
    }
}