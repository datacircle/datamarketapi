﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class RatingRepository : IRatingRepository
    {
        private readonly DatabaseContext _context;

        public RatingRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<RatingModel> IRatingRepository.Create(RatingModel model)
        {
            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            return (await _context.Rating.AddAsync(model)).Entity;
        }

        bool IRatingRepository.Delete(RatingModel model)
        {
            _context.Rating.Remove(model);
            return true;
        }

        async Task<RatingModel> IRatingRepository.Get(RatingModel model)
        {
            return await _context.Rating.AsNoTracking().Where(r => r.ID == model.ID).FirstAsync();
        }

        async Task<RatingModel> IRatingRepository.GetByUserMetric(RatingModel model)
        {
            return await _context.Rating
                        .AsNoTracking()
                        .Where(r => r.Owner == model.Owner && r.MetricID == model.MetricID)
                        .FirstOrDefaultAsync();
        }

        async Task<int> IRatingRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<IEnumerable<RatingModel>> IRatingRepository.AllForMetric(RatingModel model)
        {
            return await _context.Rating.Where(r => r.MetricID == model.MetricID).ToListAsync();
        }

        async Task<bool> IRatingRepository.Update(RatingModel model)
        {
            RatingModel _ratingEntity = await _context.Rating
                                              .Where(r => r.MetricID == model.MetricID && r.Owner == model.Owner)
                                              .FirstOrDefaultAsync();

            if (_ratingEntity == null)
            {
                return false;
            }

            _ratingEntity.Value = model.Value;
            _context.Rating.Update(_ratingEntity);

            return true;
        }

        async Task<bool> IRatingRepository.UpdateRatingOfMetric(RatingModel model, double avg)
        {
            MetricModel _metricEntity = await _context.Metric.Where(m => m.ID == model.MetricID).FirstAsync();

            _metricEntity.Rating = avg;
            _context.Metric.Update(_metricEntity);

            return true;
        }
    }
}