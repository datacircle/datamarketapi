using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class RequestCommentRepository : IRequestCommentRepository
    {
        private readonly DatabaseContext _context;

        public RequestCommentRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<RequestCommentModel> IRequestCommentRepository.Create(RequestCommentModel model)
        {
            model.CreatedAt = model.UpdatedAt = DateTime.Now;
            return (await _context.RequestComment.AddAsync(model)).Entity;
        }

        async Task<bool> IRequestCommentRepository.Delete(RequestCommentModel model)
        {
            RequestCommentModel _requestComment = await _context.RequestComment
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync(rc => rc.Owner == model.Owner);

            _context.Remove(_requestComment);

            return true;
        }

        async Task<IEnumerable<RequestCommentModel>> IRequestCommentRepository.GetByRequest(RequestCommentModel model)
        {
            return await _context.RequestComment.Where(rc => rc.RequestId == model.RequestId).Include(rc => rc.User).ToListAsync();
        }

        async Task<int> IRequestCommentRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}