using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class RequestRepository : IRequestRepository
    {
        private readonly DatabaseContext _context;

        public RequestRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<RequestModel> IRequestRepository.Create(RequestModel request)
        {
            request.CreatedAt = request.UpdatedAt = DateTime.Now;
            await _context.Request.AddAsync(request);
            return request;
        }

        async Task<RequestModel> IRequestRepository.Get(RequestModel request)
        {
            return await _context.Request
                        .Where(r => r.Owner == request.Owner)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(r => r.ID == request.ID);
        }

        async Task<IEnumerable<RequestModel>> IRequestRepository.GetAll(RequestModel request)
        {
            return await _context.Request
                        .Where(r => r.Owner == request.Owner)
                        .AsNoTracking()
                        .ToListAsync();
        }

        async Task<IEnumerable<RequestModel>> IRequestRepository.GetAllUncensored()
        {
            return await _context.Request
                        .Include(r => r.User)
                        .AsNoTracking()
                        .ToListAsync();
        }

        async Task<RequestModel> IRequestRepository.GetUncensored(RequestModel request)
        {
            return await _context.Request
                        .Include(r => r.User)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(r => r.ID == request.ID);
        }

        async Task<RequestModel> IRequestRepository.Update(RequestModel request)
        {
            RequestModel _requestEntity = await _context.Request
                                                .Where(r => r.Owner == request.Owner)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync(r => r.ID == request.ID);

            if (_requestEntity == null)
            {
                return null;
            }

            _requestEntity.Hydrate(request);
            _context.Request.Update(_requestEntity);

            return _requestEntity;
        }

        bool IRequestRepository.Delete(RequestModel request)
        {
            RequestModel _requestEntity = _context.Request
                .Where(r => r.Owner == request.Owner)
                .AsNoTracking()
                .FirstOrDefault(r => r.ID == request.ID);

            if (_requestEntity != null)
            {
                _context.Remove(_requestEntity);
            }

            return true;
        }

        async Task<int> IRequestRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<bool> IRequestRepository.UpdateCommentCount(Guid id)
        {
            RequestModel _requestEntity = await _context.Request
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(r => r.ID == id);

            _requestEntity.CommentCount = await _context.RequestComment.Where(rc => rc.RequestId == id).CountAsync();

            _context.Request.Update(_requestEntity);

            return true;
        }

        async Task<bool> IRequestRepository.UpdateVoteCount(Guid id)
        {
            int Downvotes = await _context.RequestVote.Where(v => v.RequestId == id && v.Vote == RequestVoteModel.VoteType.Downvote).CountAsync();
            int Upvotes = await _context.RequestVote.Where(v => v.RequestId == id && v.Vote == RequestVoteModel.VoteType.Upvote).CountAsync();

            RequestModel _r = await _context.Request.FirstOrDefaultAsync(r => r.ID == id);

            _r.UpvoteCount = Upvotes - Downvotes;
            _context.Update(_r);

            return true;
        }
    }
}