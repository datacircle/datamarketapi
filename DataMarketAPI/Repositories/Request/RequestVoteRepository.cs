using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class RequestVoteRepository : IRequestVoteRepository
    {
        private readonly DatabaseContext _context;

        public RequestVoteRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<bool> IRequestVoteRepository.Upvote(RequestVoteModel model)
        {
            RequestVoteModel _vote = await _context.RequestVote.Where(v => v.Owner == model.Owner && v.RequestId == model.RequestId).FirstOrDefaultAsync();

            if (_vote == null)
            {
                model.CreatedAt = model.UpdatedAt = DateTime.Now;
                await _context.RequestVote.AddAsync(model);
                return true;
            }

            if (_vote.Vote == RequestVoteModel.VoteType.Downvote)
            {
                _vote.Vote = RequestVoteModel.VoteType.Upvote;
                _vote.UpdatedAt = DateTime.Now;
                _context.RequestVote.Update(_vote);

                return true;
            }

            return true;
        }

        async Task<bool> IRequestVoteRepository.Downvote(RequestVoteModel model)
        {
            RequestVoteModel _vote = await _context.RequestVote.Where(v => v.Owner == model.Owner && v.RequestId == model.RequestId).FirstOrDefaultAsync();

            if (_vote == null)
            {
                model.CreatedAt = model.UpdatedAt = DateTime.Now;
                await _context.RequestVote.AddAsync(model);
                return true;
            }

            if (_vote.Vote == RequestVoteModel.VoteType.Upvote)
            {
                _vote.Vote = RequestVoteModel.VoteType.Downvote;
                _vote.UpdatedAt = DateTime.Now;
                _context.RequestVote.Update(_vote);

                return true;
            }

            return true;
        }

        async Task<int> IRequestVoteRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<bool> IRequestVoteRepository.Unvote(RequestVoteModel model)
        {
            RequestVoteModel _vote = await _context.RequestVote.FirstOrDefaultAsync(v => v.Owner == model.Owner && v.RequestId == model.RequestId);

            if (_vote != null)
            {
                _context.Remove(_vote);
            }

            return true;
        }
    }
}