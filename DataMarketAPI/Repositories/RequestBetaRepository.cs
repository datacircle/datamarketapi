using DataMarketAPI.Database;
using DataMarketAPI.Models;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Repositories
{
    public class RequestBetaRepository : IRequestBetaRepository
    {
        private readonly DatabaseContext _context;

        public RequestBetaRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<RequestBetaModel> IRequestBetaRepository.Create(RequestBetaModel requestBeta)
        {
            requestBeta.CreatedAt = requestBeta.UpdatedAt = DateTime.Now;
            return (await _context.RequestBeta.AddAsync(requestBeta)).Entity;
        }

        async Task<int> IRequestBetaRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}