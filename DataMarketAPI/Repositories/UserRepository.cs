using DataMarketAPI.Database;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace DataMarketAPI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DatabaseContext _context;

        public UserRepository(DatabaseContext context)
        {
            _context = context;
        }

        async Task<User> IUserRepository.FindByEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(user => user.Email == email);
        }

        async Task<User> IUserRepository.FindByVerificationToken(string verificationToken)
        {
            return await _context.Users.FirstOrDefaultAsync(user => user.VerificationKey == verificationToken);
        }

        async Task<CompanyModel> IUserRepository.GetCompany(User user)
        {
            CompanyModel company = await _context.Company.FirstOrDefaultAsync(c => c.Id == user.CompanyId);
            return company;
        }

        async Task<int> IUserRepository.Save()
        {
            return await _context.SaveChangesAsync();
        }

        async Task<User> IUserRepository.FindForShare(string Email, string PublicId)
        {
            return await _context.Users.Where(u => u.Email == Email || u.PublicId == PublicId).FirstOrDefaultAsync();
        }

        async Task IUserRepository.UpdateLastLogin(User user)
        {
            User u = await _context.Users.FirstOrDefaultAsync(x => x.Email == user.Email);
            
            if (u != null)
            {
                u.LastLogin = DateTime.Now;
                _context.Users.Update(u);
                await _context.SaveChangesAsync();
            }
        }

        async Task IUserRepository.UpdateUserDetails(User user)
        {
            User u = await _context.Users.FirstOrDefaultAsync(x => x.Email == user.Email);

            if (u != null)
            {
                u.Email = user.Email;
                u.PayPalLink = user.PayPalLink;
                u.FirstName = user.FirstName;
                u.LastName = user.LastName;
                _context.Users.Update(u);
                await _context.SaveChangesAsync();            
            }
        }
    }
}