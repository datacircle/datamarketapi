using DataMarketAPI.Application;
using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using DataMarketAPI.ViewModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http;

namespace DataMarketAPI.Services
{
    public class AccountService
    {
        private readonly ICompanyRepository _companyRepository;

        // private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly SecurityService _securityService;
        private readonly IEmailSender _emailSender;
        private readonly TokenService _tokenService;

        public AccountService(
            ICompanyRepository companyRepository,
            // IRoleRepository roleRepository,
            IUserRepository userRepository,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            SecurityService securityService,
            IEmailSender emailSender,
            TokenService tokenService)
        {
            _companyRepository = companyRepository;
            //_roleRepository = roleRepository;
            _userManager = userManager;
            _userRepository = userRepository;
            _signInManager = signInManager;
            _securityService = securityService;
            _emailSender = emailSender;
            _tokenService = tokenService;
        }

        private async Task<bool> EmailIsBurnerAsync(string email)
        {
            using (var client = new HttpClient())
            {
                var responseString = await client.GetStringAsync("https://burnerchecker.azurewebsites.net/api/isburneremail/" + email.ToString());
                return responseString == "true";                
            }
        }

        public async Task Register(RegisterViewModel registerVm, bool SignIn = true)
        {
            if (await EmailIsBurnerAsync(registerVm.Email))
            {
                throw new ResponseException("Please use a valid email address.");
            }

            CompanyModel newCompanyEntity = null;
            if (registerVm.Company != null && registerVm.Company != String.Empty)
            {
                CompanyModel _company = new CompanyModel() { NormalizedName = registerVm.Company.ToLower() };
                _company = await _companyRepository.GetByNormalizedName(_company);

                if (_company != null)
                {
                    throw new ResponseException("A company already exists.");
                }

                newCompanyEntity = await _companyRepository.Create(new CompanyModel { Name = registerVm.Company });

                await _companyRepository.Save();
            }

            // Role newRole = _roleRepository.getDefaultNewRole();
            // newRole.Company = newCompanyEntity;
            // Role newRoleEntity = _roleRepository.Create(newRole);

            User newUser = GetDefaultNewUser(
                registerVm,
                newCompanyEntity
            //, newRoleEntity
            );

            IdentityResult result = await _userManager.CreateAsync(newUser, registerVm.Password);
            if (result.Succeeded)
            {
                await _userManager.AddClaimsAsync(newUser,
                    new List<Claim>
                    {
                        new Claim(CustomClaimTypes.CompanyId, newUser.CompanyId.ToString()),
                        new Claim(CustomClaimTypes.UserId, newUser.Id.ToString())
                    });

                User newUserEntity = await _userManager.FindByEmailAsync(newUser.Email);
                await _userManager.GenerateEmailConfirmationTokenAsync(newUserEntity);
                AccountRegistrationTemplate emailTemplate = new AccountRegistrationTemplate(registerVm);
                await _emailSender.SendEmailAsync(emailTemplate);

                if (SignIn)
                {
                    var singInResult = await _signInManager.PasswordSignInAsync(registerVm.Email, registerVm.Password, false, lockoutOnFailure: true);

                    if (!singInResult.Succeeded)
                    {
                        throw new ResponseException("Could not login new user.");
                    }
                }
            }
            else
            {
                string message = "";
                foreach (IdentityError error in result.Errors)
                {
                    message += error.Description.Replace("User name", "Email") + Environment.NewLine;
                }

                throw new ResponseException(message);
            }
        }

        public async Task ForgotPassowrd(string email)
        {
            User user = await _userManager.FindByEmailAsync(email);

            if (user != null)
            {
                string hash = await _userManager.GeneratePasswordResetTokenAsync(user);

                // byte[] tokenArray = Microsoft.AspNetCore.WebUtilities.WebEncoders.Base64UrlEncode(Convert.ToBase64CharArray(hash));
                hash = System.Net.WebUtility.UrlEncode(hash);

                ForgotPasswordViewModel forgotPasswordVM = new ForgotPasswordViewModel();
                forgotPasswordVM.Email = user.Email;
                forgotPasswordVM.FirstName = user.FirstName;
                forgotPasswordVM.LastName = user.LastName;
                forgotPasswordVM.Token = hash;

                // send email
                ForgotPasswordTemplate emailTemplate = new ForgotPasswordTemplate(forgotPasswordVM);

                await _emailSender.SendEmailAsync(emailTemplate);
            }
        }

        public async Task<bool> ResetPassword(NewPasswordViewModel vm)
        {
            User user = await _userManager.FindByEmailAsync(vm.Email);
            // vm.Token = System.Net.WebUtility.UrlDecode(vm.Token);

            if (user != null)
            {
                PasswordValidator<User> validator = new PasswordValidator<User>();
                var passwordResult = await validator.ValidateAsync(_userManager, null, vm.Password);
                var a = passwordResult.ToString();

                var result = await _userManager.ResetPasswordAsync(user, vm.Token, vm.Password);
                if (result.Succeeded)
                {
                    await _userRepository.Save();
                    return true;
                }
                return false;
            }

            return false;
        }

        public async Task<bool> ConfirmEmail(string verificationKey)
        {
            User _user = await _userRepository.FindByVerificationToken(verificationKey);

            if (_user != null)
            {
                _user.IsVerified = true;
                return Convert.ToBoolean(_userRepository.Save());
            }

            return false;
        }

        private User GetDefaultNewUser(RegisterViewModel vm, CompanyModel company)
        {
            DateTime now = DateTime.Now;

            User newUser = new User
            {
                Email = vm.Email,
                UserName = vm.Email,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                IsCompanyOwner = true,
                IsActive = false,
                VerificationKey = null,
                Token = null,
                IsVerified = false,
                CreatedAt = now,
                UpdatedAt = now,
                Channel = ""
            };

            if (company != null)
            {
                newUser.CompanyId = company.Id;
            }
            // newUser.Token = _tokenService.GenerateLongLivedToken(newUser);
            // newUser.Role = role;

            return newUser;
        }

        public async Task<CompanyModel> PopulateCompany(User user)
        {
            return await _userRepository.GetCompany(user);
        }

        public async Task UpdateLastLogin(User user)
        {
            await _userRepository.UpdateLastLogin(user);
        }

        public async Task UpdateProfile(User user)
        {
            await _userRepository.UpdateUserDetails(user);
        }
    }
}