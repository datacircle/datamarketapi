using DataMarketAPI.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using ProtoBuf;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class CacheService
    {
        private readonly ILogger _logger;
        private readonly IDistributedCache _distributedCache;
        private DistributedCacheEntryOptions _distributedCacheOptions;

        public CacheService(ILogger<CacheService> logger, IDistributedCache distributedCache)
        {
            _logger = logger;
            _distributedCache = distributedCache;
            _distributedCacheOptions = new DistributedCacheEntryOptions();
            _distributedCacheOptions.AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(100);
        }

        public async Task<bool> DeleteKeyValue(Guid key)
        {
            try
            {
                _logger.LogInformation("Deleting Key", key.ToString());
                await _distributedCache.RemoveAsync(key.ToString());
            }
            catch (Exception e)
            {
                _logger.LogError("Error Deleting key: ", e);
                return false;
            }

            return true;
        }

        public async Task<bool> Write(DataStoreCollectionModel dataStoreCollection)
        {
            try
            {
                byte[] bytes = Serialize(dataStoreCollection);
                _logger.LogInformation("Writing to cache", dataStoreCollection);
                await _distributedCache.SetAsync(dataStoreCollection.ID.ToString(), bytes, _distributedCacheOptions);
            }
            catch (Exception e)
            {
                _logger.LogError("Error accessing cache: ", e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Access the cache to read.
        /// If you ever see a spike in SQL reads and drop in Cache Usage thats coz Cache is down duh.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<DataStoreCollectionModel> Read(string key)
        {
            byte[] bytes;
            try
            {
                _logger.LogInformation("Reading from cache KEY: " + key);
                bytes = await _distributedCache.GetAsync(key);
            }
            catch (Exception e)
            {
                _logger.LogError("Error accessing cache: ", e);
                return null;
            }

            if (bytes == null)
            {
                return null;
            }

            return Deserialize(bytes);
        }

        public byte[] Serialize(DataStoreCollectionModel dataStoreItem)
        {
            byte[] bytes;

            using (MemoryStream stream = new MemoryStream())
            {
                Serializer.Serialize(stream, dataStoreItem);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        protected DataStoreCollectionModel Deserialize(byte[] bytes)
        {
            DataStoreCollectionModel _dsc = new DataStoreCollectionModel();

            using (var stream = new MemoryStream(bytes))
            {
                _dsc = Serializer.Deserialize<DataStoreCollectionModel>(stream);
            }

            return _dsc;
        }
    }
}