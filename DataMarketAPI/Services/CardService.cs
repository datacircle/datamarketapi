﻿using DataMarketAPI.Models;
using DataMarketAPI.Repositories;

namespace DataMarketAPI.Services
{
    public class CardService
    {
        private readonly ICardRepository _cardRepository;

        public CardService(ICardRepository cardRepository)
        {
            _cardRepository = cardRepository;
        }

        public int GetConsumerCount(User model)
        {
            return _cardRepository.GetConsumerCount(model);
        }

        public int GetDatasourceCount(User model)
        {
            return _cardRepository.GetDatasourceCount(model);
        }

        public int GetMetricCount(User model)
        {
            return _cardRepository.GetMetricCount(model);
        }

        public int GetRequestCount(User model)
        {
            return _cardRepository.GetRequestCount(model);
        }
    }
}