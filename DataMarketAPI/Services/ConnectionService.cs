using DataMarketAPI.Application;
using DataMarketAPI.Library.Connection;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class ConnectionService
    {
        private readonly IDatasourceRepository _datasourceRepository;

        public ConnectionService(IDatasourceRepository datasourceRepository)
        {
            _datasourceRepository = datasourceRepository;
        }

        public async Task<bool> CheckConnection(DatasourceModel datasource)
        {
            return await GetConnector(datasource.Type).CheckConnection(datasource);
        }

        public Connection Get(Type datasourceType)
        {
            return GetConnector(datasourceType);
        }

        private Connection GetConnector(Type datasourceConnectionType)
        {
            switch (datasourceConnectionType)
            {
                case Models.Type.Mysql:
                    return new MySqlDbConnection();
                case Models.Type.Mssql:
                    return new MSSqlDbConnection();
                default:
                    throw new HandledException("Datasource type not recognized.");
            }
        }
    }
}