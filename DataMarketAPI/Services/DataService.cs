using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class DataService
    {
        private readonly IMetricRepository _metricRepository;
        private readonly EngineService _engine;
        private readonly CacheService _cacheService;
        private readonly QueryLogService _queryLogService;
        private readonly RequestService _requestService;
        private readonly IInsertLogRepository _insertLogRepository;
        private readonly DatasourceService _datasourceService;

        private readonly LocalStorageService _localStorageService;
        private QueryLogModel _queryLog;

        public DataService(
            IMetricRepository metricRepository,
            EngineService engine,
            CacheService cacheService,
            RequestService requestService,
            IInsertLogRepository insertLogRepository,
            QueryLogService queryLogService,
            LocalStorageService localStorageService,
            DatasourceService datasourceService)
        {
            _engine = engine;
            _metricRepository = metricRepository;
            _cacheService = cacheService;
            _requestService = requestService;
            _insertLogRepository = insertLogRepository;
            _queryLogService = queryLogService;
            _localStorageService = localStorageService;
            _datasourceService = datasourceService;
        }

        public async Task<DataStoreCollectionModel> GetDataStoreCollectionItemFromMetric(Guid metricId, bool useCache = false)
        {
            DataStoreCollectionModel dsc = new DataStoreCollectionModel();

            MetricModel _metric = await LoadMetric(metricId);

            dsc.FileName = _metric.Title;

            _queryLog = new QueryLogModel(_metric);

            //use cache copy, early exit
            if (useCache)
            {
                dsc = await ReadCache(metricId);

                if (dsc != null)
                {
                    _queryLog.Cached = true;
                    _queryLog.IsSuccesful = true;
                }
            }
            else
            {
                if (_metric.Datasource.Type == Models.Type.FileUploadGeneric)
                {
                    dsc.FileFullPath = _localStorageService.GetFilePath(_metric);
                }
                else
                {
                    dsc = await QueryDatasource(_metric);
                    dsc.FileName = _metric.Title;
                    _metric.Structure = JsonConvert.SerializeObject(dsc.Structure);
                    _metric.LastPool = DateTime.Now;
                    _metric.Processing = false;
                    await _metricRepository.Update(_metric);
                    await _metricRepository.Save();
                }

                _queryLog.IsSuccesful = (dsc != null);
                dsc.ID = metricId;
                await _cacheService.Write(dsc);
            }

            await _queryLogService.Write(_queryLog);

            return dsc;
        }

        private async Task<MetricModel> LoadMetric(Guid metricId)
        {
            MetricModel _metric = await _metricRepository.Get(metricId);

            if (_metric == null)
            {
                throw new Application.HandledException("Metric is null.");
            }

            _metric = await _metricRepository.EagerLoad(_metric);

            if (_metric.Datasource == null)
            {
                throw new Application.HandledException("Datasource for Metric is null");
            }

            _metric.Datasource = _datasourceService.UnProtect(_metric.Datasource);

            return _metric;
        }

        public async Task<DataStoreCollectionModel> Gather(MetricModel metric, bool useCache = true)
        {
            metric.Processing = true;
            await _metricRepository.UpdateProcessingStatus(metric);

            DataStoreCollectionModel _dsc = null;

            if (metric == null)
            {
                throw new Application.HandledException("Metric is null.");
            }

            _queryLog = new QueryLogModel(metric);

            if (useCache)
            {
                _dsc = await ReadCache(metric.ID);
            }

            if (_dsc != null)
            {
                _queryLog.Cached = true;
                await _queryLogService.Write(_queryLog);
                return _dsc;
            }

            // if not populate through the engine
            _dsc = await QueryDatasource(metric);
            metric.Structure = JsonConvert.SerializeObject(_dsc.Structure);
            metric.LastPool = DateTime.Now;
            metric.Processing = false;
            await _metricRepository.Update(metric);
            await _metricRepository.Save();

            if (_dsc != null)
            {
                _queryLog.IsSuccesful = true;
                _dsc.ID = metric.ID;
                await _cacheService.Write(_dsc);
            }

            // return result
            await _queryLogService.Write(_queryLog);
            return _dsc;
        }

        public async Task<bool> Populate(MetricTransferModel metricTransfer)
        {
            DataStoreCollectionModel dsc = await Gather(metricTransfer.Metric);
            string tableName = await _engine.InitTable(metricTransfer.TargetDatasource, metricTransfer.Metric.Title, dsc.Structure, metricTransfer.TargetTableName);

            InsertLogModel _insertLog = new InsertLogModel();
            _insertLog.Rows = dsc.Data.Count;
            _insertLog.MetricTransferID = metricTransfer.Id;

            try
            {
                _insertLog.InsertTime = await _engine.Insert(dsc, metricTransfer.Metric.Datasource, tableName);
                _insertLog.IsSuccesful = true;
            }
            catch (Exception e)
            {
                _insertLog.InsertTime = 0;
                _insertLog.IsSuccesful = false;
                _insertLog.ErrorMessage = e.Message;
            }

            await _insertLogRepository.Write(_insertLog);

            return true;
        }

        protected async Task<DataStoreCollectionModel> ReadCache(Guid metricId)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            DataStoreCollectionModel _dsc = await _cacheService.Read(metricId.ToString());
            watch.Stop();
            _queryLog.QueryTime = watch.ElapsedMilliseconds;

            return _dsc;
        }

        protected async Task<DataStoreCollectionModel> QueryDatasource(MetricModel metric)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            try
            {
                DataStoreCollectionModel _dsc = await _engine.Pool(metric.Datasource, metric);
                watch.Stop();
                _queryLog.QueryTime = watch.ElapsedMilliseconds;

                return _dsc;
            }
            catch (HandledException e)
            {
                watch.Stop();
                _queryLog.QueryTime = watch.ElapsedMilliseconds;
                _queryLog.IsSuccesful = false;
                _queryLog.ErrorMessage = e.Message;
                throw e;
            }
            catch (Exception e)
            {
                watch.Stop();
                _queryLog.QueryTime = watch.ElapsedMilliseconds;
                _queryLog.IsSuccesful = false;
                _queryLog.ErrorMessage = "Error, please contact support.";
                throw e;
            }
        }
    }
}