using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class DatasourceService
    {
        private readonly IDatasourceRepository _datasourceRepository;
        private readonly ConnectionService _connectionService;
        private readonly SecurityService _securityService;

        public DatasourceService(
            IDatasourceRepository datasourceRepository,
            SecurityService securityService,
            ConnectionService connectionService
            )
        {
            _datasourceRepository = datasourceRepository;
            _securityService = securityService;
            _connectionService = connectionService;
        }

        public async Task<bool> IsAttachedToMetric(DatasourceModel datasource)
        {
            return await _datasourceRepository.IsAttachedToMetric(datasource);
        }

        public async Task<bool> CheckAnonymConnection(DatasourceModel datasource)
        {
            return await _connectionService.CheckConnection(datasource);
        }

        public async Task<bool> CheckConnection(DatasourceModel datasource)
        {
            DatasourceModel _datasource = await Get(datasource);

            bool value = await _connectionService.CheckConnection(_datasource);
            _datasource.ConnectionStatus = value;
            await _datasourceRepository.Update(_datasource);
            await _datasourceRepository.Save();
            return value;
        }

        public async Task<DatasourceModel> Create(DatasourceModel datasource)
        {
            datasource.Host = _securityService.Protect(datasource.Host);
            datasource.Password = _securityService.Protect(datasource.Password);
            datasource.Username = _securityService.Protect(datasource.Username);
            datasource.Database = _securityService.Protect(datasource.Database);

            datasource = await _datasourceRepository.Create(datasource);
            await _datasourceRepository.Save();
            return datasource;
        }

        public async Task<DatasourceModel> Get(DatasourceModel datasource)
        {
            DatasourceModel datasourceEntity = await _datasourceRepository.Get(datasource);

            datasourceEntity = UnProtect(datasourceEntity);

            return datasourceEntity;
        }

        public async Task<DatasourceModel> GetUncencorsed(DatasourceModel datasource)
        {
            DatasourceModel datasourceEntity = await _datasourceRepository.GetUncencorsed(datasource);

            datasourceEntity = UnProtect(datasourceEntity);

            return datasourceEntity;
        }

        public DatasourceModel UnProtect(DatasourceModel datasource)
        {
            // I F*cked up during Seed and didnt protect, with netcore2.0 you can't unprotect something you didnt protect. fuck. Exclude all System Datasources.
            if (datasource != null && datasource.CompanyId != 1)
            {
                datasource.Host = _securityService.UnProtect(datasource.Host);
                datasource.Password = _securityService.UnProtect(datasource.Password);
                datasource.Username = _securityService.UnProtect(datasource.Username);
                datasource.Database = _securityService.UnProtect(datasource.Database);
            }

            return datasource;
        }

        public async Task<bool> Exists(DatasourceModel datasource)
        {
            DatasourceModel datasourceEntity = await _datasourceRepository.Get(datasource);
            if (datasourceEntity != null)
            {
                return true;
            }

            return false;
        }

        public async Task<IEnumerable<DatasourceModel>> GetAll(DatasourceModel model)
        {
            return await _datasourceRepository.GetAll(model);
        }

        public async Task<DatasourceModel> Update(DatasourceModel datasource)
        {
            datasource.Host = _securityService.Protect(datasource.Host);
            datasource.Password = _securityService.Protect(datasource.Password);
            datasource.Username = _securityService.Protect(datasource.Username);
            datasource.Database = _securityService.Protect(datasource.Database);

            datasource = await _datasourceRepository.Update(datasource);

            await _datasourceRepository.Save();
            return datasource;
        }

        public async Task Delete(DatasourceModel datasource)
        {
            _datasourceRepository.Delete(datasource);
            await _datasourceRepository.Save();
        }
    }
}