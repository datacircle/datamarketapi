using DataMarketAPI.Library.Connection;
using DataMarketAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class EngineService
    {
        private readonly ConnectionService _connectionService;

        public EngineService(ConnectionService connectionService)
        {
            _connectionService = connectionService;
        }

        public async Task<DataStoreCollectionModel> Pool(DatasourceModel datasource, MetricModel metric)
        {
            DataStoreCollectionModel _dsc = new DataStoreCollectionModel();
            _dsc.Data = new List<List<string>>();

            Connection _con = _connectionService.Get(datasource.Type);

            DbDataReader reader = await _con.ExecuteCmd(datasource, metric.Query);

            try
            {
                bool readStructure = true;
                while (await reader.ReadAsync())
                {
                    string dataValue = string.Empty;
                    var row = new List<string>();

                    for (int col = 0; col < reader.FieldCount; col++)
                    {
                        string datatype = reader.GetDataTypeName(col);
                        if (readStructure)
                        {
                            _dsc.Structure.Add(reader.GetName(col), reader.GetFieldType(col).Name);                            
                        }

                        if (datatype == "TIMESTAMP")
                        {
                            DateTime dt = DateTime.Parse(reader[col].ToString());
                            dataValue = dt.ToString("yyyy-MM-dd HH:MM:ss");
                        }
                        else if (datatype == "BOOL")
                        {
                            var a = reader[col].ToString();
                            dataValue = reader[col].ToString() == "True" ? "1" : "0";
                        }
                        else
                        {
                            dataValue = reader[col].ToString();
                        }
                        
                        row.Add(dataValue.Replace("Sql", ""));
                    }

                    _dsc.Data.Add(row);
                    row = new List<string>();
                    readStructure = false;
                }
            }
            catch (Exception e)
            {
            }

            _con.Close();

            return _dsc;
        }

        public async Task<string> InitTable(
            DatasourceModel datasource,
            string metricName,
            Dictionary<string, string> structure,
            string tableName = null)
        {
            if (tableName == String.Empty || tableName == null)
            {
                tableName = "tbl_" + metricName.Replace(' ', '_');
            }

            Connection _con = _connectionService.Get(datasource.Type);

            await _con.ExecuteCmd(datasource, _con.GenerateTable(tableName, structure));

            return tableName;
        }

        public async Task<long> Insert(DataStoreCollectionModel dsc, DatasourceModel datasource, string TableName)
        {
            Connection _con = _connectionService.Get(datasource.Type);

            return await _con.InsertPrepare(datasource, _con.GenerateInsert(dsc, TableName), dsc);
        }
    }
}