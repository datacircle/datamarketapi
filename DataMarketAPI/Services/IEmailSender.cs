using DataMarketAPI.EmailTemplates;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(IMailTemplate mail);
    }
}