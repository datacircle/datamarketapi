using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}