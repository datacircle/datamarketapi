using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    /// <summary>
    /// This class is used by the application to send Email and SMS
    /// when you turn on two-factor authentication in ASP.NET Identity.
    /// For more details see this link https://go.microsoft.com/fwlink/?LinkID=532713
    /// </summary>
    public class MessageService : IEmailSender
    {
        private string _apiKey;
        private Uri _uri;
        private string _requestUri;
        private readonly EmailSettingsModel _emailSettings;

        /// <summary>
        ///
        /// </summary>
        /// <param name="emailSettings"></param>
        public MessageService(IOptions<EmailSettingsModel> emailSettings)
        {
            _emailSettings = emailSettings.Value;
            _apiKey = Convert.ToBase64String(Encoding.ASCII.GetBytes(_emailSettings.ApiKey));
            _uri = new Uri(_emailSettings.BaseUri);
            _requestUri = _emailSettings.RequestUri;
        }

        async Task IEmailSender.SendEmailAsync(IMailTemplate mail)
        {
            var client = new HttpClient { BaseAddress = _uri };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _apiKey);

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("from", _emailSettings.From),
                new KeyValuePair<string, string>("to", mail.Recipient),
                new KeyValuePair<string, string>("subject", mail.Subject),
                new KeyValuePair<string, string>("text", mail.ContentText),
                new KeyValuePair<string, string>("html", mail.ContentHtml)
            });

            // ByteArrayContent fileContent = new ByteArrayContent(File.ReadAllBytes(filePath));

            // fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            //                 {
            //                     Name = "attachment",
            //                     FileName = "MyAttachment.pdf"
            //                 };

            // content.Add(fileContent);

            var result = await client.PostAsync(_requestUri, content).ConfigureAwait(false);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="number"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}