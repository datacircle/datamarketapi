﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class MetricAccessService
    {
        private readonly IMetricAccessRepository _metricAccessRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMetricRepository _metricRepository;

        public MetricAccessService(IMetricAccessRepository metricAccessRepository, IUserRepository userRepository, IMetricRepository metricRepository)
        {
            _metricAccessRepository = metricAccessRepository;
            _userRepository = userRepository;
            _metricRepository = metricRepository;
        }

        public async Task<MetricAccessMapModel> Create(MetricAccessMapModel model)
        {
            MetricModel _metric = await _metricRepository.Get(model.MetricId);

            if (_metric.Owner != model.ActionableUserId)
            {
                throw new HandledException("User is not allowed to edit Metric.");
            }

            model = await _metricAccessRepository.Create(model);
            model.Metric = null;
            model.TargetUser.OmitSentsitiveData();

            return model;
        }

        public bool Delete(MetricAccessMapModel model)
        {
            return _metricAccessRepository.Delete(model);
        }

        public async Task<User> FindUser(string Email, string TargetPublicId)
        {
            return await _userRepository.FindForShare(Email, TargetPublicId);
        }

        public async Task<MetricAccessMapModel> Get(MetricAccessMapModel model)
        {
            return await _metricAccessRepository.GetAsync(model);
        }

        public async Task<IEnumerable<MetricAccessMapModel>> GetAll(MetricAccessMapModel model)
        {
            var a = await _metricAccessRepository.GetAllAsync(model);
            a.ToList().ForEach(e => e.TargetUser.OmitSentsitiveData());

            return a;
        }
    }
}