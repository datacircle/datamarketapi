using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class MetricCommentService
    {
        private readonly IMetricCommentRepository _metricCommentRepository;
        private readonly IMetricRepository _metricRepository;

        public MetricCommentService(IMetricCommentRepository metricCommentRepository, IMetricRepository metricRepository)
        {
            _metricCommentRepository = metricCommentRepository;
            _metricRepository = metricRepository;
        }

        public async Task<MetricCommentModel> Create(MetricCommentModel model)
        {
            MetricCommentModel _rc = await _metricCommentRepository.Create(model);
            await _metricCommentRepository.Save();

            await _metricRepository.UpdateCommentCount(model.MetricId);
            await _metricRepository.Save();
            return _rc;
        }

        public async Task<IEnumerable<MetricCommentModel>> GetByRequest(MetricCommentModel model)
        {
            return await _metricCommentRepository.GetByRequest(model);
        }

        public async Task<bool> Delete(MetricCommentModel metricModel)
        {
            return await _metricCommentRepository.Delete(metricModel);
        }
    }
}