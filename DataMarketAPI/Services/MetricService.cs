using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class MetricService
    {
        private readonly IMetricRepository _metricRepository;
        private readonly IDatasourceRepository _datasourceRepository;
        private readonly DatasourceService _datasourceService;
        private readonly CacheService _cacheService;
        private readonly UploadService _uploadService;
        private readonly DataService _dataService;
        private readonly IHashTagRepository _hashTagRepository;

        public MetricService(
            IMetricRepository metricRepository,
            IDatasourceRepository datasourceRepository,
            DatasourceService datasourceService,
            CacheService cacheService,
            UploadService uploadService,
            DataService dataService,
            IHashTagRepository hashTagRepository
            )
        {
            _metricRepository = metricRepository;
            _datasourceRepository = datasourceRepository;
            _datasourceService = datasourceService;
            _cacheService = cacheService;
            _uploadService = uploadService;
            _dataService = dataService;
            _hashTagRepository = hashTagRepository;
        }

        public async Task<bool> HasCost(Guid metricId)
        {
            return await _metricRepository.HasCostAsync(metricId);
        }

        public async Task<MetricModel> Create(MetricModel metric)
        {
            metric = await _metricRepository.Create(metric);
            await _hashTagRepository.Upsert(metric.HashTags);
            await _hashTagRepository.MetricBinding(metric.ID, metric.HashTags);

            await _metricRepository.Save();

            metric = await _metricRepository.EagerLoad(metric);

            if (!string.IsNullOrEmpty(metric.FileName) && !string.IsNullOrEmpty(metric.FilePath))
            {
                Console.WriteLine("uploading file.");
                await UploadFile(metric);
            }
            else if (string.IsNullOrEmpty(metric.Url))
            {
                metric.Datasource = _datasourceService.UnProtect(metric.Datasource);
                BackgroundJob.Enqueue(() => _dataService.Gather(metric, false));
            }

            return metric;
        }

        public async Task<MetricModel> Update(MetricModel metric)
        {
            metric = await _metricRepository.Update(metric);
            await _metricRepository.Save();
            await _hashTagRepository.Upsert(metric.HashTags);
            await _hashTagRepository.MetricBinding(metric.ID, metric.HashTags);

            return metric;
        }

        public async Task<MetricModel> UpdateMetricBusiness(MetricModel newMetric)
        {
            MetricModel oldMetric = await _metricRepository.Get(newMetric);

            newMetric.CreatedAt = oldMetric.CreatedAt;

            if (oldMetric.Query != newMetric.Query || oldMetric.DatasourceId != newMetric.DatasourceId)
            {
                await _cacheService.DeleteKeyValue(newMetric.ID);
                newMetric.Structure = String.Empty;
            }

            newMetric = await _metricRepository.EagerLoad(newMetric);

            newMetric = PreserveFileData(oldMetric, newMetric);

            CleanUpDatasourceChange(newMetric);

            await SubProcesses(oldMetric, newMetric);

            await Update(newMetric);

            return newMetric;
        }

        private async Task SubProcesses(MetricModel oldMetric, MetricModel newMetric)
        {
            if (newMetric.Datasource.Type == Models.Type.FileUploadGeneric)
            {
                if (newMetric.FileName != oldMetric.FileName)
                {
                    await UploadFile(newMetric);
                }
            }

            if (newMetric.Datasource.Type == Models.Type.Mysql || newMetric.Datasource.Type == Models.Type.Mssql)
            {
                if (newMetric.Query != oldMetric.Query)
                {
                    //newMetric.Datasource = _datasourceService.UnProtect(newMetric.Datasource);
                    BackgroundJob.Enqueue(() => _dataService.Gather(newMetric, false));
                }
            }
        }

        // Must have run Enrich/Eagerload FIRST!
        private MetricModel PreserveFileData(MetricModel oldMetric, MetricModel newMetric)
        {
            if (newMetric.Datasource.Type == Models.Type.FileUploadGeneric && newMetric.FileName == null && newMetric.FilePath == null)
            {
                newMetric.FilePath = oldMetric.FilePath;
                newMetric.FileName = oldMetric.FileName;
            }

            return newMetric;
        }

        // Must have run Enrich/Eagerload FIRST!
        private void CleanUpDatasourceChange(MetricModel metric)
        {
            if (metric.Datasource.Type == Models.Type.FileUploadGeneric)
            {
                metric.Query = String.Empty;
            }

            if (metric.Datasource.Type == Models.Type.Mysql || metric.Datasource.Type == Models.Type.Mssql)
            {
                metric.FileName = String.Empty;
                metric.FilePath = String.Empty;
            }
        }

        private async Task UploadFile(MetricModel metric)
        {
            FileShareModel _fileModel = new FileShareModel
            {
                UserId = metric.Owner,
                FileName = metric.FileName,
                FilePath = metric.FilePath,
                MetricId = metric.ID
            };

            metric.Processing = true;

            await Update(metric);

            _uploadService.UploadFile(_fileModel);

            metric.FilePath = _uploadService.GetFilePath(_fileModel);
            metric.Processing = false;
            await Update(metric);
        }

        public async Task<MetricModel> Get(MetricModel metric)
        {
            MetricModel _metric = await _metricRepository.Get(metric);

            if (_metric == null)
            {
                return null;
            }

            if (_metric != null && _metric.Owner != metric.Owner)
            {
                _metric.OmitSensitiveData();
                _metric.User.OmitSentsitiveData();
            }

            if (_metric.Approved == false && _metric.Owner != metric.Owner)
            {
                return null;
            }

            if (_metric.Open == false && _metric.Owner != metric.Owner)
            {
                return null;
            }

            return _metric;
        }

        public async Task<MetricModel> GetWithDatasource(MetricModel metric)
        {
            return await _metricRepository.GetWithDatasource(metric);
        }

        public async Task<IEnumerable<MetricModel>> GetAllForCompany(MetricModel metric)
        {
            return await _metricRepository.GetAllForCompany(metric);
        }

        public async Task<IEnumerable<MetricModel>> GetAllForMarketplace()
        {
            return await _metricRepository.GetAllForMarketplace();
        }

        public async Task<IEnumerable<MetricModel>> GetAllForRequestCreation(MetricModel metric)
        {
            return await _metricRepository.GetAllForRequestCreation(metric);
        }

        public async Task<IEnumerable<MetricModel>> GetAllForSearch(MetricModel metric)
        {
            return await _metricRepository.GetAllForSearch(metric);
        }

        public async Task<IEnumerable<MetricModel>> GetMetricThatNeedUpdate()
        {
            IEnumerable<MetricModel> _metricCollection = await _metricRepository.GetMetricThatNeedUpdate();
            foreach (MetricModel metric in _metricCollection)
            {
                metric.Datasource = _datasourceService.UnProtect(metric.Datasource);
            }
            return _metricCollection;
        }

        public async Task<IEnumerable<MetricModel>> Search(string searchTerm = "", string hashtags = "")
        {
            if (searchTerm == null )
            {
                searchTerm = "";
            }

            if (hashtags == null)
            {
                hashtags = "";
            }

            if (searchTerm.StartsWith("!c-"))
            {
                string category = searchTerm.Replace("!c-", "");
                return await _metricRepository.SearchByCategory(category);
            }

            switch (searchTerm.ToLower())
            {
                case "!top50":
                    return await _metricRepository.SearchTop50();

                case "!trending50":
                    return await _metricRepository.SearchTrending50();

                case "!new50":
                    return await _metricRepository.SearchNew50();

                case "!paid":
                    return await _metricRepository.SearchPaid();
                default:

                    string[] arrayHashTags = { };
                    if (!string.IsNullOrEmpty(hashtags))
                    {
                        arrayHashTags = hashtags.Split(',');
                    } 
                    IEnumerable<MetricModel> _metricCollection = await _metricRepository.Search(searchTerm, arrayHashTags);
                    _metricCollection = _metricCollection.OrderByDescending(m => m.Rating);
                    return _metricCollection;
            }
        }

        public async Task Delete(MetricModel metric)
        {
            await _metricRepository.Delete(metric);
            await _metricRepository.Save();
        }
    }
}