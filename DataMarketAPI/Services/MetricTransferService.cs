﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using Hangfire;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class MetricTransferService
    {
        private readonly DatasourceService _datasourceService;
        private readonly IMetricTransferRepository _metricTransferRepository;
        private readonly DataService _dataService;
        private readonly ILogger _logger;

        public MetricTransferService(ILogger<MetricTransferService> logger, IMetricTransferRepository metricTransferRepository, DataService dataService, DatasourceService datasourceService)
        {
            _metricTransferRepository = metricTransferRepository;
            _dataService = dataService;
            _datasourceService = datasourceService;
            _logger = logger;
        }

        public async Task<MetricTransferModel> Create(MetricTransferModel model)
        {
            model.Status = MetricTransferModel.StatusCode.Created;
            MetricTransferModel _metricEntity = await _metricTransferRepository.Create(model);
            await _metricTransferRepository.Save();

            return _metricEntity;
        }

        public async Task<MetricTransferModel> Update(MetricTransferModel model)
        {
            MetricTransferModel _metricEntity = await _metricTransferRepository.Update(model);
            await _metricTransferRepository.Save();

            return _metricEntity;
        }

        public async Task<IEnumerable<MetricTransferModel>> GetAll(MetricTransferModel model)
        {
            return await _metricTransferRepository.GetAll(model);
        }

        public async Task<IEnumerable<MetricTransferModel>> GetAllThatNeedProcessing()
        {
            return await _metricTransferRepository.GetAllThatNeedProcessing();
        }

        public async Task ExecutePopulate(MetricTransferModel metricTransfer)
        {
            await _dataService.Populate(metricTransfer);
            metricTransfer.Status = MetricTransferModel.StatusCode.Completed;
            await Update(metricTransfer);
        }

        public async Task<DataStoreCollectionModel> Transfer(MetricTransferModel metricTransfer)
        {
            metricTransfer.Status = MetricTransferModel.StatusCode.Started;

            _logger.LogInformation("Initiating Transfer for MetricTransfer: " + metricTransfer.Id);
            _logger.LogInformation("Hydrating MetricTransfer: " + metricTransfer.Id);
            metricTransfer = await _metricTransferRepository.Hydrate(metricTransfer);

            metricTransfer.Metric.Datasource = _datasourceService.UnProtect(metricTransfer.Metric.Datasource);
            metricTransfer.TargetDatasource = _datasourceService.UnProtect(metricTransfer.TargetDatasource);

            DataStoreCollectionModel _dsc = null;

            try
            {
                _logger.LogInformation("Call to DataService for MetricTransfer: " + metricTransfer.Id);
                if (!metricTransfer.DownloadAsFile)
                {
                    metricTransfer.Status = MetricTransferModel.StatusCode.Queued;
                    await Update(metricTransfer);

                    BackgroundJob.Enqueue(() => ExecutePopulate(metricTransfer));

                    return null;
                }
                else
                {
                    if (metricTransfer.Metric.Datasource.Type == Models.Type.FileUploadGeneric)
                    {
                        return new DataStoreCollectionModel
                        {
                            FileFullPath = metricTransfer.Metric.FilePath
                        };
                    }
                    else
                    {
                        _dsc = await _dataService.Gather(metricTransfer.Metric);
                        metricTransfer.Status = MetricTransferModel.StatusCode.Completed;
                        return _dsc;
                    }
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                metricTransfer.Status = MetricTransferModel.StatusCode.Error;
                metricTransfer.Message = "An error occured. Please make sure Query, Target Datasource or Target Table Name are correct.";

                _logger.LogError("Pomelo error in MetricTransfer: " + metricTransfer.Id.ToString(), e.Message);
                throw new HandledException("An error occured. Please make sure Query and Target Datasource are correct.");
            }
            catch (Exception e)
            {
                metricTransfer.Status = MetricTransferModel.StatusCode.Error;
                metricTransfer.Message = "Error";
                _logger.LogError("There was an error in MetricTransfer: " + metricTransfer.Id.ToString(), e.Message);
                throw e;
            }
            finally
            {
                await Update(metricTransfer);
            };
        }
    }
}