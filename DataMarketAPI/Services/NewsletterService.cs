using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class NewsletterService
    {
        private readonly INewsletterRepository _newsletterRepository;
        private readonly UserManager<User> _userManager;
        private readonly SecurityService _securityService;
        private readonly IEmailSender _emailSender;

        public NewsletterService(
            INewsletterRepository newsletterRepository,
            UserManager<User> userManager,
            SecurityService securityService,
            IEmailSender emailSender
            )
        {
            _newsletterRepository = newsletterRepository;
            _userManager = userManager;
            _securityService = securityService;
            _emailSender = emailSender;
        }

        public async Task<NewsletterModel> Get(string email)
        {
            return await _newsletterRepository.Get(email);
        }

        public async Task<int> Create(NewsletterModel newsletter)
        {
            NewsletterModel newsletterEntity = await _newsletterRepository.Get(newsletter.Email);
            if (newsletterEntity != null)
            {
                newsletterEntity.UpdatedAt = DateTime.Now;
                newsletterEntity.Subscribed = true;
                newsletterEntity.DoubleOptedIn = false;
            }
            else
            {
                newsletterEntity = await _newsletterRepository.Create(newsletter);
            }

            newsletterEntity.DoubleOptInToken = _securityService.HashKey(newsletter.Email);
            newsletterEntity.UnsubscriptionToken = _securityService.HashKey(newsletter.Email);

            DoubleOptInTemplate emailTemplate = new DoubleOptInTemplate(newsletterEntity);
            await _emailSender.SendEmailAsync(emailTemplate);

            return await _newsletterRepository.Save();
        }

        public async Task<int> Unsubscribe(string token)
        {
            if (await _newsletterRepository.Unsubscribe(token))
            {
                await _newsletterRepository.Save();
                return 1;
            }

            return 0;
        }

        public async Task<int> DoubleOptIn(string token)
        {
            Console.WriteLine(token);
            if (await _newsletterRepository.DoubleOptIn(token))
            {
                await _newsletterRepository.Save();
                return 1;
            }

            return 0;
        }

        public async Task<int> Delete(NewsletterModel newsletter)
        {
            await _newsletterRepository.Delete(newsletter);
            return await _newsletterRepository.Save();
        }
    }
}