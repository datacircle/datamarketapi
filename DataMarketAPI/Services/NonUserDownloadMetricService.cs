﻿using DataMarketAPI.Application;
using DataMarketAPI.Database;
using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class NonUserDownloadMetricService
    {
        private readonly DatabaseContext _context;
        private readonly DataService _dataService;
        private readonly IEmailSender _emailSender;

        public NonUserDownloadMetricService(DatabaseContext context, IEmailSender emailSender, DataService dataService)
        {
            _context = context;
            _emailSender = emailSender;
            _dataService = dataService;
        }

        public async Task Create(FreeDownloadModel model)
        {
            await _context.FreeDownload.AddAsync(model);

            await _context.Entry(model).Reference(m => m.Metric).LoadAsync();

            DownloadMetricTemplate emailTemplate = new DownloadMetricTemplate(model);
            await _emailSender.SendEmailAsync(emailTemplate);
            await _context.SaveChangesAsync();
        }

        public async Task JustAdd(FreeDownloadModel model)
        {
            await _context.FreeDownload.AddAsync(model);

            await _context.SaveChangesAsync();
        }

        public async Task<DataStoreCollectionModel> Download(Guid freeDownloadId)
        {
            FreeDownloadModel _fdm = await _context.FreeDownload.FirstOrDefaultAsync(m => m.ID == freeDownloadId);

            if (_fdm == null)
            {
                throw new HandledException("No such code found.");
            }

            return await _dataService.GetDataStoreCollectionItemFromMetric(_fdm.MetricId);
        }
    }
}