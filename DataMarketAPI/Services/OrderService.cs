﻿using DataMarketAPI.Database;
using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using DataMarketAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace DataMarketAPI.Services
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderService
    {
        private readonly IEmailSender _emailSender;
        private readonly DatabaseContext _context;

        public OrderService(
            DatabaseContext context,
            IEmailSender emailSender
            )
        {
            _context = context;
            _emailSender = emailSender;
        }

        public async Task<OrderModel> GetOrderDetails(Guid metricId, int userId)
        {
            return await _context.Order.Where(o => o.MetricId == metricId && o.Owner == userId).FirstOrDefaultAsync();
        }

        public async Task<List<OrderModel>> GetOrders(int userId)
        {
            return await _context.Order.Where(o => o.Owner == userId).ToListAsync();
        }

        public async Task<bool> OrderIsPaid(Guid metricId, int userId)
        {
            OrderModel order = await _context.Order.Where(o => o.MetricId == metricId && o.Owner == userId).FirstOrDefaultAsync();

            if (order == null)
            {
                return false;
            }

            if (order.Status != OrderStatus.Paid)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> Order(OrderViewModel vm)
        {
            MetricModel _metric = await _context.Metric.FirstOrDefaultAsync(m => m.ID == vm.MetricId);

            if (_metric == null)
            {
                return false;
            }

            OrderModel order = new OrderModel();        
            order.Cost = _metric.Cost;
            order.Owner = vm.UserId;
            order.MetricId = vm.MetricId;
            order.InvoiceId = (await CreateInvoice(vm)).Id;          

            await _context.Order.AddAsync(order);
            await _context.SaveChangesAsync();

            InvoiceTemplate emailTemplate = new InvoiceTemplate(vm);
            await _emailSender.SendEmailAsync(emailTemplate);

            return true;
        }
        
        private async Task<InvoiceModel> CreateInvoice(OrderViewModel vm)
        {
            InvoiceModel invoice = new InvoiceModel();
            invoice.FullName = vm.FullName;
            invoice.Address = vm.Address;

            var dt = DateTime.Now;            
            var dtInt = dt.Year + dt.Month*43800 + dt.Day*60*24 + dt.Hour*60 + dt.Minute;

            invoice.PublicId = "DE" + vm.UserId.ToString("000000") + dtInt.ToString();
            invoice.CountryId = 1;            

            await _context.Invoice.AddAsync(invoice);
            await _context.SaveChangesAsync();

            return invoice;
        }
    }    
}