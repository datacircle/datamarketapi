using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class QueryLogService
    {
        private readonly IQueryLogRepository _queryLogRepository;

        public QueryLogService(IQueryLogRepository queryLogRepository)
        {
            _queryLogRepository = queryLogRepository;
        }

        public async Task<bool> Write(QueryLogModel model)
        {
            return await _queryLogRepository.Write(model);
        }

        public async Task<QueryLogModel> Get(QueryLogModel model)
        {
            return await _queryLogRepository.Read(model.ID);
        }

        public async Task<IEnumerable<QueryLogModel>> GetByMetric(MetricModel metricModel)
        {
            return await _queryLogRepository.ReadByMetric(metricModel);
        }

        public async Task<IEnumerable<QueryLogModel>> GetByDatasource(DatasourceModel datasourceModel)
        {
            return await _queryLogRepository.ReadByDatasource(datasourceModel);
        }

        public async Task<IEnumerable<QueryLogModel>> GetByCompanyWithUserFallback(User user)
        {
            return await _queryLogRepository.GetByCompanyWithUserFallback(user);
        }
    }
}