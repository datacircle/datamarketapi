﻿using DataMarketAPI.Application;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataMarketAPI.Application.HandledException;

namespace DataMarketAPI.Services
{
    public class RatingService
    {
        private readonly IRatingRepository _ratingRepository;
        private readonly IMetricRepository _metricRepository;

        public RatingService(IRatingRepository ratingRepository, IMetricRepository metricRepository)
        {
            _ratingRepository = ratingRepository;
            _metricRepository = metricRepository;
        }

        public async Task<double> Add(RatingModel model, int? companyId)
        {
            if (await _metricRepository.UserHasViewAccess(model.MetricID, model.Owner, companyId))
            {
                RatingModel _rating = await _ratingRepository.GetByUserMetric(model);

                if (_rating != null)
                {
                    if (await _ratingRepository.Update(model))
                    {
                        return await UpdateRating(model);
                    }
                }
                else
                {
                    await _ratingRepository.Create(model);
                    await _ratingRepository.Save();
                    return await UpdateRating(model);
                }
            }

            throw new HandledException("User has no such access.", ThrowResponseLevel.Unathorized);
        }

        public async Task<double> Update(RatingModel model, int? companyId)
        {
            if (await _ratingRepository.Update(model))
            {
                return await UpdateRating(model);
            }

            throw new HandledException("User has no such access.", ThrowResponseLevel.BadRequest);
        }

        public async Task<double> Delete(RatingModel model)
        {
            _ratingRepository.Delete(model);
            return await UpdateRating(model);
        }

        private async Task<double> UpdateRating(RatingModel model)
        {
            double newAvg = await CalculateAvg(model);
            await _ratingRepository.UpdateRatingOfMetric(model, newAvg);

            await _ratingRepository.Save();

            return newAvg;
        }

        private async Task<double> CalculateAvg(RatingModel model)
        {
            IEnumerable<RatingModel> _ratingEntityCollection = await _ratingRepository.AllForMetric(model);

            double AvgValue = _ratingEntityCollection.Average(r => r.Value);

            double newValue = Math.Round(AvgValue * 2, MidpointRounding.AwayFromZero) / 2;

            return AvgValue;
        }
    }
}