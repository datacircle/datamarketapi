using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class RequestCommentService
    {
        private readonly IRequestCommentRepository _requestCommentRepository;
        private readonly IRequestRepository _requestRepository;

        public RequestCommentService(IRequestCommentRepository requestCommentRepository, IRequestRepository requestRepository)
        {
            _requestCommentRepository = requestCommentRepository;
            _requestRepository = requestRepository;
        }

        public async Task<RequestCommentModel> Create(RequestCommentModel model)
        {            
            RequestCommentModel _rc = await _requestCommentRepository.Create(model);
            await _requestCommentRepository.Save();

            await _requestRepository.UpdateCommentCount(model.RequestId);
            await _requestCommentRepository.Save();
            return _rc;
        }

        public async Task<IEnumerable<RequestCommentModel>> GetByRequest(RequestCommentModel model)
        {
            return await _requestCommentRepository.GetByRequest(model);
        }

        public async Task<bool> Delete(RequestCommentModel metricModel)
        {
            return await _requestCommentRepository.Delete(metricModel);
        }
    }
}