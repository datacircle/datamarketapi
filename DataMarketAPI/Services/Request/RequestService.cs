using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class RequestService
    {
        private readonly IRequestRepository _requestRepository;

        public RequestService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        public async Task<RequestModel> Create(RequestModel request)
        {
            request = await _requestRepository.Create(request);
            await _requestRepository.Save();

            return request;
        }

        public async Task<RequestModel> Get(RequestModel request)
        {
            return await _requestRepository.Get(request);
        }

        public async Task<IEnumerable<RequestModel>> GetAll(RequestModel request)
        {
            return await _requestRepository.GetAll(request);
        }

        public async Task<IEnumerable<RequestModel>> GetAllUncensored()
        {
            return await _requestRepository.GetAllUncensored();
        }

        public async Task<RequestModel> GetUncensored(RequestModel request)
        {
            return await _requestRepository.GetUncensored(request);
        }

        public async Task<RequestModel> Update(RequestModel request)
        {
            request = await _requestRepository.Update(request);

            await _requestRepository.Save();
            return request;
        }

        public async Task Delete(RequestModel request)
        {
            _requestRepository.Delete(request);
            await _requestRepository.Save();
        }
    }
}