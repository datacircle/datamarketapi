using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class RequestVoteService
    {
        private readonly IRequestRepository _requestRepository;
        private readonly IRequestVoteRepository _requestVoteRepository;

        public RequestVoteService(IRequestRepository requestRepository, IRequestVoteRepository requestVoteRepository)
        {
            _requestRepository = requestRepository;
            _requestVoteRepository = requestVoteRepository;
        }

        public async Task<bool> Upvote(RequestVoteModel model)
        {
            await _requestVoteRepository.Upvote(model);
            await _requestVoteRepository.Save();

            await _requestRepository.UpdateVoteCount(model.RequestId);
            await _requestRepository.Save();

            return true;
        }

        public async Task<bool> Downvote(RequestVoteModel model)
        {
            await _requestVoteRepository.Downvote(model);
            await _requestVoteRepository.Save();

            await _requestRepository.UpdateVoteCount(model.RequestId);
            await _requestRepository.Save();

            return true;
        }

        public async Task<bool> Unvote(RequestVoteModel model)
        {
            await _requestVoteRepository.Unvote(model);
            await _requestVoteRepository.Save();

            await _requestRepository.UpdateVoteCount(model.RequestId);
            await _requestRepository.Save();

            return true;
        }
    }
}