using DataMarketAPI.EmailTemplates;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class RequestBetaService
    {
        private readonly IRequestBetaRepository _requestBetaRepository;
        private readonly IEmailSender _emailSender;

        public RequestBetaService(IRequestBetaRepository requestBetaRepository, IEmailSender emailSender)
        {
            _requestBetaRepository = requestBetaRepository;
            _emailSender = emailSender;
        }

        public async Task<int> Create(RequestBetaModel requestBeta)
        {
            await _requestBetaRepository.Create(requestBeta);
            if (await _requestBetaRepository.Save() > 0)
            {
                BetaRegistrationTemplate emailTemplate = new BetaRegistrationTemplate(requestBeta);
                await _emailSender.SendEmailAsync(emailTemplate);
                return 1;
            }

            return 0;
        }
    }
}