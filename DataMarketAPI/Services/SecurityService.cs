using DataMarketAPI.Application;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Security.Cryptography;

public class SecurityService
{
    private readonly IDataProtector _dataprotector;

    private readonly IKeyManager _keyManager;

    public SecurityService(IDataProtectionProvider dataProtector, IKeyManager keyManager)
    {
        _dataprotector = dataProtector.CreateProtector("DataProtector");
        _keyManager = keyManager;
    }

    public string Protect(string input)
    {
        try
        {
            return _dataprotector.Protect(input);
        }
        catch (Exception ex)
        {
            throw new ResponseException(ex.Message);
        }
    }

    public string UnProtect(string input)
    {
        try
        {
            return _dataprotector.Unprotect(input);
        }
        catch (Exception ex)
        {
            // Was not protected by this provider, probably data is already unprotected
            if (ex.HResult == -2146233088)
            {
                return input;
            }
            throw new ResponseException(ex.Message);
        }
    }

    private byte[] GenerateSalt()
    {
        byte[] salt = new byte[128 / 8];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(salt);
        }
        return salt;
    }

    public string GenerateHash()
    {
        byte[] salt = GenerateSalt();
        return Convert.ToBase64String(salt);
    }

    public string HashKey(string input)
    {
        byte[] salt = GenerateSalt();
        string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: input,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

        // hashed = System.Net.WebUtility.UrlEncode(hashed);
        // hashed = hashed.Replace(" ","");

        return hashed;
    }
}