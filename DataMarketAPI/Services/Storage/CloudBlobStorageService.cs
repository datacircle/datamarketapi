﻿using DataMarketAPI.Models;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class CloudBlobStorageService
    {
        private readonly StorageConfigurationModel _storageConfiguration;

        private readonly CloudStorageAccount _storageAccount;

        private CloudBlobClient blobClient;

        public CloudBlobStorageService(IOptions<StorageConfigurationModel> storageConfiguration)
        {
            _storageConfiguration = storageConfiguration.Value;

            _storageAccount = CloudStorageAccount.Parse(_storageConfiguration.StorageConnectionString);
            blobClient = _storageAccount.CreateCloudBlobClient();
        }

        private async Task<CloudBlobContainer> GetContainer()
        {
            CloudBlobContainer container = blobClient.GetContainerReference(_storageConfiguration.Container);
            await container.CreateIfNotExistsAsync();

            return container;
        }

        public async Task<string> GetFile(FileShareModel shareFile)
        {
            CloudBlobContainer container = await GetContainer();

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(shareFile.GetFullyQualifiedName());

            string targetFilePath = Path.GetTempFileName();

            using (FileStream fileStream = System.IO.File.OpenWrite(targetFilePath))
            {
                await blockBlob.DownloadToStreamAsync(fileStream);
            }

            return targetFilePath;
        }

        public async Task<bool> CreateFile(FileShareModel shareFile)
        {
            Console.WriteLine("[" + shareFile.GetFullyQualifiedName() + "] Starting Create File");
            CloudBlobContainer container = await GetContainer();

            if (!await container.ExistsAsync())
            {
                return false;
            }

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(shareFile.GetFullyQualifiedName());

            using (FileStream fileStream = System.IO.File.OpenRead(shareFile.FilePath))
            {
                Console.WriteLine("[" + shareFile.GetFullyQualifiedName() + "] File Size: (bytes) " + fileStream.Length);
                Console.WriteLine("[" + shareFile.GetFullyQualifiedName() + "] Pre UploadFromStream.");

                await blockBlob.UploadFromStreamAsync(fileStream);

                Console.WriteLine("[" + shareFile.GetFullyQualifiedName() + "] Post UploadFromStream.");
            }

            return true;
        }
    }
}