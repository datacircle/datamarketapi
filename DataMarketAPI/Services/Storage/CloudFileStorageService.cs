﻿using DataMarketAPI.Models;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DataMarketAPI.Services
{
    public class CloudFileStorageService
    {
        private readonly StorageConfigurationModel _storageConfiguration;

        private readonly CloudStorageAccount _storageAccount;

        private CloudFileClient fileClient;

        public CloudFileStorageService(IOptions<StorageConfigurationModel> storageConfiguration)
        {
            _storageConfiguration = storageConfiguration.Value;

            _storageAccount = CloudStorageAccount.Parse(_storageConfiguration.StorageConnectionString);
            fileClient = _storageAccount.CreateCloudFileClient();
        }

        private async Task<CloudFileShare> GetShare()
        {
            CloudFileShare share = fileClient.GetShareReference(_storageConfiguration.Share);
            await AutoIncreaseShare(share);

            return share;
        }

        private async Task AutoIncreaseShare(CloudFileShare share)
        {
            Microsoft.WindowsAzure.Storage.File.Protocol.ShareStats stats = await share.GetStatsAsync();
            Console.WriteLine("Current share usage: {0} GB", stats.Usage.ToString());

            // Specify the maximum size of the share, in GB.
            // This line sets the quota to be 10 GB greater than the current usage of the share.
            share.Properties.Quota = 10 + stats.Usage;
            await share.SetPropertiesAsync();

            // Now check the quota for the share. Call FetchAttributes() to populate the share's properties.
            await share.FetchAttributesAsync();
            Console.WriteLine("Current share quota: {0} GB", share.Properties.Quota);
        }

        public async Task<string> GetFile(string fileName, string userId)
        {
            CloudFileShare share = await GetShare();

            // Ensure that the share exists.
            if (await share.ExistsAsync())
            {
                CloudFileDirectory rootDir = share.GetRootDirectoryReference();
                CloudFileDirectory sampleDir = rootDir.GetDirectoryReference(userId);

                // Ensure that the directory exists.
                if (await sampleDir.ExistsAsync())
                {
                    // Get a reference to the file we created previously.
                    CloudFile file = sampleDir.GetFileReference(fileName);

                    // Ensure that the file exists.
                    if (await file.ExistsAsync())
                    {
                        return await file.DownloadTextAsync();
                    }
                }
            }

            return null;
        }

        public async Task<bool> CreateFile(string userId, FileShareModel shareFile)
        {
            // env - user - metric - file

            CloudFileShare share = await GetShare();

            if (!await share.ExistsAsync())
            {
                return false;
            }

            CloudFileDirectory rootDir = share.GetRootDirectoryReference();
            CloudFileDirectory userDir = rootDir.GetDirectoryReference(userId);
            await userDir.CreateIfNotExistsAsync();
            CloudFileDirectory metricDir = userDir.GetDirectoryReference(shareFile.MetricId.ToString());
            await metricDir.CreateIfNotExistsAsync();

            CloudFile cloudFileTarget = metricDir.GetFileReference(shareFile.FileName);

            if (await metricDir.ExistsAsync())
            {
                Console.WriteLine("Deleting everything in folder");

                FileContinuationToken token = null;
                do
                {
                    int maxResults = 5000;
                    FileResultSegment results = await metricDir.ListFilesAndDirectoriesSegmentedAsync(maxResults, token, null, null);
                    foreach (IListFileItem fileItem in results.Results)
                    {
                        CloudFile file = new CloudFile(fileItem.Uri, fileClient.Credentials);
                        await file.DeleteIfExistsAsync();
                    }
                    token = results.ContinuationToken;
                }
                while (token != null);
            }

            Console.WriteLine(shareFile.FilePath);

            await cloudFileTarget.DeleteIfExistsAsync();

            using (Stream streamLocal = new FileStream(shareFile.FilePath, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                cloudFileTarget.UploadFromStreamAsync(streamLocal).Wait();
            }

            Console.WriteLine("upload to azure qeueud!");

            return true;
        }
    }
}