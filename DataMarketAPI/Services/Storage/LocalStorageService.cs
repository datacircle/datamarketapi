﻿using DataMarketAPI.Models;
using Microsoft.Extensions.Options;
using System.IO;

namespace DataMarketAPI.Services
{
    public class LocalStorageService
    {
        private readonly string _basePath;
        private readonly string _baseDirectory;
        private readonly string _rootDir;
        private readonly StorageConfigurationModel _storageConfiguration;

        public LocalStorageService(IOptions<StorageConfigurationModel> storageConfiguration)
        {
            _storageConfiguration = storageConfiguration.Value;
            _basePath = _storageConfiguration.BasePath;
            _baseDirectory = _storageConfiguration.BaseDirectory;
            _rootDir = _basePath + "/" + _baseDirectory;
        }

        public string GetFilePath(MetricModel metric)
        {
            return _rootDir
                + "/" + metric.Owner.ToString()
                + "/" + metric.ID.ToString()
                + "/" + metric.FileName;
        }

        public string GetFilePath(FileShareModel fileShare)
        {
            return _rootDir
                + "/" + fileShare.UserId.ToString()
                + "/" + fileShare.MetricId.ToString()
                + "/" + fileShare.FileName;
        }

        public bool WriteFile(FileShareModel fileShare)
        {
            string userDir = _rootDir + "/" + fileShare.UserId.ToString();
            string metricDir = userDir + "/" + fileShare.MetricId.ToString();
            string newFilePath = metricDir + "/" + fileShare.FileName;

            Directory.CreateDirectory(_rootDir);
            Directory.CreateDirectory(userDir);
            Directory.CreateDirectory(metricDir);

            if (File.Exists(newFilePath))
            {
                File.Delete(newFilePath);
            }

            File.Move(fileShare.FilePath, newFilePath);
            return true;
        }
    }
}