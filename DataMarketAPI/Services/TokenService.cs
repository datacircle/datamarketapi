using DataMarketAPI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;

namespace DataMarketAPI.Services
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class TokenService
    {
        private readonly TokenProviderOptionsModel _options;

        public TokenService(TokenProviderOptionsModel options)
        {
            _options = options;
        }

        public TokenProviderOptionsModel getTokenProviderOptions()
        {
            return _options;
        }

        public String GenerateToken(HttpContext httpContext, User user)
        {
            return CreateToken(user, httpContext.Connection.RemoteIpAddress);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public String GenerateLongLivedToken(User user)
        {
            return CreateToken(user, null, true);
        }

        private String CreateToken(User user, IPAddress IP = null, bool longLived = false)
        {
            var nowDt = DateTime.UtcNow;
            var nowDto = DateTimeOffset.UtcNow;

            // Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(CustomClaimTypes.Email, user.Email.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, nowDto.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim(CustomClaimTypes.CompanyId, user.CompanyId.HasValue ? user.CompanyId.ToString() : String.Empty),
                new Claim(CustomClaimTypes.CompanyName, user.Company != null ? user.Company.Name.ToString() : String.Empty),
                new Claim(CustomClaimTypes.FirstName, user.FirstName.ToString()),
                new Claim(CustomClaimTypes.LastName, user.LastName.ToString()),
                new Claim(CustomClaimTypes.UserId, user.Id.ToString()),
            };

            TimeSpan _expiration = _options.Expiration;

            if (longLived)
            {
                _expiration = TimeSpan.FromDays(7300);
            }

            // Create the JWT and write it to a string
            JwtSecurityToken jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: nowDt,
                expires: nowDt.Add(_expiration),
                signingCredentials: _options.SigningCredentials);

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}