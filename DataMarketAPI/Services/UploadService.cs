using DataMarketAPI.Models;
using Microsoft.Extensions.Logging;

namespace DataMarketAPI.Services
{
    public class UploadService
    {
        private readonly ILogger _logger;

        private readonly LocalStorageService _storageService;

        public UploadService(ILogger<UploadService> logger, LocalStorageService storageService)
        {
            _logger = logger;
            _storageService = storageService;
        }

        public void UploadFile(FileShareModel file)
        {
            _storageService.WriteFile(file);
        }

        public string GetFilePath(FileShareModel file)
        {
            return _storageService.GetFilePath(file);
        }
    }
}