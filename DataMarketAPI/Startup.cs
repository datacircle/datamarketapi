﻿using DataMarketAPI.Database;
using DataMarketAPI.Models;
using DataMarketAPI.Repositories;
using DataMarketAPI.Services;
using Hangfire;
using Hangfire.Console;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Swashbuckle.Swagger.Model;
using System;
using System.IO;
using System.Text;

namespace DataMarketAPI
{
    public class Startup
    {
        public readonly SymmetricSecurityKey _signingKey;
        public readonly SigningCredentials _sc;
        private readonly IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            builder.AddApplicationInsightsSettings(developerMode: env.IsDevelopment());

            var loggerconfig = new LoggerConfiguration()
            .MinimumLevel.Information()
            .Enrich.FromLogContext()
            .WriteTo.RollingFile("../Logs/DataMArketAPI/" + env.EnvironmentName + "/log-{Date}.txt");

            if (env.IsProduction())
            {
                loggerconfig.WriteTo.Logentries("63f83c24-e4da-4ff6-be3d-e65bf3f047df");
            }

            Log.Logger = loggerconfig.CreateLogger();
            Configuration = builder.Build();

            var secretKey = "gjn2j343@3fnrng1u!!23nfeb13";
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            _sc = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            SetupPersistance(services);

            SetupDataProtection(services);

            services.AddIdentity<User, Role>().AddEntityFrameworkStores<DatabaseContext>().AddDefaultTokenProviders();

            //.AddRoleManager<RoleManager<Role>>();

            SetupRepositories(services);
            SetupServices(services);

            services.Configure<EmailSettingsModel>(Configuration.GetSection("EmailSettings"));
            services.Configure<StorageConfigurationModel>(Configuration.GetSection("StorageConfiguration"));

            TokenProviderOptionsModel _tpo = new TokenProviderOptionsModel
            {
                SigningCredentials = _sc,
                Audience = "dcAudience65324",
                Issuer = "dataDc101",
            };

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _tpo.Issuer,
                    ValidAudience = _tpo.Audience,
                    IssuerSigningKey = _signingKey
                };
            });           

            services.AddSingleton<TokenProviderOptionsModel>(_tpo);
            services.AddScoped<TokenService>();
            services.AddOptions();

            services.AddMvc().AddJsonOptions(options =>
            {
                // !important! stops infite loop in nested models
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            SetupSwagger(services);
            services.AddApplicationInsightsTelemetry(Configuration);
        }

        private void SetupDataProtection(IServiceCollection services)
        {
            //if (_env.IsDevelopment())
            //{
                services.AddDataProtection()
                    .SetApplicationName("DataMarketAPI")
                    .UseCryptographicAlgorithms(new AuthenticatedEncryptorConfiguration()
                    {
                        EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
                        ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
                    })
                    .SetDefaultKeyLifetime(TimeSpan.FromDays(7))
                    .PersistKeysToFileSystem(new DirectoryInfo(Directory.GetCurrentDirectory() + "/Keys"));

            //}
            //else
            //{
            //    services.AddDataProtection()
            //        .SetApplicationName("DataMarketAPI")
            //        .UseCryptographicAlgorithms(new AuthenticatedEncryptorConfiguration()
            //        {
            //            EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
            //            ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
            //        })
            //        .SetDefaultKeyLifetime(TimeSpan.FromDays(7))
            //        .PersistKeysToAzureBlobStorage()
            //}
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseAuthentication();

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUi();
            }
        }

        public virtual void SetupRepositories(IServiceCollection services)
        {
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IDatasourceRepository, DatasourceRepository>();
            services.AddScoped<IMetricRepository, MetricRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRequestBetaRepository, RequestBetaRepository>();
            services.AddScoped<IQueryLogRepository, QueryLogRepository>();
            services.AddScoped<IRequestRepository, RequestRepository>();
            services.AddScoped<ICardRepository, CardRepository>();
            services.AddScoped<IInsertLogRepository, InsertLogRepository>();
            services.AddScoped<IRatingRepository, RatingRepository>();
            services.AddScoped<IMetricTransferRepository, MetricTransferRepository>();
            services.AddScoped<IRequestCommentRepository, RequestCommentRepository>();
            services.AddScoped<IRequestVoteRepository, RequestVoteRepository>();
            services.AddScoped<IMetricAccessRepository, MetricAccessRepository>();
            services.AddScoped<IMetricCommentRepository, MetricCommentRepository>();
            services.AddScoped<IHashTagRepository, HashTagRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
        }

        public virtual void SetupServices(IServiceCollection services)
        {
            services.AddTransient<IEmailSender, MessageService>();
            services.AddScoped<AccountService>();
            services.AddScoped<DatasourceService>();
            services.AddScoped<EngineService>();
            services.AddScoped<MetricService>();
            services.AddScoped<SecurityService>();
            services.AddScoped<TokenService>();
            services.AddScoped<DataService>();
            services.AddScoped<CacheService>();
            services.AddScoped<RequestBetaService>();
            services.AddScoped<QueryLogService>();
            services.AddScoped<ConnectionService>();
            services.AddScoped<RequestService>();
            services.AddScoped<CardService>();
            services.AddScoped<RatingService>();
            services.AddScoped<MetricTransferService>();
            services.AddScoped<RequestCommentService>();
            services.AddScoped<RequestVoteService>();
            services.AddScoped<CloudFileStorageService>();
            services.AddScoped<CloudBlobStorageService>();
            services.AddScoped<LocalStorageService>();
            services.AddScoped<UploadService>();
            services.AddScoped<MetricAccessService>();
            services.AddScoped<NonUserDownloadMetricService>();
            services.AddScoped<MetricCommentService>();
            services.AddScoped<OrderService>();

            services.AddHangfire(
                config =>
                {
                    config.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"));
                    config.UseConsole();
                }
            );
        }

        public virtual void SetupPersistance(IServiceCollection services)
        {
            Console.WriteLine(Configuration.GetConnectionString("DefaultConnection"));
            services.AddDbContext<DatabaseContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
            );

            // redis add correct string config
            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration.GetConnectionString("RedisConnection");
                option.InstanceName = "master";
            });
        }

        public virtual void SetupSwagger(IServiceCollection services)
        {
            if (_env.IsDevelopment())
            {
                services.AddSwaggerGen(options =>
                {
                    options.SingleApiVersion(new Info
                    {
                        Version = "v1",
                        Title = "DataCircle API",
                        Description = "A simple example ASP.NET Core Web API",
                        TermsOfService = "None"
                    });

                    var fileName = "DataMarketAPI.xml";
                    var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, fileName);
                    options.IncludeXmlComments(filePath);
                });
            }
        }
    }
}