namespace DataMarketAPI.ViewModel
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}