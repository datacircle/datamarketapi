﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketAPI.ViewModels
{
    public class MetricAccessMapViewModel
    {
        [Required]
        public Guid MetricId { get; set; }

        public string Email { get; set; }

        public string TargetPublicId { get; set; }
    }
}