﻿using System;
using DataMarketAPI.Models;

namespace DataMarketAPI.ViewModels
{
    public class MetricDatasourceViewModel : MetricModel
    {
        public Models.Type DatasourceType { get; set; }        
    }
}