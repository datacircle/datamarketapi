﻿using DataMarketAPI.Models;
using System;

namespace DataMarketAPI.ViewModels
{
    public class OrderViewModel
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public Guid MetricId { get; set; }
        public MetricModel Metric { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}