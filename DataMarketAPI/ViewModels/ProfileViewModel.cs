using System.ComponentModel.DataAnnotations;

namespace DataMarketAPI.ViewModel
{
    public class ProfileViewModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Company { get; set; }

        public string UnsubscribeToken { get; set; }
        public string DoubleOptInToken { get; set; }
        public bool Subscribed { get; set; }
        public bool DoubleOptedIn { get; set; }
    }
}